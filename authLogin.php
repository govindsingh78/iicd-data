<?php 
  session_start();
  include 'meekrodb.2.3.class.php';
 
  $qry = DB::insert('users', array(
          'first_name' => $_SESSION['first_name'],
          'middle_name' => $_SESSION['middle_name'],
          'last_name' => $_SESSION['last_name'],
          'email' => $_SESSION['uemail'],
          'password' => $_SESSION['password'],
          'otp' => $_SESSION['otp'],
          'email_verified' => 1,
          'phone_verified' => 1
        ));
  
  session_unset();
  session_destroy();
?>