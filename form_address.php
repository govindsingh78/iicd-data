<?php 
  session_start();
  include 'meekrodb.2.3.class.php';

  $query = "select * from user_details where user_id = '".$_SESSION['user_id']."'";

  $row = DB::queryFirstRow($query);
 
  $countries = DB::query("select * from country");

?>
<form id="form_address" name="form_address">
   <div class="my-dtl-feed">
      <div class="col-md-12">
        <p> Permanent Address </p> <br/>
        <div class="group">
            <div class="col-md-4">
             <div class="my-input-bx field required-field">      
                <input type="text" id="address_line1" name="address_line1" class="form-control" value="<?=$row['address_line1']?>">
                <span class="bar"></span>
                <label>Address Line No 1</label>
             </div>
            </div>

            <div class="col-md-4">  
             <div class="my-input-bx field required-field">    
                <input type="text" id="address_line2" name="address_line2" class="form-control" value="<?=$row['address_line2']?>">
                <span class="bar"></span>
                <label>Address Line No 2</label>
             </div>
            </div>

            <div class="col-md-4">  
                  <div class="my-input-bx field required-field">    
                    <div class="selectContainer"> 
                        <label class="my-label">Country</label>
                         <span class="bar"></span>
                        <select name="country" class="form-control" id="country">  
                         <option value="">Select Country</option>
                         <?php
                          foreach ($countries as $val) {
                              $selected = '';
                              if($val['id']==$row['country']){
                                  $selected = 'selected="selected"';
                              }
                              echo '<option value="'.$val['id'].'" '.$selected.'>'.$val['country_name'].'</option>';
                          }
                          ?>
                        </select>
                     </div>
                  </div>
            </div>
         </div>
        <div class="group">
            <div class="col-md-4">  
             <div class="my-input-bx field required-field">    
              <div class="selectContainer"> 
                  <label class="my-label">State</label>
                   <span class="bar"></span>
                   <select id="state" name="state" class="form-control" id="country">  
                    <option value="">Select State</option>
                  </select>  
               </div>
              </div>
            </div>


            <div class="col-md-4">  
                <div class="my-input-bx field required-field">    
                  <div class="selectContainer"> 
                      <label class="my-label">City</label>
                       <span class="bar"></span>
                       <select name="city" class="form-control" id="city">  
                        <option value="">Select City</option>
                      </select> 
                     
                   </div>
                </div>
            </div>

            <div class="col-md-4">  
             <div class="my-input-bx field required-field">    
                <input type="text" id="pin_code" name="pin_code" value="<?=$row['pin_code']?>" class="form-control">
                <span class="bar"></span>
                <label> Pin Number</label>
             </div>
            </div>
        </div>

      <p>Correspondence  address</p><br/>
        <div class="group">
	      	<div class="col-md-8">
	      	  <input type="checkbox" id="addresscheck" name="sameaddress" value="1">  Check this box if Permanent Address and Correspondence Address are the same.
			</div>
        </div>	<br><br/>	<br>	

        <div class="group"><br/>
            <div class="col-md-4">
             <div class="my-input-bx field required-field">      
                <input type="text" id="caddress_line1" name="caddress_line1" class="form-control" value="<?=$row['caddress_line1']?>">
                <span class="bar"></span>
                <label>Address Line No 1</label>
             </div>
            </div>

            <div class="col-md-4">  
             <div class="my-input-bx field required-field">    
                <input type="text" id="caddress_line2" name="caddress_line2" class="form-control" value="<?=$row['caddress_line2']?>">
                <span class="bar"></span>
                <label>Address Line No 2</label>
             </div>
            </div>

            <div class="col-md-4">  
                  <div class="my-input-bx field required-field">    
                    <div class="selectContainer"> 
                        <label class="my-label">Country</label>
                         <span class="bar"></span>
                        <select name="ccountry" class="form-control" id="ccountry">  
                         <option value="">Select Country</option>
                         <?php
                          foreach ($countries as $val) {
                              $selected = '';
                              if($val['id']==$row['country']){
                                  $selected = 'selected="selected"';
                              }
                              echo '<option value="'.$val['id'].'" '.$selected.'>'.$val['country_name'].'</option>';
                          }
                          ?>
                        </select>
                     </div>
                  </div>
            </div>
         </div>
        <div class="group">
            <div class="col-md-4">  
             <div class="my-input-bx field required-field">    
              <div class="selectContainer"> 
                  <label class="my-label">State</label>
                   <span class="bar"></span>
                   <select id="cstate" name="cstate" class="form-control">  
                    <option value="">Select State</option>
                  </select>  
               </div>
              </div>
            </div>


            <div class="col-md-4">  
                <div class="my-input-bx field required-field">    
                  <div class="selectContainer"> 
                      <label class="my-label">City</label>
                       <span class="bar"></span>
                       <select name="ccity" class="form-control" id="ccity">  
                        <option value="">Select City</option>
                      </select> 
                     
                   </div>
                </div>
            </div>

            <div class="col-md-4">  
             <div class="my-input-bx field required-field">    
                <input type="text" id="cpin_code" name="cpin_code" value="<?=$row['cpin_code']?>" class="form-control">
                <span class="bar"></span>
                <label> Pin Number</label>
             </div>
            </div>
        </div>

      <nav class="form-section-nav">
        <input type="hidden" name="action" id="action" value="save_address">
        <span id="btn_back_address" class="btn-secondary form-nav-prev"><img src="images/left-arrow.jpg" alt="left"> Prev</span>
        <span id="btn_next_address" class="btn-std form-nav-next"> Save & Next <img src="images/right-arrow.jpg" alt="left"></span>
      </nav>

  </div>
  </div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function(){


    $("#btn_back_address").unbind().click(function() {
      $('#personal_container').load('form_personal.php',function(e){
          $("#address_container" ).slideUp( "slow");
          $('#address_container').html('');
          $("#personal_container" ).slideDown( "slow");
      });
    });


    $("#btn_next_address").unbind().click(function() { 
        
        if(!$('#form_address').valid()){
          return false;
        }

      //  var formData = new FormData($('form#form_address')[0]);
var formData = $('form#form_address').serialize();
        $.ajax({
            type: "POST",
            url:"admission-save.php",
            data:  formData,
            dataType: "json",
            cache: false,
            success: function(response) {
                if(response.status == 1){
                  $('#exam_container').load('form_exam.php',function(e){
                    
                    $("#address_container" ).slideUp( "slow");
                    $('#address_container').html('');
                    $("#exam_container" ).slideDown( "slow");

                  });
                }
            }
        });

    });

    $('#form_address').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules: 
        {
          "address_line1": {
            required: true             
          },
          "address_line2": {
            required: true             
          },
          "country": {
            required: true             
          },
          "state": {
            required: true             
          },
          "city": {
            required: true             
          },
          "pin_code": {
            required: true,
            validate_pincode:true             
          },
          "caddress_line1": {
            required: true             
          },
          "caddress_line2": {
            required: true             
          },
          "ccountry": {
            required: true             
          },
          "cstate": {
            required: true             
          },
          "ccity": {
            required: true             
          },
          "cpin_code": {
            required: true,
            validate_pincode:true             
          }
        },
        messages: 
        {
         "address_line1": {
            required: "Address line 1 is required"
          },
          "address_line2": {
            required: "Address line 2 is required"
          },
          "country": {
            required: "Country is required"
          },
          "state": {
            required: "State is required"
          },
          "city": {
            required: "City is required"
          },
          "pin_code": {
            required: "Pincode is required",
          },
          "caddress_line1": {
            required: "Address line 1 is required"
          },
          "caddress_line2": {
            required: "Address line 2 is required"
          },
          "ccountry": {
            required: "Country is required"
          },
          "cstate": {
            required: "State is required"
          },
          "ccity": {
            required: "City is required"
          },
          "cpin_code": {
            required: "Pincode is required",
          }
        }
  });

   
  $("#state").unbind().change(function() {
    var stateid = $(this).val();
    if(stateid !=''){
      $.ajax({
          type: "POST",
          url: "showcity.php",
          dataType: "json",
          data: "stateid=" + stateid,
          success: function (response) {
            $('#city').html(response.data);
          }
      });
    }
  })


  $("#country").unbind().change(function() {
    var countryid = $(this).val();
    if(countryid !=''){
      $.ajax({
          type: "POST",
          url: "showstate.php",
          dataType: "json",
          data: "countryid=" + countryid,
          success: function (response) {
            $('#state').html(response.data);
          }
      });
    }
  })

  

  if("<?=$row['country'] !='' ?>"){

    var countryid = "<?php echo $row['country']; ?>";
    $.ajax({
          type: "POST",
          url: "showstate.php",
          dataType: "json",
          data: "countryid=" + countryid,
          success: function (response) {
            $('#state').html(response.data);
          }
      });
  }


  if("<?=$row['state'] !='' ?>"){

    var stateid = "<?php echo $row['state']; ?>";

    $.ajax({
      type: "POST",
      url: "showcity.php",
      dataType: "json",
      data: "stateid=" + stateid,
      success: function (response) {
        $('#city').html(response.data);
      }
    });
  }

   
  $("#cstate").unbind().change(function() {
    var stateid = $(this).val();
    if(stateid !=''){
      $.ajax({
          type: "POST",
          url: "showcity.php",
          dataType: "json",
          data: "stateid=" + stateid,
          success: function (response) {
            $('#ccity').html(response.data);
          }
      });
    }
  })


  $("#ccountry").unbind().change(function() {
    var countryid = $(this).val();
    if(countryid !=''){
      $.ajax({
          type: "POST",
          url: "showstate.php",
          dataType: "json",
          data: "countryid=" + countryid,
          success: function (response) {
            $('#cstate').html(response.data);
          }
      });
    }
  })

  

  if("<?=$row['ccountry'] !='' ?>"){

    var countryid = "<?php echo $row['ccountry']; ?>";
    $.ajax({
          type: "POST",
          url: "showstate.php",
          dataType: "json",
          data: "countryid=" + countryid,
          success: function (response) {
            $('#cstate').html(response.data);
          }
      });
  }


  if("<?=$row['cstate'] !='' ?>"){

    var stateid = "<?php echo $row['cstate']; ?>";

    $.ajax({
      type: "POST",
      url: "showcity.php",
      dataType: "json",
      data: "stateid=" + stateid,
      success: function (response) {
        $('#ccity').html(response.data);
      }
    });
  }


$('#addresscheck').click(function() {
    var c=$('#addresscheck').val();
    if ($('#addresscheck').is(':checked')) {
    	var al1=$('#address_line1').val();
    	var al2=$('#address_line2').val();
    	var cnt=$('#country').val();
    	var st=$('#state').val();

    	var ct=$('#city').val();
    	var pc=$('#pin_code').val();

    	$("#caddress_line1").val(al1);
    	$("#caddress_line2").val(al2);
    	$("#ccountry").val(cnt);
   		 $.ajax({
          type: "POST",
          url: "showstate.php",
          dataType: "json",
          data: "countryid=" + cnt,
          success: function (response) {
            $('#cstate').html(response.data);
            $("#cstate").val(st);
          }
      	});

   		 $.ajax({
	      type: "POST",
	      url: "showcity.php",
	      dataType: "json",
	      data: "stateid=" + st,
	      success: function (response) {
	        $('#ccity').html(response.data);
	        $("#ccity").val(ct);
	      }
	    });

    	$("#cpin_code").val(pc);
    }else{
      $("#caddress_line1").val('');
      $("#caddress_line2").val('');
      $("#ccountry").val('');
      $("#cstate").val('');
      $("#ccity").val('');
      $("#cpin_code").val('');
    }
});



});
</script>