=== Users Excluded ===
Contributors: Heinz JKarl Otta Fritz
Tags: users, roles, permissions, pages
Requires at least: 2.7
Tested up to: 2.9.2
Stable tag: 1.0

Control what pages/posts a user is able to edit.

== Description ==

This plugin will allow you to exclud users from editing specific pages.
Warning: The quick edit functionality is just hidden. Not blocked.
Works fine with PHP4.

This plugin is based on "Users Only" by "Start a Web Design Business" (http://www.sohohappens.com/347/users-only-wordpress-plugin)

== Installation ==

1. Download the plugin, extract and upload it to your plugins folder on your server.

2. Activate the plugin.

3. Edit a page/post.

4. Select which users shall be exlucded from editing that page/post.

5. Publish or Save the page/post.


== Changelog ==

= 1.0 =
* Initial release