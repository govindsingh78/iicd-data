=== NextGEN Gallery Comments ===
Contributors: Roberto Cantarano
Tags: photo-gallery,nextgen-gallery,nextgen,gallery,comment,comments,nextgen-gallery-comments,nextgen gallery comments
Requires at least: 2.9.1
Tested up to: 3.2.1
Stable tag: 0.1.5

This plugin add comments (form and list) in every NextGEN Gallery.

== Description ==

**Please use at least version 1.8.3 of NextGEN Gallery. This plugin is not tested with lower versions**

NextGEN Gallery Comments give power to the best wordpress gallery plugin i have seen! With my plugin, now all users can leave comments to your galleries. Comments are also displayed in admin in Comments section and in manage gallery page.

** * * NOTE * * **

For now, comments are show on galleries only when they are not called directly in post/page with shortcode. Example:


[nggallery id=X]  ----> gallery comments are not showed

[album id=X] ----> gallery comments are showed (Not in album list, but inside the gallery page)


I will check in next release to find a way to do it.

= Features =

* Template : You can add custom comment form template for your theme. (read F.A.Q.)

== Credits ==

Copyright 2011 by Roberto Cantarano

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

== Installation ==

1. Upload `plugin-name.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress (you need NextGEN Gallery plugin to be active!)

That's it ... Have fun

== Screenshots ==

== Frequently Asked Questions ==

= I have installed the plugin but i can't see any admin options page. Where is it? =

Ehm.. this is my first release of my first plugin, in next versions i will ad an option page with various options. 

= What about customizing the comments page? =

This is a simple task. This plugin use the template `comments-nggallery.php` inside `plugins/nextgen-gallery-comments/template` to show comments list and form.
If you want to customize it, copy it and paste in your theme inside `nggallery` folder (if this not exists, create it. This folder is needed to customize [NextGEN Gallery templates](http://nextgen-gallery.com/templates/))

= If i click the logout button in gallery comment form, the page is bad redirected. How can i fix it? =

You have to use a filter (put it in FUNCTIONS.PHP in your theme) to change the logout url:

add_filter('your_function','rcwd_comment_form_defaults');
function your_function($defaults){
   global $user_identity, $post_id;
   if(get_query_var('gallery') != ''){
      $logout_url = esc_attr($_SERVER["REQUEST_URI"]);
   }else{
      $logout_url = get_permalink($post_id);
   }
   $defaults['logged_in_as'] = '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', $logout_url ) ) ) . '</p>';
   return $defaults;

}

== Changelog ==

= V0.1.5 - 16.09.2011 =
* FIX: gallery is displayed above the content event if inside the editor it was placed below

= V0.1.4 - 02.09.2011 =
* FIX: 'no comments' text in nggallery-manage-gallery appears in bottom left position

= V0.1.3 - 31.08.2011 =
* FIX: all existing galleries before the plugin activation don't show comments
* Removed comments from gallery when called directly with shortcode [nggallery id=X]   

= V0.1.2 - 28.08.2011 =
* Fixed some bugs which did not allow the installation in NextGEN Gallery Date is active

= V0.1.1 - 27.08.2011 =
* Fixed some bugs which did not allow the display of comments 

= V0.1 - 24.08.2011 =
* Initial release