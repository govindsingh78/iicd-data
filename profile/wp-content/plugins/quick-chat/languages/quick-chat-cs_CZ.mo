��          �   %   �      p     q  !   �  !   �     �     �  6   �  .        E     T     d  +   k  C   �  )   �  A     1   G  2   y     �     �  C   �  Y         z  2   �     �     �  2   �  $         %    2     A  0   W  2   �  
   �     �  I   �  :   	     R	     e	     x	  /   ~	  >   �	  0   �	  R   
  +   q
  +   �
     �
     �
  C   �
  Y   A     �  @   �     �        +        3     R     
                                                        	                                                              Appearance options Bad words list (comma separated): Chat name prefix for guest users: Chat room name: Delete Donate using PayPal (sincere thank you for your help): Filter bad words contained inside other words: Filter options General options Guest_ Here are the Quick Chat appearance options: Here you can control Quick Chat message and chat user names filter: Here you can control all general options: Hide "Powered by Quick Chat" link (big thanks for not hiding it): In this section you can control security options: Incoming message sound notification on by default: Message container height: Powered by Quick Chat Protect registered users user names from being used by other users: Quick Chat is quick and elegant WordPress chat plugin that does not waste your bandwidth. Reply to Restricted chat user names list (comma separated): Security options Title You must login if you want to participate in chat. You must select at least one message options page Project-Id-Version: Quick Chat 2.40
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-30 13:04+0100
PO-Revision-Date: 
Last-Translator: Marko <marko [at] techytalk.info>
Language-Team: Petr
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: .
X-Poedit-Language: Czech
X-Poedit-Country: CZECH REPUBLIC
X-Poedit-SourceCharset: utf-8
X-Poedit-SearchPath-0: /home/marko/Documents/public_html/wordpress/wp-content/plugins/quick-chat
 Nastavení zobrazení Seznam nevhodných slov (oddělených čárkou): Označení uživatelů přihlášených jako host: Místnost: Smazat Podpořit prostřednictvím PayPal (srdečně děkujeme za Vaší pomoc): Odstranit nevhodná slova obsažená uvnitř jiných slov: Nastavení filtrů Obecné nastavení Host_ Zde můžete nastavit zobrazování Quick Chat: Zde můžete nastavit filtry pro zprávy a jména uživatelů: Zde můžete změnit volby obecného nastavení: Skrýt odkaz "Běží na pluginu Quick Chat" (velké díky za zachování odkazu): Zde můžete změnit volby pro bezpečnost: Zvukové upozornění příchozích zpráv: Velikost okna zpráv: Běží na pluginu Quick Chat Zamezit použití uživatelských jmen registrovaných uživatelů: Quck Chat je rychlý a elegantní plugin pro WordPress, který neplýtvá datovým tokem. Odpovědět Seznam nevhodných uživatelských jmen (oddělených čárkou): Nastavení bezpečnosti Název Pro zapojení do konverzace se přihlašte. Vyberte alespoň jednu zprávu stránka voleb 