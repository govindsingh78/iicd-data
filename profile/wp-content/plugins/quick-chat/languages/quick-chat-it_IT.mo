��    1      �  C   ,      8  t   9     �     �  !   �  	   �  !   �          .     :     V  A   ]  _   �  6   �     6     O  .   S     �     �     �  +   �  C   �  )     A   B  m   �     �  1        8  2   K     ~     �     �  C   �     �     	     ,	  Y   B	     �	     �	     �	  2   �	     
  6   
     M
     S
  2   g
  $   �
  $   �
     �
    �
  T        X     h  &   x  	   �  $   �  
   �     �  -   �  
     I     k   i  7   �          (  +   ,     X     g     x  3   �  H   �  )   �  K   '  x   s     �  >        D  2   Z     �     �     �  O   �          *     G  T   ]     �     �     �  7   �     '  @   9     z     �  1   �  $   �  *   �             %                              ,   )                 #   &      0      	           +   "                         (      *   $   -      1                       /          
                !   .                                     '    Advertisement code for your AdSense or other ads placed between chat user name input box and message text input box: Already taken! Appearance options Bad words list (comma separated): Changelog Chat name prefix for guest users: Chat room name: Checking... Convert URLs to hyperlinks: Delete Deny chat access to the following IP addresses (comma separated): Disallow using special characters inside chat user names (including special locale characters): Donate using PayPal (sincere thank you for your help): Donating or getting help FAQ Filter bad words contained inside other words: Filter options General options Guest_ Here are the Quick Chat appearance options: Here you can control Quick Chat message and chat user names filter: Here you can control all general options: Hide "Powered by Quick Chat" link (big thanks for not hiding it): If you find Quick Chat useful you can donate to help it's development. Also you can get help with Quick Chat: Illegal characters! In this section you can control security options: Include user list: Incoming message sound notification on by default: Message container height: Not allowed! Powered by Quick Chat Protect registered users user names from being used by other users: Quick Chat FAQ: Quick Chat at TechyTalk.info Quick Chat changelog: Quick Chat is quick and elegant WordPress chat plugin that does not waste your bandwidth. Quick Chat support page: Quick Chat version: Reply to Restricted chat user names list (comma separated): Security options Timeout for refreshing list of online users (seconds): Title User list position: You must login if you want to participate in chat. You must select at least one message Your IP address is banned from chat. options page Project-Id-Version: Quick Chat 2.40
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-12 23:58+0100
PO-Revision-Date: 
Last-Translator: Marko <marko [at] techytalk.info>
Language-Team: Alex Camilleri
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Italian
X-Poedit-Country: ITALY
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: /home/marko/Documents/public_html/wordpress/wp-content/plugins/quick-chat
 Codice AdSense o altri ads da posizionare tra spazio nome utente e spazio scrittura: Gia' utilizzato Opzioni aspetto Lista parolacce (separate da virgola): Changelog Prefisso nome utente per gli ospiti: Chat room: Controllo... Conversione URL in collegamenti ipertestuali: Cancellare Rifiuta l'accesso in chat ai seguenti indirizzi IP (separati da virgola): Non permettere uso di caratteri speciali all'ìinterno del nome utente (inclusi caratteri speciali locali): Dona utilizzando PayPal (grazie mille per il supporto): Donare o ottenere supporto FAQ Filtra parolacce contenute in altre parole: Opzioni filtri opzioni generali Ospite_ Qui trovi le opzioni dell'aspetto della Quick Chat: Qui puoi controllare il filtraggio di messaggi e nomi utenti Quick Chat: Qui puoi controllare le opzioni generali: Nascondi il link "Powered by Quick Chat" (mille grazie se non lo nascondi): Se trovi utile Quick Chat puoi fare una donazione per supportarne lo sviluppo. Puoi anche ottenere aiuto con Quick Chat: Caratteri non accettati! In questa sezione puoi controllare le opzioni sulla sicurezza: Includi lista utenti: In entrata audio messaggio di notifica di default: Altezza area messaggi: Non consentito! Powered by Quick Chat Proteggi i nomi utente degli utenti registrati dall'essere utilizzati da altri: FAQ Quick Chat: Quick Chat su TechyTalk.info Changelog Quick Chat: Quick Chat è un rapido ed elegante plugin di Wordpress che non intasa la tua banda. Pagina di supporto Quick Chat: Versione Quick Chat: Rispondi Lista nomi utenti chat ristretta (separati da virgola): Opzioni sicurezza Timeout per l'aggiornamento della lista utenti online (secondi): Titolo Posizione lista utenti: Devi essere registrato per partecipare alla chat. Devi selezionare almeno un messaggio Il tuo indirizzo IP e' bannato dalla chat. pagina opzioni 