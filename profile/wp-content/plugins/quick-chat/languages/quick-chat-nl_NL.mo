��    0      �  C         (  t   )     �     �  !   �  	   �  !   �               6  A   =  _     6   �          /  .   3     b     q     �  +   �  C   �  )   �  A   "  m   d     �  1   �       2   +  A   ^     �     �  C   �     	     	     4	  Y   J	     �	     �	     �	  2   �	     
  6   
     U
     [
  2   o
  $   �
  $   �
     �
    �
  d        s     �  4   �     �  )   �                8  Q   D  U   �  3   �           8  )   M     w     �     �  (   �  C   �  +     K   4  Q   �     �  5   �       5   4  ^   j     �  %   �  [   �      [     |     �  b   �          5  
   L  2   W     �  ,   �     �     �  )   �  *     )   =     g        $      +                       &   (                 "   %      /                 *   !                         '      )   #       
   0                        .   ,       	                    -                                        Advertisement code for your AdSense or other ads placed between chat user name input box and message text input box: Already taken! Appearance options Bad words list (comma separated): Changelog Chat name prefix for guest users: Checking... Convert URLs to hyperlinks: Delete Deny chat access to the following IP addresses (comma separated): Disallow using special characters inside chat user names (including special locale characters): Donate using PayPal (sincere thank you for your help): Donating or getting help FAQ Filter bad words contained inside other words: Filter options General options Guest_ Here are the Quick Chat appearance options: Here you can control Quick Chat message and chat user names filter: Here you can control all general options: Hide "Powered by Quick Chat" link (big thanks for not hiding it): If you find Quick Chat useful you can donate to help it's development. Also you can get help with Quick Chat: Illegal characters! In this section you can control security options: Include user list: Incoming message sound notification on by default: Maximum number of digits for random guests chat user name suffix: Not allowed! Powered by Quick Chat Protect registered users user names from being used by other users: Quick Chat FAQ: Quick Chat at TechyTalk.info Quick Chat changelog: Quick Chat is quick and elegant WordPress chat plugin that does not waste your bandwidth. Quick Chat support page: Quick Chat version: Reply to Restricted chat user names list (comma separated): Security options Timeout for refreshing list of online users (seconds): Title User list position: You must login if you want to participate in chat. You must select at least one message Your IP address is banned from chat. options page Project-Id-Version: Quick Chat 2.40
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-12 23:58+0100
PO-Revision-Date: 
Last-Translator: Marko <marko [at] techytalk.info>
Language-Team: AiKoiN & Rene
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Dutch
X-Poedit-Country: NETHERLANDS
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: /home/marko/Documents/public_html/wordpress/wp-content/plugins/quick-chat
 advertentie code voor je AdSense of andere advertenties tussen de gebruikersnaam en het invoer veld: Reeds ingebruik Weergave opties Verboden woorden lijst (afgescheiden met een komma): Veranderingen logboek Standaard chat naam voor gast gebruikers: controleren... converteer URL naar Hyperlinksi: Verwijderen Verbied toegang tot chat voor de volgende IP adressen (gescheiden met een komma): Niet toestaan van speciale tekens in gebruikersnamen (ook de lokale speciale tekens): Doneer met PayPal (hartelijk bedankt voor je hulp): Doneren of hulp krijgen Veel gestelde vragen Filter verboden woorden of delen daarvan: Filter opties Algemene opties Gast_ Hier zijn de Quick Chat weergave opties: Hier kun je het Quick Chat boodschap en chat naam filter instellen: Hier kun je alle algemene opties instellen: Verberg de "Powered by Quick Chat" link (bedankt voor het NIET verbergen!): Als je Quick Chat handig vindt kun je doneren om de ontwikkeling te ondersteunen: verkeerde tekens In deze sectie kun je de veiligheidsopties instellen: Gebruikerslijst bijvoegen: Geluidssignaal standaard aan bij inkomende berichten: Maximaal aantal getallen voor het achtervoegsel van de gebruikersnaam van willekeurige gasten: Niet toegestaan Mede mogelijk gemaakt door Quick Chat Bescherm gebruik van gebruikersnamen van geregistreerde gebruikers  door andere gebruikers: Quick Chat veel gestelde vragen: Quick Chat op TechyTalk.info Quick Chat veranderingen log: Quick Chat is een snelle en elegante WordPress chat plugin welke je bandbreedte niet verwaarloosd. Quick Chat support pagina: Versie van Quick Chat: Beantwoord Verboden woorden lijst (gescheiden met een komma): Veiligheidsopties Verversen van online user lijst in seconden: Titel Gebruikerslijst positie: Je moet inloggen als je mee wilt chatten. Je moet minimaal één bericht selecteren. Uw IP-adres is geblokkeerd van deze chat. opties pagina 