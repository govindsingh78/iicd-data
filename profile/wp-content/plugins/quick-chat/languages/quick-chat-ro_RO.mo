��    0      �  C         (  t   )     �     �  !   �  	   �  !   �               6  A   =  _     6   �          /  .   3     b     q     �  +   �  C   �  )   �  A   "  m   d     �  1   �       2   +  A   ^     �     �  C   �     	     	     4	  Y   J	     �	     �	     �	  2   �	     
  6   
     U
     [
  2   o
  $   �
  $   �
     �
    �
  �        �     �  ,   �  
   �  1        6  #   E     i  N   p  ~   �  ?   >      ~     �  A   �     �     �       *     O   E  .   �  _   �  P   $     u  ?   �     �  6   �  R        p     �  h   �               .  ]   F     �     �     �  E   �     '  C   >     �  !   �  =   �  *   �  +        ?        $      +                       &   (                 "   %      /                 *   !                         '      )   #       
   0                        .   ,       	                    -                                        Advertisement code for your AdSense or other ads placed between chat user name input box and message text input box: Already taken! Appearance options Bad words list (comma separated): Changelog Chat name prefix for guest users: Checking... Convert URLs to hyperlinks: Delete Deny chat access to the following IP addresses (comma separated): Disallow using special characters inside chat user names (including special locale characters): Donate using PayPal (sincere thank you for your help): Donating or getting help FAQ Filter bad words contained inside other words: Filter options General options Guest_ Here are the Quick Chat appearance options: Here you can control Quick Chat message and chat user names filter: Here you can control all general options: Hide "Powered by Quick Chat" link (big thanks for not hiding it): If you find Quick Chat useful you can donate to help it's development. Also you can get help with Quick Chat: Illegal characters! In this section you can control security options: Include user list: Incoming message sound notification on by default: Maximum number of digits for random guests chat user name suffix: Not allowed! Powered by Quick Chat Protect registered users user names from being used by other users: Quick Chat FAQ: Quick Chat at TechyTalk.info Quick Chat changelog: Quick Chat is quick and elegant WordPress chat plugin that does not waste your bandwidth. Quick Chat support page: Quick Chat version: Reply to Restricted chat user names list (comma separated): Security options Timeout for refreshing list of online users (seconds): Title User list position: You must login if you want to participate in chat. You must select at least one message Your IP address is banned from chat. options page Project-Id-Version: Quick Chat 2.40
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-12 23:59+0100
PO-Revision-Date: 
Last-Translator: Marko <marko [at] techytalk.info>
Language-Team: Dragisha
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Romanian
X-Poedit-Country: ROMANIA
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: /home/marko/Documents/public_html/wordpress/wp-content/plugins/quick-chat
 Codul de Publicitate pentru anunţurile dvs. AdSense sau alte plasat între caseta de nume de utilizator de chat de intrare şi de mesaj de tip text caseta de intrare: Deja luate! Aspect opţiuni Bad cuvinte listă (separate prin virgulă): Modificari Chat prefix nume pentru utilizatorii clienţilor: Verificarea... Conversia URL-uri de hyperlink-uri: Sterge Interzice chat-ul de acces la următoarele adrese IP (separate prin virgulă): Nu permite utilizarea caracterelor speciale în interiorul numelor de utilizator de chat (inclusiv caractere speciale locale): Donează folosind PayPal (sincer vă mulţumesc pentru ajutor): Donarea sau obţinerea de ajutor FAQ Filtrul de cuvinte rele conţinute în interior, cu alte cuvinte: Opţiunile de filtrare Opţiunilor generale Oaspete_ Aici sunt opţiunile de Quick Chat aspect: Aici puteţi controla mesajul Quick Chat şi chat-ul nume de utilizator filtru: Aici puteţi controla toate opţiuni generale: Ascunde "Alimentat de Quick Chat" legătură într-(mulţumiri mare, pentru care nu-l ascunde): Dacă găsiţi rapid Chat utile puteţi dona pentru a ajuta la develompent sale: Caractere ilegale! În această secţiune puteţi controla opţiuni de securitate: Include lista de utilizatori: De intrare sunet mesaj de notificare în mod implicit: Numărul maxim de cifre pentru aleatoare de chat sufix ghidul de persoane cu nume: Nu este permis! Alimentat de Quick Chat Protejarea utilizatori înregistraţi numele de utilizator de a fi utilizat de către alţi utilizatori: Quick Chat FAQ: Quick Chat la TechyTalk.info Quick Chat modificări: Quick Chat este rapid şi elegant plugin WordPress de chat care nu deşeuri latimea de banda. Quick Chat sprijin pagina: Quick Chat Versiune: Răspundeţi Restrictionata de chat lista de utilizatori (separate prin virgulă): Opţiuni de securitate Expirare pentru lista de utilizatori online răcoritoare (secunde): Titlu Ghid de utilizare Lista poziţia: Trebuie sa te loghezi dacă doriţi să participaţi la chat. Trebuie să selectaţi cel puţin un mesaj Adresa dvs. de IP este interzis de la chat. opţiuni pagină 