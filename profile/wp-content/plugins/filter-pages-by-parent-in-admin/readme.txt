=== Filter Pages by parent in admin ===
Contributors: irvingswiftj, Electric Studio
Tags: wp-admin, parent, filter, pages, page, sort, child
Requires at least: 3.1
Tested up to: 3.2.1
Stable tag: 1.1

Filter pages in the wp-admin by their parent

== Description ==

Adds a filter in your wp-admin that allows you to choose a parent page so that only the children of that page will be shown. Very useful if you have a lot of pages!


== Installation ==

Install from wordpress plugins directory.

Else, to install manually:

1. Upload unzipped plugin folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Screenshots ==

1. The filter select box (the right most option box)

== Changelog ==

= 1.0 =
* Version 1.

= 1.1 =
* bug fix for Drafts and Trashed Pages

== Upgrade Notice ==

= 1.0 =

= 1.1 =
Bug fix
