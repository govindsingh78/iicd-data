=== Add Multiple Users for WordPress ===
Contributors: happynuclear
Donate link: http://www.happynuclear.com/sandbox/amu/add-multiple-users-for-wordpress.php
Tags: users, registration, admin
Requires at least: 3.1
Tested up to: 3.2.1
Stable tag: trunk

Provides a simple interface for an administrator to bulk register up to 50 WordPress user accounts in a single form.

== Description ==

Add Multiple Users provides the ability for WordPress administrators to add up to 50 WordPress user accounts at a single
time, using a simple form available as an extra admin page under your Users tab in the administration section.

Each new user can have a custom username, a customised or randomly generated password, an email address, first name, last
name and website data added automatically.

For more information please see the plugin page at http://www.happynuclear.com/sandbox/amu/add-multiple-users-for-wordpress.php

== Installation ==

Installing and activating this plugin adds an additional menu option to your Users menu in the WordPress administration
section named 'Add Multiple Users'.

1. Upload 'add_multiple_users' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Click the Add Multiple link in the Users section to access the plugin's interface

== Frequently Asked Questions ==

= Can I import users from a CSV or list of email addresses? =

A range of import options are being designed for the next release. For now its manual input.

= What information per user is needed to register a new user on the site? =

A unique username, password and email address is needed for each user. Passwords can be generated automatically if the
field is left blank. Dummy email addresses can also be created using the Force Fill Email Addresses option if necessary.

= What other user information can I add when I register a new user? =

For each user you can also specify a First Name, Last Name and Website, just like the regular WordPress Add New User form.
These fields are optional.

= Will each user I register be sent a confirmation email? =

The General Options above the registration list has an option to either send passwords to new users or not.
The default setting is to email usernames and passwords to all users but this can be disabled if desired.

= How does the 'Force Fill Email Addresses' work exactly and why shouldn't I use it? =

Force Fill Email Addresses works by creating a fake email for users whose email address field is left blank, utilising
the new user's Username and a .fake extension to create a non-functional, dummy email address. This is NOT a 
function I recommend using unless absolutely necessary. User email addresses in WordPress are highly recommended if only 
to retrieve a password if it is forgotten. In a particular real-world scenario I found I had to add a user who had no email 
address due to their lower socio-economic circumstance and it was unrealistic to deny access to a potential user because 
of this as since the site's purpose was tailored specifically for that demographic of user. The function is there if you
need it, but its not a recommended solution (try getting your user's free Gmail accounts or something in the first instance!).

= Can I customise the text in this email confirmation? =

Not yet. Currently the email provides a brief message regarding their being added to the site as a user, a link to the 
site url for the site in question, their username and password combination. Customisation feature will be available in a 
later version.

= Does the plugin keep a record of all the users I've added using the plugin? =

Not yet. This feature may be available in a later release.

= Can I delete multiple users using this plugin? =

You can already bulk delete users in the Users section of WordPress administration, so this is not planned for development.
I am thinking of a bulk edit of existing user data though, but its not the point of the plugin at this time.

== Screenshots ==

1. The basic layout of the user input form.

== Changelog ==

= 1.1.0 =
* Password strength notifications added for password fields that are not blank
* Validation error notice for usernames that include unsafe characters
* Strict sanitization option added to general options
* Username and email fields revalidated on change to general options
* Improved error notification states
* General options are now saved on form submission
* Bugs fixes

= 1.0.1 =
* Email validation bug fix

= 1.0.0 =
* Initial release.