<?php
/**
 * @package Add_Multiple_Users
 * @version 1.1.0
 */
/*
Plugin Name: Add Multiple Users
Plugin URI: http://www.happynuclear.com/sandbox/amu/add-multiple-users-for-wordpress.php
Description: This plugin allows you to add multiple user accounts to your Wordpress blog using a single form. When activated you should see the Add Multiple Users link under your Users menu.
Version: 1.1.0
Author: HappyNuclear
Author URI: http://www.happynuclear.com
License: GPLv2
*/

/*
This program is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 
*/

define('AMU_VERSION', '1.1.0');

//protect from direct call
if ( !function_exists( 'add_action' ) ) {
	echo "Access denied!";
	exit;
}

function amu_menu() {
	// add the admin options page
	add_users_page('Add Multiple', 'Add Multiple Users', 'manage_options', 'addmultiple', 'add_multiple_users');
}

function on_screen_validation() {
	wp_enqueue_script( "field_validation", plugins_url( "field-validation.js", __FILE__ ), array( 'jquery' ) );
	wp_localize_script( "field_validation", "MySecureAjax", array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}

function addmultiuser_style() {
	wp_register_style($handle = 'amu_css_style', $src = plugins_url('amustyle.css', __FILE__), $deps = array(), $ver = '1.1.0', $media = 'all');
    wp_enqueue_style('amu_css_style');
}

//MAIN FUNCTION
function add_multiple_users() {
	//test again for admin priviledges
	if (!current_user_can('manage_options') )  {
		wp_die( __('You do not have sufficient permissions to access this page.') );
	}
	
	//globals for functions
	global $current_user, $wpdb;
    get_currentuserinfo();
	$thisUserEmail = $current_user->user_email;
	$thisBlogName = get_bloginfo('name');
	$thisBlogUrl = site_url();
	$confirmationStack = '';
	
	//begin wrap class
	echo '<div class="wrap">';
	echo '<div id="amu">';

	//if submitted
    if ( isset($_POST['addnewusers'] ) ) {
		
		//GET GENERAL OPTION SETTINGS
		
		// 1. send users emails
		if ( isset($_POST['sendpswemails'] ) ) {
			$sendEmail = 'yes';
		} else {
			$sendEmail = 'no';
		}
		
		// 2. email to user...
		if ( isset($_POST['confirmEmail'] ) ) {
			$yesConfirm = 'yes';
		} else {
			$yesConfirm = 'no';
		}
		
		// 3. set all to this role
		$setAllRoles = $_POST['allToRole'];
		
		// 4. force fill email...
		if ( isset($_POST['forcefillemail'] ) ) {
			$forceEmail = 'yes';
		} else {
			$forceEmail = 'no';
		}
		
		// 5. validate filled email addresses...
		if ( isset($_POST['validatemail'] ) ) {
			$validateEmail = 'yes';
		} else {
			$validateEmail = 'no';
		}
		
		// 5. validate usernames with strict method...
		if ( isset($_POST['validateStrict'] ) ) {
			$validateStrict = 'yes';
		} else {
			$validateStrict = 'no';
		}
		
		//SAVE GENERAL OPTIONS
		update_option( 'amu_usernotify', $sendEmail );
		update_option( 'amu_confirmation', $yesConfirm );
		update_option( 'amu_setallroles', $setAllRoles );
		update_option( 'amu_validatestrict', $validateStrict );
		update_option( 'amu_validatemail', $validateEmail );
		update_option( 'amu_forcefill', $forceEmail );
		
		//BEGIN MESSAGE DISPLAY
		
		//wp-style feedback message
		echo '<div id="message" class="updated">';
		echo '<p><strong>New User User Accounts Processed!</strong></p>';
		echo '</div>';
		
		//information feedback
		echo '<h2>New User Information Added</h2>';
		echo '<p><span class="important">If you did not request this information emailed to you and you wish to save the information for future reference, please copy the New User Information section below IMMEDIATELY and save it for you records. It will not be available again if the Add New form is submitted again or you navigate away from this page.</span></br />';
		echo '<span class="important">If any of your users have not been added due to an error, please note these errors and use the Add Multiple Users form below to attempt these registrations again.</span></p>';		
		
		echo '<div class="regnotice">';
		echo '<h3>New User Information</h3>';
		
		//run through registration lines by email
		for ( $icounter = 1; $icounter <= 50; $icounter += 1 ) {
			
			//get username/password/email/additional vars
			$tempusername = trim($_POST['username'.$icounter]);
			$password = trim($_POST['password'.$icounter]);
			$email = trim($_POST['email'.$icounter]);
			$userRole = $_POST['roleSetter'.$icounter];
			$firstname = $_POST['firstname'.$icounter];
			$lastname = $_POST['lastname'.$icounter];
			$website = $_POST['website'.$icounter];
			$emailGen = 'valid';
			
			//if username is not blank
			if ($tempusername != '') {
				
				//process username
				if ($validateStrict == 'yes') {
					$username = sanitize_user( $tempusername, true );
				} else {
					$username = sanitize_user( $tempusername, false );
				}
				
				//check if username exists
				if ( username_exists( $username ) ) {
					
					$fail_userexists = '<p style="color:red;"><strong>'.$icounter.'. Error:</strong> The user <strong>'.$username. '</strong> already exists. Please try adding this user again with a different username.</p>';
					$confirmationStack = $confirmationStack.$fail_userexists;
					echo $fail_userexists;
			
				} else {
					//process password
					if ( $password == '' ) {
						//generate random password if blank
						$password = wp_generate_password();
					}
					
					//process email
					if ( $email == '' ) {
						
						if ( $forceEmail == 'no' ) {
							$fail_noemailadded = '<p style="color:red;"><strong>'.$icounter.'. Error:</strong>: No email address for the user <strong>'.$username.'</strong> was included. Please try adding this user again with a valid email address.</p>';
							$email = '';
							$confirmationStack = $confirmationStack.$fail_noemailadded;
							echo $fail_noemailadded;
						} else {
							//generate random email address
							$email = 'temp_'.$username.'@temp'.$username.'.fake';
							$emailGen = 'generated';
						 }
					} else {
						//validate entered password if set
						if ( $validateEmail == 'yes' ) {
							if ( !is_email( $email ) ) {
								$fail_emailnotvalid = '<p style="color:red;"><strong>'.$icounter.'. Error:</strong>: New user '.$username.'</strong> was not added because the email address provided <strong>'.$email.'</strong> for this user was not valid. Please try again using a valid email address or disable email verification.</p>';
								$email = '';
								$confirmationStack = $confirmationStack.$fail_emailnotvalid;
								echo $fail_emailnotvalid;
							}
						}
					}
					
					//VERIFY ALL DATA EXISTS THEN PROCESS
					if ( ( $username != '' ) && ( $password != '' ) && ( $email != '' ) ) {
						
						//confirmation of addition of user			
						$userSuccess = '<p>'.$icounter.'. <strong>Success!</strong> The user <strong>'.$username.'</strong> has been added. Email Address for this user is <strong>'.$email.'</strong>. Password for this user is <strong>'.$password.'</strong></p>';
						$confirmationStack = $confirmationStack.$userSuccess;
						echo $userSuccess;
						
						//create new wordpress user
						$newuser = wp_create_user( $username, $password, $email );
						$wp_user_object = new WP_User($newuser);
						
						//id of new user
						$newuserID = $wp_user_object->ID;
						
						//set user first name
						if ($firstname != '') {
							update_user_meta( $newuserID, 'first_name', $firstname );
						}
						
						//set user last name
						if ($lastname != '') {
							update_user_meta( $newuserID, 'last_name', $lastname );
						}
						
						//set user web site
						if ($website != '') {
							wp_update_user( array ('ID' => $newuserID, 'user_url' => $website) ) ;
						}
						
						//set user role
						if ($setAllRoles == 'notset') {
							$wp_user_object->set_role($userRole);
						} else {
							$wp_user_object->set_role($setAllRoles);
						}
						
						unset($wp_user_object);

						//send password to new user?
						if (($sendEmail == 'yes') && ($emailGen == 'valid')) {
							//set up email
							$to = $email;
							$subject = 'Your New User Account on '.$thisBlogName;
							$message = '<p>You have been registered on the '.$thisBlogName.' website.</p>
							<p>You may now log into the site at <a href="'.$thisBlogUrl.'">'.$thisBlogUrl.'</a></p>
							<p>Your username is <strong>'.$username.'</strong> and your password is <strong>'.$password.'</strong> </p>
							<p>Regards, <br>
							'.$thisBlogName.'</p>';
							
							$headers = 'From: '.$thisUserEmail.' <'.$thisUserEmail.'>' . "\r\n";
							//filter to create html email
							add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
							//send email
							wp_mail($to, $subject, $message, $headers);
						}
					}
				}
				
			} else { 
			//no username entered in this line, skip and move on
			}
		}//end for loop
		
		//SEND CONFIRMATION EMAIL TO LOGGED IN USER
		if ($yesConfirm == 'yes') {
			
			//set up confirmation email
			$confirmTo = $thisUserEmail;
			$confirmSubject = 'New User Account Information for '.$thisBlogName;
			$confirmMessage = '<p><strong>This email is to confirm new user accounts for your website generated using the Add Multiple Users plugin.</strong></p>
			<p>All errors have also been included for reference when re-entering failed registrations.</p>
			'.$confirmationStack.'
			<p><strong>End of message.</strong></p>';
			
			$confirmHeaders = 'From: '.$thisUserEmail.' <'.$thisUserEmail.'>' . "\r\n";
			//filter to create html email
			add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
			//send email
			wp_mail($confirmTo, $confirmSubject, $confirmMessage, $confirmHeaders);
			
			echo '<p class="important">This information has been emailed to your WordPress administrator email <'.$thisUserEmail.'> as requested.</span></p>';
		}
			
		//on-screen confirmation
		if ($sendEmail == 'yes') {
			echo '<p  class="important">All users have been sent their login information (except where random emails were force-created).</span></p>';
		}
			
		//close new user information div
		echo '</div>';
	//end of add multiple users function
	}
	
	//SHOW ADDING FORM
	
	echo '<h2>Add Multiple Users</h2>';
	echo '<p><strong>Use the form below to add up to 50 users at once to your WordPress site.</strong><br />';
	echo '<span class="important">Important - Please read the plugin information at the bottom of the page before first use.</span></p>';
	
	echo '<form method="post">';
	
	//general options
	echo '<div class="genoptionwrap">';
	echo '<div class="formhead">';
	echo '<p>General Options (will apply to all new users added below):</p>';
	echo '</div>';
	//send emails...
	echo '<div class="optionbox">';
	echo '	<label for="sendpswemails">Email each new user their username and password?</label>';
	echo '	<input name="sendpswemails" id="sendpswemails" type="checkbox" ';
	//retrieve option if set
	if (get_option('amu_usernotify')) {
		if ( get_option('amu_usernotify') == 'yes') {
			echo 'checked="checked"';
		}
	} else {
		echo 'checked="checked"';
	}
	echo ' value="send" />';
	echo '</div>';
	
	//send confirmation email
	echo '<div class="optionbox">';
	echo '	<label for="confirmEmail">Email me a complete list of new user account details?</label>';
	echo '	<input name="confirmEmail" id="confirmEmail" type="checkbox" ';
	//retrieve option if set
	if (get_option('amu_confirmation')) {
		if ( get_option('amu_confirmation') == 'yes') {
			echo 'checked="checked"';
		}
	} else {
		echo 'checked="checked"';
	}
	echo ' value="yesConfirm" />';
	echo '</div>';
	
	//validate emails...
	echo '<div class="optionbox">';
	echo '	<label for="validatemail">Validate entered email address format? <em>(uses WordPress "is_email" validation method)</em></label>';
	echo '	<input name="validatemail" id="validatemail" type="checkbox" ';
	//retrieve option if set
	if (get_option('amu_validatemail')) {
		if ( get_option('amu_validatemail') == 'yes') {
			echo 'checked="checked"';
		}
	} else {
		echo 'checked="checked"';
	}
	echo ' value="validate" />';
	echo '</div>';
	
	//username strict validation option...
	echo '<div class="optionbox">';
	echo '	<label for="validateStrict">Sanitize usernames using Strict method <em>(allows only alphanumeric and _, space, ., -, *, and @ characters)</em></label>';
	echo '	<input name="validateStrict" id="validateStrict" type="checkbox" ';
	//retrieve option if set
	if (get_option('amu_validatestrict')) {
		if ( get_option('amu_validatestrict') == 'yes') {
			echo 'checked="checked"';
		}
	}
	echo ' value="userstrict" />';
	echo '</div>';
	
	//force fill emails...
	echo '<div class="optionbox">';
	echo '	<label for="forcefillemail">Force Fill empty email addresses? <em>(not recommended - see bottom of page for details)</em></label>';
	echo '	<input name="forcefillemail" id="forcefillemail" type="checkbox" ';
	//retrieve option if set
	if (get_option('amu_forcefill')) {
		if ( get_option('amu_forcefill') == 'yes') {
			echo 'checked="checked"';
		}
	}
	echo ' value="fill" />';
	echo '</div>';
	
	//set all users to this role
	//retrieve set option value
	if (get_option('amu_setallroles')) {
		$rolesel = get_option('amu_setallroles');
	} else {
		$rolesel = 'notset';
	}
	echo '<div class="optionbox lastoption">';
	echo '	<label for="allToRole">Ignore individual User Role settings and set all new users to this role: </label>';
	echo '	<select name="allToRole" id="allToRole">';
    echo '		<option value="notset"'; if($rolesel=='notset'){echo ' selected="selected" ';} echo '>no, set individually...</option>';
    echo '		<option value="subscriber"'; if($rolesel=='subscriber'){echo ' selected="selected" ';} echo '>Subscriber</option>';
    echo '		<option value="contributor"'; if($rolesel=='contributor'){echo ' selected="selected" ';} echo '>Contributor</option>';
    echo '		<option value="author"'; if($rolesel=='author'){echo ' selected="selected" ';} echo '>Author</option>';
    echo '		<option value="editor"'; if($rolesel=='editor'){echo ' selected="selected" ';} echo '>Editor</option>';
    echo '		<option value="administrator"'; if($rolesel=='administrator'){echo ' selected="selected" ';} echo '>Administrator</option>';
	echo '	</select>';
	echo '</div>';

	echo '</div>';
	
	echo '<div class="formhead">';
	echo '<p>Your New User Details:</p>';
	echo '</div>';
	
	echo '<div class="fieldsetwrap">';
	
	for ( $counter = 1; $counter <= 50; $counter += 1) {
		
		if ($counter & 1) {
			echo '<div class="formwrap wrapwhite">';
		} else {
			echo '<div class="formwrap wrapgrey">';
		}
		
		echo '<div class="formline">';
		echo '	<span class="countline">'.$counter.'.</span>';
		echo '	<label for="username'.$counter.'">Username</label>';
		echo '	<input type="text" name="username'.$counter.'" id="username'.$counter.'" class="valusername validatefield" />';
		echo '	<label for="password'.$counter.'">Password</label>';
		echo '	<input type="text" name="password'.$counter.'" id="password'.$counter.'" class="valpassword validatefield" />';
		echo '	<label for="email'.$counter.'">Email</label>';
		echo '	<input type="text" name="email'.$counter.'" id="email'.$counter.'" class="valemail validatefield" />';
		echo '	<label for="roleSetter'.$counter.'">UserRole</label>';
		echo '	<select name="roleSetter'.$counter.'" id="roleSetter'.$counter.'">';
		echo '		<option value="faculty">Faculty</option>';
		echo '		<option value="student" selected="selected">Student</option>';
		echo '	</select>';
		echo '</div>';
		
		echo '<div class="formline">';
		echo '	<span class="countline">&nbsp;</span>';
		echo '	<label for="firstname'.$counter.'">FirstName</label>';
		echo '	<input type="text" name="firstname'.$counter.'" id="firstname'.$counter.'" />';
		echo '	<label for="lastname'.$counter.'">LastName</label>';
		echo '	<input type="text" name="lastname'.$counter.'" id="lastname'.$counter.'" />';
		echo '	<label for="website'.$counter.'">Website</label>';
		echo '	<input type="text" name="website'.$counter.'" id="website'.$counter.'" />';
		echo '</div>';
		
		echo '</div>';
	}
	
	echo '</div>';
	
	//add submit button
	echo '<div class="buttonline">';
	echo '	<input type="submit" name="addnewusers" id="addnewusers" class="button-primary" value="Add All Users" />';
	echo '	<input type="reset" name="clearform" id="clearform" class="button-primary" value="Reset Form" />';
	echo '</div>';
	
	echo '</form>';
	
	echo '<div class="infosection">';
	
	echo '<h2>Information about the Add Multiple Users plugin';
	
	echo '<h3>Information on form fields and settings</h3>
			<p><strong>Usernames:</strong><br />
			All new users must be given a unique username. Rows without a username are automatically skipped during the multiple registration process. Usernames cannot be changed once set. Usernames are automatically sanitized to strip out unsafe characters, but are not strictly sanitized.</p>
			<p><strong>Password:</strong><br />
			May be set for each user, or left blank to generate a random password for that user. Passwords are not validated for their strength nor validity (yet). For more information on password strength and security, please visit the <a href="http://codex.wordpress.org/Hardening_WordPress" target="_blank">Hardening Wordpress</a> page.</p>
			<p><strong>Email:</strong><br />
			An unique, valid email address for each user is required (or use Force Fill option if emails are not available, see below). Emails will be checked for uniqueness and, if selected, validity.</p>
			<p><strong>FirstName, LastName, Website:</strong><br />
			These parameters per user are optional and will be left blank if not filled in. Users or Administrators may update this information using the regular Wordpress user profile settings later. These fields are not validated.</p>
		';
			
	echo '<h3>Information on General Option settings</h3>
		<p><strong>Email each new user their username and password:</strong><br />
			If selected, automatically sends an email to each new registered user with their password, email, the name and host address of the website. Users who have been added with a "forced" email address will not be emailed (obviously).</p>
		<p><strong>Send me a complete list of new user account details:</strong><br />
			<em>Highly recommended.</em> When you submit the multiple registration form, the results of your registration will display on the screen. However, this information will not remain on the screen once you navigate away from the page or submit the form again. This option emails all this information to your registered WordPress user account email.</p>
		<p><strong>Validate entered email addresses:</strong><br />
			This setting affects both the in-page validation and the on-submit validation. It uses <a href="http://codex.wordpress.org/Function_Reference/is_email" target="_blank">WordPress "is_email" verification</a>. If you have trouble entering email addresses that you believe are valid, disable this option.</p>
			<p><strong>Sanitize usernames using Strict method:</strong><br />
			Determines whether usernames are sanitized with Strict method or not. Enabling this option disallows the use of many symbols that may be used in usernames normally. Affects both the on-screen validation and the on-submit validation. Get more info on <a href="http://codex.wordpress.org/Function_Reference/sanitize_user" target="_blank">user sanitization</a>.</p>
		<p><strong>Force Fill empty email addresses:</strong><br />
			<em>Highly NOT recommended.</em> This setting ignores empty email address fields that would normally cause that new user\'s registration to fail by creating a fake email address such as "temp_username@temp_username.info". It is very much recommended that all new users have a valid email address, and this function should only be used in cases where you need to register new users that do not have active email accounts.</p>
		<p><strong>Ignore individual User Role settings and set all new users to this role:</strong><br />
			Does exactly what it says... ignores the User Role option for each new user and sets them to the role you choose here.</p>
		';
	
	echo '<h3>More Information</h3>';
	echo '<p>Visit the plugin page at <a href="http://www.happynuclear.com/sandbox/amu/add-multiple-users-for-wordpress.php" target="_blank">http://www.happynuclear.com/sandbox/amu/add-multiple-users-for-wordpress.php</a> for more information on the Add Multiple Users plugin.';
		
	echo '</div>';
	echo '</div>';
	echo '</div><!-- end wrap -->';
}
//a call to action
add_action( 'admin_menu', 'amu_menu' );
add_action( 'admin_print_styles', 'addmultiuser_style' );
add_action( 'admin_print_scripts', 'on_screen_validation' );
add_action( 'wp_ajax_UserNameValidation', 'validateUserName' );
add_action( 'wp_ajax_EmailValidation', 'validateEmail' );

//validation functions
function validateUserName() {
    $username = trim($_POST['thevars']);
	$sanstrict = $_POST['sanstrict'];
	if ($sanstrict == 'yes') {
		$sanUsername = sanitize_user( $username, true );
	} else {
		$sanUsername = sanitize_user( $username, false );
	}
    if ( username_exists( $sanUsername ) ) {
		echo 'exists'; 
    } else {
		if ($sanUsername != $username) {
			echo 'badchars';
		} else {
			echo 'spare';
		}
    }
    exit;
}
function validateEmail() {
    $email = $_POST['thevars'];
	$shouldValidate = $_POST['isValidated'];
    if ($shouldValidate == 'yes') {
		if ( !is_email($email) ) {
			echo 'emailinvalid';
			exit;
		}
	}
	if ( email_exists($email) ) {
		echo 'exists'; 
    } else {
		echo 'spare';
    }
    exit;
}
?>