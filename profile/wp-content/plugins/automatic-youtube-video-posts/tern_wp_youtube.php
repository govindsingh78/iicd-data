<?php
/*
Plugin Name: Automatic Youtube Video Posts
Plugin URI: http://www.ternstyle.us/products/plugins/wordpress/wordpress-automatic-youtube-video-posts
Description: Add youtube video posts automatically whenever you add or upload a video to youtube.
Author: Matthew Praetzel
Version: 2.1.4
Author URI: http://www.ternstyle.us/
Licensing : http://www.ternstyle.us/license.html
*/

////////////////////////////////////////////////////////////////////////////////////////////////////
//
//		File:
//			tern_wp_youtube.php
//		Description:
//			This file initializes the Wordpress Plugin - Automatic YouTube Video Posts
//		Actions:
//			1) Add youtube video posts automatically whenever you add or upload a video to youtube.
//		Date:
//			Added on August 8th 2009
//		Version:
//			2.1.4
//		Copyright:
//			Copyright (c) 2010 Matthew Praetzel.
//		License:
//			This software is licensed under the terms of the GNU Lesser General Public License v3
//			as published by the Free Software Foundation. You should have received a copy of of
//			the GNU Lesser General Public License along with this software. In the event that you
//			have not, please visit: http://www.gnu.org/licenses/gpl-3.0.txt
//
////////////////////////////////////////////////////////////////////////////////////////////////////

/****************************************Commence Script*******************************************/

//                                *******************************                                 //
//________________________________** INCLUDES                  **_________________________________//
//////////////////////////////////**                           **///////////////////////////////////
//                                **                           **                                 //
//                                *******************************                                 //
require_once(dirname(__FILE__).'/conf.php');

/****************************************Terminate Script******************************************/
?>