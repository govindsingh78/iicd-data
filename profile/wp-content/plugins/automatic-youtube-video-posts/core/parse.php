<?php
////////////////////////////////////////////////////////////////////////////////////////////////////
//
//		File:
//			settings.php
//		Description:
//			This file compiles and processes the plugin's various settings pages.
//		Actions:
//			1) compile overall plugin settings form
//			2) process and save plugin settings
//		Date:
//			Added on November 3rd 2010
//		Copyright:
//			Copyright (c) 2010 Matthew Praetzel.
//		License:
//			This software is licensed under the terms of the GNU Lesser General Public License v3
//			as published by the Free Software Foundation. You should have received a copy of of
//			the GNU Lesser General Public License along with this software. In the event that you
//			have not, please visit: http://www.gnu.org/licenses/gpl-3.0.txt
//
////////////////////////////////////////////////////////////////////////////////////////////////////

/****************************************Commence Script*******************************************/

//                                *******************************                                 //
//________________________________** INITIALIZE VARIABLES      **_________________________________//
//////////////////////////////////**                           **///////////////////////////////////
//                                **                           **                                 //
//                                *******************************                                 //
$tern_wp_youtube_vids = array();
ini_set(max_execution_time,600);
//ini_set('memory_limit','40M');
//                                *******************************                                 //
//________________________________** ADD EVENTS                **_________________________________//
//////////////////////////////////**                           **///////////////////////////////////
//                                **                           **                                 //
//                                *******************************                                 //
if($GLOBALS['pagenow'] != 'admin-ajax.php') {
	add_action('init','WP_ayvpp_add_posts',10);
}
add_action('wp_insert_post','WP_ayvpp_add_meta');
//                                *******************************                                 //
//________________________________** POST FUNCTIONS            **_________________________________//
//////////////////////////////////**                           **///////////////////////////////////
//                                **                           **                                 //
//                                *******************************                                 //
function WP_ayvpp_add_posts($x=1,$n=20) {
	global $getWP,$getFIX,$tern_wp_youtube_options,$tern_wp_youtube_o,$tern_wp_users,$tern_wp_youtube_videos,$tern_wp_youtube_keys;
	$tern_wp_youtube_o = $getWP->getOption('tern_wp_youtube',$tern_wp_youtube_options);

	//no channels or WordPress category have been specified
	if(empty($tern_wp_youtube_o['user']) or !$tern_wp_youtube_o['category'] or empty($tern_wp_youtube_o['category']) or $tern_wp_youtube_o['category'] == '-1') {
		return;
	}
	
	//currently there is an import taking place
	if($tern_wp_youtube_o['is_importing'] and $tern_wp_youtube_o['is_importing'] !== false and $tern_wp_youtube_o['is_importing'] !== $r) {
		if($n == '*') {
			die('There is an import currently taking place. Please try again later.');
		}
		return;
	}
	
	//it is not yet time to import
	if(($tern_wp_youtube_o['last_import']+($tern_wp_youtube_o['cron']*3600)) > time() and !empty($tern_wp_youtube_o['last_import']) and $n != '*') {
		return;
	}
	
	//get all existing imported videos
	$tern_wp_youtube_videos = get_option('tern_wp_youtube_videos');
	$tern_wp_youtube_videos = is_array($tern_wp_youtube_videos) ? $tern_wp_youtube_videos : array();
	$tern_wp_youtube_keys = array_keys($tern_wp_youtube_videos);
	
	$x = empty($x) ? 1 : (int)$x;
	
	//keep other imports from happening
	if(!$r or $r !== $tern_wp_youtube_o['is_importing']) {
		$tern_wp_youtube_o['is_importing'] = $r = wp_create_nonce('ayvpp-'.time());
		$tern_wp_youtube_o = $getWP->getOption('tern_wp_youtube',$tern_wp_youtube_o,true);	
	}
	
	//get user accounts
	$tern_wp_users = explode(',',$tern_wp_youtube_o['user']);
	$tern_wp_users = $getFIX->removeEmptyValues($tern_wp_users,false);
	
	//import
	WP_ayvpp_add_import_posts($x,$n,$r);
	
	//finish import
	$getWP->getOption('tern_wp_youtube_videos',$tern_wp_youtube_videos,true);
	$tern_wp_youtube_o['is_importing'] = false;
	if($n != '*') {
		$tern_wp_youtube_o['last_import'] = time();
	}
	$getWP->getOption('tern_wp_youtube',$tern_wp_youtube_o,true);
	unset($GLOBALS['tern_wp_youtube_o'],$GLOBALS['tern_wp_youtube_videos'],$GLOBALS['tern_wp_youtube_keys'],$GLOBALS['tern_wp_users'],$GLOBALS['tern_wp_youtube_vids']);
	
	return true;
	
}
function WP_ayvpp_add_import_posts($x=1,$n=20,$r=false) {

	global $getWP,$getFILE,$tern_wp_users,$tern_wp_youtube_o,$tern_wp_youtube_videos,$tern_wp_youtube_keys,$tern_wp_youtube_vids,$tern_wp_youtube_post_defaults;

	//deal with memory issues
	if($n == '*' and !WP_ayvpp_check_memory()) {
		return false;
	}
	
	//perform import
	$y = 0;
	foreach((array)$tern_wp_users as $v) {
		$y++;
	
		unset($w,$c);
		
		$tern_wp_youtube_vids = array();
		WP_ayvpp_parse_videos($v,$x,$n);

		if(empty($tern_wp_youtube_vids) and $y >= count($tern_wp_users)) {
			return false;
		}
		elseif(empty($tern_wp_youtube_vids)) {
			continue;
		}
		$c = 0;
		foreach((array)$tern_wp_youtube_vids as $w) {

			unset($i,$k,$s,$t,$a,$d,$m);
		
			$w = $w['value'] ? $w['value'] : $w;
			$i = explode('/',$w['id']);
			$i = $i[count($i)-1];
			//$k = array_keys($tern_wp_youtube_videos);

			if(!in_array($i,$tern_wp_youtube_keys)) {

				$s = !empty($w['content']['value']) ? $w['content']['value'] : '';
				if(empty($s)) {
					$s = !empty($w['media:description']['value']) ? $w['media:description']['value'] : '';
				}
				$s = preg_replace('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)@',"<a href=\"$1\">$1</a>",$s);
				$s = explode(' ',$s);
				if($tern_wp_youtube_o['words'] and count($s) > $tern_wp_youtube_o['words']) {
					$s = array_merge(array_splice($s,0,$tern_wp_youtube_o['words']),array('<!--more-->'),$s);
				}
				$s = implode(' ',$s);
				
				$t = isset($w['media:group']['media:keywords']) ? $w['media:group']['media:keywords'] : '';
				
				$a = array(
					'post_title'					=>	$w['title']['value'],
					'post_content'					=>	$s,
					'_tern_wp_youtube_author'		=>	$w['author']['name'],
					'tags_input'					=>	$t,
					'post_date'						=>	gmdate('Y-m-d H:i:s',strtotime($w['published']))
				);
				
				$a['id'] = WP_ayvpp_insert_post(array_merge($a,$tern_wp_youtube_post_defaults),$i);
				if($a['id']) {
					if($n == '*') {
						$d = $getFILE->contents(WP_CONTENT_DIR.'/cache/ayvpp.txt');
						$d .= empty($d) ? (count($tern_wp_youtube_videos)+1).'. '.$w['title']['value'] : '<->'.(count($tern_wp_youtube_videos)+1).'. '.$w['title']['value'];
						//$m = memory_get_usage();
						//$d .= "<br /><ul class=\"tern_wp_youtube_output_list\"><li>memory limit: ".ini_get('memory_limit')."</li><li>memory usage: ".number_format($m).' bytes = '.number_format(round(($m/1024),2)).' KB = '.round(($m/1048576),2).' MB</li></ul>';
						$getFILE->createFile('ayvpp.txt',$d,WP_CONTENT_DIR.'/cache');
					}
					$tern_wp_youtube_keys[] = $i;
					$tern_wp_youtube_videos[$i] = $a;
				}
			}
			
			$c++;
		}
		
	}
	$getWP->getOption('tern_wp_youtube_videos',$tern_wp_youtube_videos,true);
	
	if($n == '*') {
		WP_ayvpp_add_import_posts($x+50,$n,$r);
	}
	
	return true;
	
}
function WP_ayvpp_check_memory() {

	$max = isset($_GET['memory']) ? (int)$_GET['memory'] : 32;

	$l = ini_get('memory_limit');
	preg_match("/[0-9]+/",$l,$m);
	$memory = (int)$m[0];
	$limit = (int)$m[0]*1048576;
	
	if(memory_get_usage() > ($limit-5242880)) {
		$memory += 5;
		if($memory <= $max) {
			ini_set('memory_limit',$memory.'M');
			$limit = $memory*1048576;
			return true;
		}
		return false;
	}
	
	return true;
}
function WP_ayvpp_insert_post($a,$i) {

	global $wpdb,$tern_wp_youtube_array;
	
	$tern_wp_youtube_array = array_merge($a,array('_tern_wp_youtube_video'=>$i,'_tern_wp_youtube_published'=>$a['post_date']));
	$p = wp_insert_post($a);
	
	unset($GLOBALS['wpdb']->last_result);
	
	/*
	global $wpdb,$getWP,$tern_wp_youtube_post_defaults,$tern_wp_youtube_array;
	$o = $getWP->getOption('tern_wp_youtube',$tern_wp_youtube_options);
	
	$s = $o['publish'] ? 'publish' : 'draft';
	$t = apply_filters("pre_post_title",$a['post_title']);
	$t = apply_filters("title_save_pre",$t);
	$c = apply_filters("pre_post_content",$a['post_content']);
	$c = apply_filters("content_save_pre",$c);
	
	$wpdb->query("insert into $wpdb->posts (post_author,comment_status,ping_status,post_status,post_title,post_content,post_date) values (".$o['author'].",'open','open','$s','$t','$c','".$a['post_date']."')");
	$p = (int)$wpdb->insert_id;
	
	wp_set_post_categories($p,array($o['category']));
	wp_set_post_tags($p,$a['tags_input']);
	
	$tern_wp_youtube_array = array_merge($a,array('_tern_wp_youtube_video'=>$i,'_tern_wp_youtube_published'=>$a['post_date']));
	WP_ayvpp_add_meta($p);
	*/
	
	return $p;
}
function WP_ayvpp_delete_posts($i) {
	global $getWP,$tern_wp_youtube_options,$tern_wp_youtube_fields;
	$o = $getWP->getOption('tern_wp_youtube',$tern_wp_youtube_options);
	$videos = $getWP->getOption('tern_wp_youtube_videos',array());
	$y = get_post_meta($i,'_tern_wp_youtube_video',true);
	if($videos[$y]) {
		$videos[$y]['id'] = false;
		foreach($tern_wp_youtube_fields as $v) {
			delete_post_meta($i,$v);
		}
	}
}
function WP_ayvpp_add_meta($i) {
	global $tern_wp_youtube_fields,$tern_wp_youtube_array;
	foreach($tern_wp_youtube_fields as $v) {
		if(!empty($tern_wp_youtube_array[$v]) or !empty($_POST[$v])) {
			$m = empty($tern_wp_youtube_array[$v]) ? $_POST[$v] : $tern_wp_youtube_array[$v];
			update_post_meta($i,$v,$m);
		}
	}
	unset($m);
	return true;
}
function WP_ayvpp_parse_videos($v,$i=1,$n=20) {
	global $tern_wp_youtube_vids;

	$z = $n == '*' ? 50 : $n;
	
	$c = new tern_curl();
	$r = $c->get(array(
		'url'		=>	'http://gdata.youtube.com/feeds/api/users/'.$v.'/uploads?orderby=published&max-results='.$z.'&start-index='.$i,
		'options'	=>	array(
			'RETURNTRANSFER'	=>	true,
			'FOLLOWLOCATION'	=>	true
		)
	));
	
	$x = new ternXML;
	$a = $x->parse($r->body);
	
	if(empty($a['feed']['value']['entry'])) {
		return;
	}
	else {
		$a = $a['feed']['value']['entry'];
	}
	
	if($a['id']) { 
		$a = array($a);
	}
	
	$tern_wp_youtube_vids = array_merge((array)$tern_wp_youtube_vids,$a);
	
	unset($c,$r,$x,$a);
	
	return true;
}
//                                *******************************                                 //
//________________________________** SOCIAL                    **_________________________________//
//////////////////////////////////**                           **///////////////////////////////////
//                                **                           **                                 //
//                                *******************************                                 //

//Twitter changed to OATH. For now the twitter features are no longer available.
/*
function WP_ayvpp_twitter_status($i) {
	global $getWP,$tern_wp_youtube_options;
	$o = $getWP->getOption('tern_wp_youtube',$tern_wp_youtube_options);
	
	if(empty($o['twitter_text'])) {
		$o['twitter_text'] = $tern_wp_youtube_options['twitter_text'];
	}
	
	if($o['twitter'] and !empty($o['twitter_user']) and !empty($o['twitter_password'])) {
		$t = new MyTwitter($o['twitter_user'],$o['twitter_password']);
		if($t) {
			$t->updateStatus(str_replace('%twitter_link%',get_permalink($i),$o['twitter_text']));var_dump($t);die();
		}
	}
}
*/

/****************************************Terminate Script******************************************/
?>