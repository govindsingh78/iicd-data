=== Automatic Youtube Video Posts Plugin ===
Contributors: mpraetzel
Donate link: http://www.ternstyle.us/donate
Tags: youtube, youtube videos, videos, embed videos, embed youtube videos, video posts
Requires at least: 2.8
Tested up to: 3.0.5
Stable tag: 2.1.4

The Automatic Youtube Video Posts Plugin automatically creates posts for YouTube videos from any YouTube account.

== Description ==

The Automatic Youtube Video Posts Plugin automatically imports YouTube videos from any YouTube account, creates posts for them and publishes them or creates a post draft according to your settings. You are also able to automatically update your twitter status posting the new Wordpress video post using text you specify.

NO CODE REQUIRED!

* Homepage for this plugin: `http://www.ternstyle.us/products/plugins/wordpress/wordpress-automatic-youtube-video-posts`
* Documentation: `http://www.ternstyle.us/products/plugins/wordpress/wordpress-automatic-youtube-video-posts/automatic-youtube-video-posts-documentation`
* Working example: `http://www.ternstyle.us/products/plugins/wordpress/wordpress-automatic-youtube-video-posts/wordpress-automatic-youtube-video-posts-plugin-demo`
* Change Log: `http://www.ternstyle.us/products/plugins/wordpress/wordpress-automatic-youtube-video-posts/wordpress-automatic-youtube-video-posts-change-log`

== Installation ==

To install the Automatic YouTube Video Posts plugin simply:

* Unpack the downloaded zipped file
* Upload the "automatic-youtube-video-posts" folder to your /wp-content/plugins directory
* Log into Wordpress
* Go to the "Plugins" page
* Activate the Automatic YouTube Video Posts Plugin

You're all set! Now you just have to change some Settings for the plugin.

To change your settings please:

* Log into Wordpress
* Open the "Youtube Posts" tab (which is located at the bottom of the vertical navigation on the left of your Wordpress administration panel.
* Click on the "Settings" option in the "Youtube Posts" tab.

== Features ==

* Automatically import YouTube videos into Wordpress Posts from a specified YouTube account or accounts
* Publish posts automatically or save them as drafts for review
* Add a YouTube video and video meta to any existing post.
* List your YouTube video posts on any page with one line of code.
* Import up to 1000 videos from your channels.
* Set the dimensions for your videos.
* Categorize your video posts.

== Resources ==

* Homepage for this plugin: `http://www.ternstyle.us/products/plugins/wordpress/wordpress-automatic-youtube-video-posts`
* Documentation: `http://www.ternstyle.us/products/plugins/wordpress/wordpress-automatic-youtube-video-posts/automatic-youtube-video-posts-documentation`
* Working example: `http://www.ternstyle.us/products/plugins/wordpress/wordpress-automatic-youtube-video-posts/wordpress-automatic-youtube-video-posts-plugin-demo`
* Change Log: `http://www.ternstyle.us/products/plugins/wordpress/wordpress-automatic-youtube-video-posts/wordpress-automatic-youtube-video-posts-change-log`

== Frequently Asked Questions ==

= How do I display the thumbnail for each video in my post lists? =

Add the following code wherever you'd like the image to appear: `<?php tern_wp_youtube_image(); ?>`

== Screenshots ==

== Changelog ==

`http://www.ternstyle.us/products/plugins/wordpress/wordpress-automatic-youtube-video-posts/wordpress-automatic-youtube-video-posts-change-log`

== Upgrade Notice ==
