<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<style type="text/css">
html {
margin-top: 0px !important;
}

#parallax {
	    
    height: 300px;
	background:url(./wp-content/themes/twentyeleven/images/px/bg.png) center;
    overflow: hidden;
    position: relative;
    width: 100%;
	z-index:3
}

#parallax img {
	position: absolute;
}

.trees { position: relative; top: 40px; }
.mountains { position: relative; top: 55px; }
#logo-container {
	height: auto;
overflow: hidden;
float: left;
margin-top: 6px;
position: absolute;
z-index: 4;
left: 50%;
    margin-left: -635px;
}




</style>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

	?></title>
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	 wp_enqueue_script("jquery");
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
 
 wp_head();
?>

				
<script type="text/javascript" src="./wp-content/themes/twentyeleven/js/jquery.event.js"></script>
<script type="text/javascript" src="./wp-content/themes/twentyeleven/js/parallax.js"></script>
<script type="text/javascript">

jQuery(document).ready(function($) {
	$('#parallax img').parallax({}, { yparallax: "false" }, {}, {}, { yparallax: "false" }); 
});
</script>


</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed">
	<header id="branding" role="banner">
			<hgroup>
				<h1 id="site-title"><span><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span></h1>
				<h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
			</hgroup>

			<?php
				// Check to see if the header image has been removed
				$header_image = get_header_image();
				if ( ! empty( $header_image ) ) :
			?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" style="background: none repeat scroll 0 0 #FFFFFF; clear: both; float: left; height: 300px; position: relative; width: 100%;" >
				<?php
					// The header image
					// Check if this is a post or page, if it has a thumbnail, and if it's a big one
					if ( is_singular() &&
							has_post_thumbnail( $post->ID ) &&
							( /* $src, $width, $height */ $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array( HEADER_IMAGE_WIDTH, HEADER_IMAGE_WIDTH ) ) ) &&
							$image[1] >= HEADER_IMAGE_WIDTH ) :
						// Houston, we have a new header image!
						echo get_the_post_thumbnail( $post->ID, 'post-thumbnail' );
					else : echo"<div id='parallax'>
									
									<div style='float: right; height: 100%; position: absolute; width: 100%; height:300px; z-index:8;' > 
										<div class='text' style='background: url(./wp-content/themes/twentyeleven/images/px/blackcolor.png);bottom: 0; height: 300px; left: 50%; margin-bottom: 0; margin-left: -630px; position: absolute; width: 1306px; z-index: 6;'></div>
										<div class='text' style='background: none repeat scroll 0 0 #000000; bottom: 0; height: 300px;left: 50%; margin-bottom: 0;margin-left: -960px;  position: absolute;  width: 357px; z-index: 2;'></div>
										<div class='text' style='background: none repeat scroll 0 0 #000000; bottom: 0; height: 300px; margin-bottom: 0; margin-right: -1015px; position: absolute; right: 50%; width: 383px; z-index: 2;'></div>
									</div>
									<div style='float: right; height: 100%; position: absolute; width: 600px; height:200px; left:40px'> 
									</div>
									<div style='float: right; position: absolute; width:130px; left:50%; margin-left:-475px; height:auto; z-index:1'> 
										<img class='text' src='./wp-content/themes/twentyeleven/images/px/Untitled-1.png' alt='' style=' float: left; width:120px; height:auto; position: absolute; z-index: 0;' />	
									</div>
									<div style='float: right; position: absolute; width:400px; left:50%; height:300px; margin-left:-100px; z-index:4'> 
										<img class='text' src='./wp-content/themes/twentyeleven/images/px/Tree1.png' style='float: left; width:265px; height:300px; position: absolute; z-index: 0;' />	
									</div>
									<div style='float: right; position: absolute; width:300px; left:50%; height:250px; margin-left:196px; z-index:3'> 
										<img class='text' src='./wp-content/themes/twentyeleven/images/px/Tree2side.png' style='float: left; width:250px; height:250px; position: absolute; z-index: 0;' />	 								
									</div>
									<div style='float: right; position: absolute; width:415px; left:50%; height:300px; margin-left:-250px; z-index:3'> 
										<img class='text' src='./wp-content/themes/twentyeleven/images/px/Tree2.png' style='float: left; width:365px; height:300px; position: absolute; z-index: 0;' />	 
									</div>
									<div style='float: right; position: absolute; width:126px; left:50%; height:150px; margin-left:-350px; z-index:2'> 
										<img class='text' src='./wp-content/themes/twentyeleven/images/px/Tree3.png' style='float: left; width:auto; height:150px; width:120px; position: absolute; z-index: 0;'></img>	 
									</div>
									<div style='bottom: 0.5px; float: right; height: 76px; left: 50%; margin-left: 200px; position: absolute; width: 334px; z-index: 6;'> 
										<img class='text' src='./wp-content/themes/twentyeleven/images/px/lotus&water.png' style='float: left; width:auto; height:80px; width:334px; height: 76px; position: absolute; z-index: 0;' />	 
									</div>
									<div style='float: left; position: absolute; width:1300px; left:50%; margin-left:-620px; height:300px; z-index:0'> 
										<img class='text' src='./wp-content/themes/twentyeleven/images/px/bg.png' style='float: right; width:1350px;  height:300px; position: absolute; z-index: 0;' />	 								
									</div>
									<div style='float: left; position: absolute; width:150px; left:50%; margin-left:-80px; height:150px; z-index:3; margin-top:180px;' > 
										<img class='text' src='./wp-content/themes/twentyeleven/images/px/bicycle.png' style='float: right; width:150px;  height:150px;  position: absolute; z-index: 0;' />	 								
									</div>
									
							</div>";
 endif; // end check for featured image or standard header ?>
			</a>
			<?php endif; // end check for removed header image ?>

			


	</header><!-- #branding -->






<nav id="access" role="navigation">
				<h3 class="assistive-text"><?php _e( 'Main menu', 'twentyeleven' ); ?></h3>
				<?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff. */ ?>
				<div class="skip-link"><a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to primary content', 'twentyeleven' ); ?>"><?php _e( 'Skip to primary content', 'twentyeleven' ); ?></a></div>
				<div class="skip-link"><a class="assistive-text" href="#secondary" title="<?php esc_attr_e( 'Skip to secondary content', 'twentyeleven' ); ?>"><?php _e( 'Skip to secondary content', 'twentyeleven' ); ?></a></div>
				<?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu. The menu assiged to the primary position is the one used. If none is assigned, the menu with the lowest ID is used. */ ?>
				<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                
                
                <?php
				// Has the text been hidden?
				if ( 'blank' == get_header_textcolor() ) :
			?>
				<div class="only-search<?php if ( ! empty( $header_image ) ) : ?> with-image<?php endif; ?>">
                <?php if ( is_user_logged_in() ) {?>
                
				<a href="wp-admin/profile.php" style="left: 50%; margin-left: 225px; position: absolute;">Edit Profile</a><a href="wp-login.php?action=logout" style="left: 50%; margin-left: 343px; position: absolute;">Log out</a>
                <?php }?>
				</div>
			<?php
				else :
			?>
            	<?php if ( is_user_logged_in() ) {
					$current_user = wp_get_current_user();
					?>
                		
				<div style="left: 50%; margin-left: 50px; position: absolute;"><a style="position:relative;float:left; <?php if($page_id == 0) { echo "color:#D6007C"; } ?> " href="index.php" >Jungle Jalebi</a> <a style=" float: left;letter-spacing: 1px; line-height: 17px; max-width: 120px; position: relative; text-align: center;"><?php echo "Logged in as ".$current_user->display_name; ?></a><a style="position:relative;float:left" href="wp-admin/profile.php" target="_blank">Profile</a><a style="position:relative;float:left" href="wp-login.php?action=logout" >Log out</a>
                <?php }
				else
				{
					
				?>
                	
				<div style="left: 50%; margin-left: 400px; position: absolute;">
                <?php 	
				}
				?>
			<?php endif; ?>
			</nav><!-- #access -->
            
            
            
	<div id="main">
    
    