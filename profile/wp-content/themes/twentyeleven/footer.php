<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
<style>
.center
{
    height: auto;
    padding: 0;
    text-align: left;
    width: 50%;	
	float: left;
    margin-top: 45px;
    position: absolute;
	color:#FFF;
	text-transform:uppercase;
	font-size:10px;
	letter-spacing:1px
}
</style>

	</div><!-- #main -->

	<footer id="colophon" role="contentinfo">

			<?php
				/* A sidebar in the footer? Yep. You can can customize
				 * your footer with three columns of widgets.
				 */
				get_sidebar( 'footer' );
			?>

			<div id="site-generator">
				<?php do_action( 'twentyeleven_credits' ); ?>
				<a href="<?php echo esc_url( __( '', 'twentyeleven' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'twentyeleven' ); ?>" rel="generator"></a>
			</div>
            
            
            <h5 class="center"> &copy; <?php echo " IICD " ?> - Designed and Developed by <a href="http://mediashala.com" style="color:#FFF; text-transform:uppercase; font-size:10px; 	letter-spacing:1px">Mediashala</a>
			</h5>
			
            <div style="width:100%; background:url(./wp-content/themes/twentyeleven/images/footer1.png); margin-bottom:0px; height:68px"/>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>