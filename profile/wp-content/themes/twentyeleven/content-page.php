<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title">
        	<?php if(function_exists('jBreadCrumbAink')) { echo jBreadCrumbAink(); } ?>
        </h1>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php 
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------							
global $wp_query;
$thePostID = $wp_query->post->ID;
$postdata = get_post($thePostID, ARRAY_A);
$authorID = $postdata['post_author'];
$user_info=get_userdata($authorID);
global $post;
if($authorID!=1 || $authorID!=1)

{
	?> 
    
    
    <div style=" font-family: Droid-serif; font-size: 22px; font-style: italic;  margin-top: 0; width: 100%; margin-bottom:20px">
	<?php echo get_the_title($post->post_parent); ?>
	</div>



<div class="personal-avatar-main">
   <div class="avatar personal-avatar">
        <a href="<?php echo get_page_link( $thePostID );?>">
			<?php echo get_avatar($user_info->user_email , apply_filters( 'twentyeleven_status_avatar', '120' ) ); ?>
        </a>
        <?php 
		$current_user = wp_get_current_user();
		if ( get_the_author_meta( 'ID' ) == $current_user->ID ) 
		echo"<a href='./wp-admin/profile.php' style='width:100%;'>Edit Profile</a>"; 
		?>  
   </div>
  
   <div class="personal-detail">
    	 <h2 class="personal-detail-id">
         	<?php if($user_info->user_nicename!='' )echo $user_info->user_nicename; ?>
         </h2>
         <h2 class="personal-detail-name">
         	<?php if($user_info->user_firstname!='' )echo $user_info->user_firstname; ?>
            <?php if($user_info->user_lastname!='' )echo " ".$user_info->user_lastname; ?>        
		 </h2>
         <h2 class="personal-detail-email">
            <?php if($user_info->yim!='' )echo "<div class='autohyperlink' style='color: #5F5F5F; font-size: 13px; letter-spacing: 1px; text-decoration: none; margin-left:6px'>".$user_info->yim."</div>"; ?>
         </h2>
         <?php if($user_info->user_description!='' ):?>
         <h2 class="personal-detail-desc">
            <?php if($user_info->user_description!='' )echo "\" ".$user_info->user_description."\""; ?>
         </h2>
         <?php endif; ?>
    </div>
</div>
<?php	
}
else
{
	
	?>
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>

	<div style=" font-family: Droid-serif; font-size: 22px; font-style: italic;  margin-top: 0; width: 100%; margin-bottom:20px">
	<?php the_title(); ?>
	</div>

<?php
global $post;

	$mypages = get_pages( array( 'child_of' =>  $post->ID, 'sort_column' => 'post_name', 'sort_order' => 'asc','parent' => -1,'exclude_tree' => 0,) );
        
	foreach( $mypages as $p ) {		
		$content = $p->post_content;
		

		$content = apply_filters( 'the_content', $content );
	
global $wp_query;

$postdata = get_post( $p->ID, ARRAY_A);
$authorID = $postdata['post_author'];
$user_info = get_userdata($authorID);


?>


<div class="profile-list-main">
   <div class="avatar profile-avatar">
         <a href="<?php echo get_page_link( $p->ID ); ?>" ><?php echo get_avatar($user_info->user_email , apply_filters( 'twentyeleven_status_avatar', '65' ) ); ?></a>
   </div>
   <div class="profile-id">
         <h2>
               <a href="<?php echo get_page_link( $p->ID ); ?>"><?php echo $p->post_title; ?></a>
         </h2>
    </div>
    <div class="profile-name">
         <h2>
                     <?php if($user_info->user_firstname!='' )echo $user_info->user_firstname; ?>
                     <?php if($user_info->user_lastname!='' )echo " ".$user_info->user_lastname; ?>
         </h2>
     </div>
     <div class="profile-email">
        <?php if($user_info->user_lastname!='' )echo "<div class='autohyperlink' style='text-decoration:none;'> ".$user_info->yim."</div>" ?>
     </div>
    
</div>

<?php	} ?>

<!---------------------------------------------------------------------------------------------------------------------------------------------------------------------->

	
	
	
	
	<?php
		
}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			the_content(); 
			wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
	<footer class="entry-meta">
		
	</footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->















