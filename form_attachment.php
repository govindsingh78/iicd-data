<?php 
  session_start();
  include 'meekrodb.2.3.class.php';

  $query = "select * from user_details where user_id = '".$_SESSION['user_id']."'";
  $row = DB::queryFirstRow($query);

  $base_url = 'https://www.iicd.ac.in/';

  $photo_src = $base_url.'images/default.png';
  if($row['applicant_photo'] !=''){
    $photo_src = $base_url.'images/'.$row['applicant_photo'];
  }

  $dob_src = $base_url.'images/default.png';
  if($row['dob_certificate'] !=''){
    $dob_src = $base_url.'images/'.$row['dob_certificate'];
  }

  $id_src = $base_url.'images/default.png';
  if($row['id_proof'] !=''){
    $id_src = $base_url.'images/'.$row['id_proof'];
  }

  $sig_src = $base_url.'images/default.png';
  if($row['signatures'] !=''){
    $sig_src = $base_url.'images/'.$row['signatures'];
  }

?>
<form id="form_attach" name="form_attach">
  <br><br>
  <div class="group">
    <div class="col-md-4"> 
      <div id="error_file_name"></div>
    </div>  
  </div>
  <div class="group">
  </div>
  <div class="group">
    <div class="col-md-3"> 
      <label class="my-label" style="font-size: 10px">Photo of the applicant (approx size(360*450 px))</label>
        <p class="pho-txt"> File size limit to 4MB, Supported file format JPG, JPEG. </p> 
         <input type='file' id="imgInp1" name="applicant_photo" accept="image/jpg, image/jpeg*" />
          <div id='img_contain1'>
            <img id="blah1" align='middle' src="<?=$photo_src?>"/>
          </div>
       
  </div>     
  <div class="col-md-3"> 
    <label class="my-label" style="font-size: 10px">Date of birth <span> ( class 10th certificate)</span></label>
       <p class="pho-txt"> File size limit to 4MB, Supported file format JPG, JPEG. </p> 
         <input type='file' id="imgInp2" name="dob_certificate" accept="image/jpg, image/jpeg"/>
          <div id='img_contain2'>
            <img id="blah2" align='middle' src="<?=$dob_src?>"/>
          </div>
        
  </div>

  <div class="col-md-3"> 
     <label class="my-label" style="font-size: 10px">Photo Identity cum Address Proof</label>
        <p class="pho-txt"> (Acceptable forms of photo identification types are limited to Driver’s License, Passport, Voter ID, UID-Aadhar Card or Identity card of the previous school attended). <br> File size limit to 4MB, Supported file format JPG, JPEG. </p> 
         <input type='file' id="imgInp3" name="id_proof" accept="image/jpg, image/jpeg"/>
          <div id='img_contain3'>
            <img id="blah3" align='middle' src="<?=$id_src?>"/>
          </div>
  </div> 
   <div class="col-md-3"> 
     <label class="my-label" style="font-size: 10px">Signature of the candidate</label>
         <p class="pho-txt"> File size limit to 4MB, Supported file format JPG, JPEG. </p> 
         <input type='file' id="imgInp4" name="signatures" accept="image/jpg, image/jpeg"/>
          <div id='img_contain4'>
            <img id="blah4" align='middle' src="<?=$sig_src?>"/>
          </div>
  </div>                           
        
  </div>
          
<nav class="form-section-nav">
  <input type="hidden" name="action" id="action" value="save_attach">
  
  <input type="hidden" name="hidden_photo" id="hidden_photo" value="<?=$row['applicant_photo']?>">
  <input type="hidden" name="hidden_id" id="hidden_id" value="<?=$row['id_proof']?>">
  <input type="hidden" name="hidden_dob" id="hidden_dob" value="<?=$row['dob_certificate']?>">
  <input type="hidden" name="hidden_sig" id="hidden_sig" value="<?=$row['signatures']?>">
  <input type="hidden" name="file_extension_allow" id="file_extension_allow" value="1">
  
  <span id="btn_back_attach" class="btn-secondary form-nav-prev">  <img src="images/left-arrow.jpg" alt="left"> Prev</span>
  <span id="btn_next_attach" class="btn-std form-nav-next"> Save & Next <img src="images/right-arrow.jpg" alt="left"></span>
</nav>
</form>
<style type="text/css">
  #error_file_name{
    color: red;
    font-size: 12px;
  }
</style>

<script type="text/javascript">
$(document).ready(function(){

    $("#imgInp1").change(function () {
      var fsize = this.files[0].size;
      check_file_type($(this).val(),fsize);
    });

    $("#imgInp2").change(function () {
      var fsize = this.files[0].size;
      check_file_type($(this).val(),fsize);
    });

    $("#imgInp3").change(function () {
      var fsize = this.files[0].size;
      check_file_type($(this).val(),fsize);
    });

    $("#imgInp4").change(function () {
      var fsize = this.files[0].size;
      check_file_type($(this).val(),fsize);
    });

    $("#btn_back_attach").unbind().click(function() {
      $('#specialization_container').load('form_specialization.php',function(e){
         $("#attachment_container" ).slideUp( "slow");
         $('#attachment_container').html('');
         $("#specialization_container" ).slideDown( "slow");
      });
    });

    $("#btn_next_attach").unbind().click(function() {
        
        var hidden_photo = $('#hidden_photo').val();
        var hidden_dob = $('#hidden_dob').val();
        var hidden_id = $('#hidden_id').val();
        var hidden_sig = $('#hidden_sig').val();

        if(hidden_photo =='' || hidden_dob =='' || hidden_id =='' || hidden_sig ==''){
            if(!$('#form_attach').valid()){
              return false;
            }
        }

        if($('#file_extension_allow').val()==0){
          return false; 
        }
        
        var formData = new FormData($('form#form_attach')[0]);
        $.ajax({
            type: "POST",
            url:"admission-save.php",
            data:  formData,
            dataType: "json",
            contentType: false,
            cache: false,

            processData:false,
            success: function(response) {
              if(response.status == 1){
                $('#declaration_container').load('form_declaration.php',function(e){
                  $("#attachment_container" ).slideUp( "slow");
                  $('#attachment_container').html('');
                  $("#declaration_container" ).slideDown( "slow");
                });
                
              }
            }
        });
    });


    $('#form_attach').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules: 
        {
          "applicant_photo": {
            required: true
                  
          },
          "dob_certificate": {
            required: true          
          },
          "id_proof": {
            required: true            
          },
          "signatures": {
            required: true          
          }

        },
        messages: 
        {
         "applicant_photo": {
            required: "Applicant photo is required"
          },
          "dob_certificate": {
            required: "DOB certificate is required"
          },
          "id_proof": {
            required: "ID proof is required"
          },
          "signatures": {
            required: "ID proof is required"
          }
        }
  });

   

function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah1').attr('src', e.target.result);

      $('#blah1').hide();
      $('#blah1').fadeIn(650);

    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp1").change(function() {
  readURL(this);
});

function readURL2(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah2').attr('src', e.target.result);

      $('#blah2').hide();
      $('#blah2').fadeIn(650);

    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp2").change(function() {
  readURL2(this);
});


function readURL3(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah3').attr('src', e.target.result);

      $('#blah3').hide();
      $('#blah3').fadeIn(650);

    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp3").change(function() {
  readURL3(this);
});

function readURL4(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah4').attr('src', e.target.result);

      $('#blah4').hide();
      $('#blah4').fadeIn(650);

    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp4").change(function() {
  readURL4(this);
});

function check_file_type(file_ext,fsize){

  var errorMsg = '';

  var fileExtension = ['jpeg','jpg'];

  if(fsize > 2097152){
    errorMsg = "File size limit 4mb";
  }

  
  if($.inArray(file_ext.split('.').pop().toLowerCase(), fileExtension) == -1 ) {
    
    if(errorMsg !=''){
      errorMsg = errorMsg+", Supported file formats: jpg,jpeg";

    }else{
      errorMsg = "Supported file formats: jpg,jpeg";
    }
  }

  if(errorMsg !=''){

    $('#error_file_name').html(errorMsg); 
    $('#file_extension_allow').val(0);
    return false;
  
  }else{
    
    $('#error_file_name').html(''); 
    $('#file_extension_allow').val(1);
  }

} 


});
</script>