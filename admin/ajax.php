<?php 
    require_once('../meekrodb.2.3.class.php');
?>
<?php

if(isset($_POST['action']) && !empty($_POST['action']))
{
		$Action = $_POST['action'];
	if($Action == 'users')
	{
		// how many rows to show per page
    	$rowsPerPage = 20;

  	    // by default we show first page
    	$pageNum = 1;
		if(!empty($_POST['page']) && (int)$_POST['page'] > 0){
	    	$pageNum = (int)$_POST['page'];
	  	} else {
	    	$pageNum =  1;
		}            
	  	// counting the offset
		   $offset = ($pageNum - 1) * $rowsPerPage;

        $sqlquery   =    "SELECT users.id AS UserID, users.*, user_details.* FROM users";

        $sqlquery   =    $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";

         $sqlquery   =    $sqlquery . " WHERE email_verified = 1";

        if(isset($_POST['first_name'] ) && $_POST['first_name'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND users.first_name LIKE '%".$_POST['first_name']."%' ";
        }
        if(isset($_POST['email']) && $_POST['email']  != "")
        {            
            $sqlquery   =    $sqlquery . " AND users.email LIKE '%".str_replace(' ', '', $_POST['email'])."%' ";
        }
        if(isset($_POST['phone']) && $_POST['phone'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND user_details.phone LIKE '%".$_POST['phone']."%' ";
        }
        if(isset($_POST['payment_status']) && $_POST['payment_status']  != "" )
        {            
            
            if ($_POST['payment_status']  == "notdone") {
                 $sqlquery   =    $sqlquery . " AND users.payment_status IS NULL";
            }else{

            $sqlquery   =    $sqlquery . " AND users.payment_status = '".$_POST['payment_status']."'";
            }
        }
        if(isset($_POST['status']) && $_POST['status']  != "" )
        {            
          $sqlquery   =    $sqlquery . " AND users.status = '".$_POST['status']."'";
        }
        if($_POST['from_date'] != "" && $_POST['to_date']  != "" )
        {            
            $from = date('Y-m-d H:i:s',strtotime($_POST['from_date']));
            $to = date('Y-m-d H:i:s',strtotime($_POST['to_date']));
            $sqlquery   =    $sqlquery . " AND users.updated_at BETWEEN '".$from."' AND '".$to."'";
        }

        if( $_POST['Programme'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND user_details.Programme = '".$_POST['Programme']."' ";
        }

        if($_POST['dob'] != "")
        {            
            $dob = date('Y-m-d H:i:s',strtotime($_POST['dob']));
            $sqlquery   =    $sqlquery . " AND users.dob LIKE '".$dob."' ";
        }

        if( $_POST['gender'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND user_details.gender = '".$_POST['gender']."' ";
        }
        if( $_POST['marital_status'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND user_details.marital_status = '".$_POST['marital_status']."' ";
        }

        if( $_POST['category'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND user_details.category = '".$_POST['category']."' ";
        }
        if( $_POST['nationality'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND user_details.nationality = '".$_POST['nationality']."' ";
        }
        if($_POST['domicile'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND user_details.domicile = '".$_POST['domicile']."' ";
        }
        if($_POST['exam_center1'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND user_details.exam_center1 = '".$_POST['exam_center1']."' ";
        }
        if( $_POST['exam_center2'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND user_details.exam_center2 = '".$_POST['exam_center2']."' ";
        }
        if( $_POST['exam_center3'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND user_details.exam_center3 = '".$_POST['exam_center3']."' ";
        }
        if( $_POST['twelft_pass_year'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND user_details.twelft_pass_year = '".$_POST['twelft_pass_year']."' ";
        }
        if( $_POST['specialization_choice1'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND user_details.specialization_choice1 = '".$_POST['specialization_choice1']."' ";
        }

        if( $_POST['specialization_choice2'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND user_details.specialization_choice2 = '".$_POST['specialization_choice2']."' ";
        }
        if( $_POST['specialization_choice3'] != "")
        {            
            $sqlquery   =    $sqlquery . " AND user_details.specialization_choice3 = '".$_POST['specialization_choice3']."' ";
        }

         $sqlquery   =    $sqlquery . " ORDER BY users.id DESC LIMIT $offset, $rowsPerPage";

        $sqlquery = DB::query($sqlquery);

        $counter = DB::count();

		if ($counter > 0) 
        { 

        	/*if ($pageNum>1) {
        		$intcnt=$pageNum.'1';
        	}else{
        		$intcnt=1;
        	}*/


        	 $intcnt=($pageNum-1) * $rowsPerPage;
          	foreach ($sqlquery as $value)
          	{
     		      ?>
             	  <tr>
                    <td class="text-center"><?php echo $intcnt+1; ?></td>
                    <td><?php echo ucwords($value['first_name'].' '.$value['middle_name'].' '.$value['last_name']); ?></td>
                    <td><?php echo $value['email']; ?></td>
                    <td><?php echo $value['phone']; ?></td>
                    <td><?php echo $value['enroll_id']; ?></td>
		    <td><?php if ($value['payment_status'] != NULL) {
                          	echo $value['payment_status'];
                          }else{
                          	echo "Not Done";
                          }  ?>
                                              
                    </td>
                    <td>
                        <?php if($value['status']==0){?>
                        <span class="label label-warning">Pending..</span>
                        <?php } else { ?>
                        <span class="label label-success">Approved</span>
                        <?php } ?>
                    </td>

                    <td class="text-center">

						           <?php if ($value['status'] == 0 && $value['email_sent'] == 1 &&  $value['payment_status'] == 'success') {?>
                              <a href="#" title="Approve" class="btn btn-effect-ripple btn-xs btn-default" onClick="UpdateRecord(<?php echo $value['UserID']; ?>);"><i class="fa fa-check"></i></a>
                              <?php } else{ ?>
                              <a href="#" title="Approve" class="btn btn-effect-ripple btn-xs btn-default" onClick="UpdateRecord(<?php echo $value['UserID']; ?>);" style="display: none;"><i class="fa fa-check"></i></a>
                          <?php } ?>

                        <a href="view_userdetail.php?id=<?php echo $value['UserID'];?>&fullname=<?php echo ucwords($value['first_name'].' '.$value['middle_name'].' '.$value['last_name']); ?>&email=<?php echo $value['email']; ?>" title="View User Details" class="btn btn-effect-ripple btn-xs btn-default"><i class="fa fa-eye"></i></a>

                     

                        <a href="delete_spec.php?id=<?php echo $value['UserID'];?>" data-toggle="modal" title="Delete Specialization" class="btn btn-effect-ripple btn-xs btn-danger detelebtn" onclick="return confirm('Are you sure you want to delete this item?');" username="<?php //  echo $rowdata['title'];?>" id="<?php //  echo $rowdata['id'];?>"><i class="fa fa-times"></i></a>
                    </td>
                </tr>

                <div id="modal-fade" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title"><strong>Delete File</strong></h3>
                            </div>
                            <div class="modal-body">
                           		Are You want to delete this? <span class="showuser" style="font-weight: bold;"></span> ?.
                            </div>
                            <div class="modal-footer">
                                <a href="#" class="btn btn-effect-ripple btn-primary myprocesdelete">Delete</a>
                                <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
          			<?php
          			$intcnt = $intcnt+1; 
          	}
        }
          		echo '*';
      ?>
    	<ul class="pagination pagination-sm remove-margin" style="visibility: visible;">

                     <?php
                      $sqlquery   =   "SELECT * FROM users";
                      $sqlquery   =    $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";

                      $sqlquery   =    $sqlquery . " WHERE email_verified = 1";

                      if(isset($_POST['first_name'] ) && $_POST['first_name'] != "")
  						        {            
  						            $sqlquery   =    $sqlquery . " AND users.first_name LIKE '%".$_POST['first_name']."%' ";
  						        }
  						        if(isset($_POST['email']) && $_POST['email']  != "")
  						        {            
  						            $sqlquery   =    $sqlquery . " AND users.email LIKE  '%".str_replace(' ', '', $_POST['email'])."%' ";
  						        }
  						        if(isset($_POST['phone']) && $_POST['phone'] != "")
  						        {            
  						            $sqlquery   =    $sqlquery . " AND user_details.phone LIKE '%".$_POST['phone']."%' ";
  						        }
  						        if(isset($_POST['payment_status']) && $_POST['payment_status']  != "" )
                      {            
                          
                          if ($_POST['payment_status']  == "notdone") {
                               $sqlquery   =    $sqlquery . " AND users.payment_status IS NULL";
                          }else{
                               $sqlquery   =    $sqlquery . " AND users.payment_status = '".$_POST['payment_status']."'";
                          }
                      }
                      if(isset($_POST['status']) && $_POST['status']  != "" )
                      {            
                        $sqlquery   =    $sqlquery . " AND users.status = '".$_POST['status']."'";
                      }
                      if($_POST['from_date'] != "" && $_POST['to_date']  != "" )
                      {            
                          $from = date('Y-m-d H:i:s',strtotime($_POST['from_date']));
                          $to = date('Y-m-d H:i:s',strtotime($_POST['to_date']));
                          $sqlquery   =    $sqlquery . " AND users.updated_at BETWEEN '".$from."' AND '".$to."'";
                      }
                      if( $_POST['Programme'] != "")
                        {            
                            $sqlquery   =    $sqlquery . " AND user_details.Programme = '".$_POST['Programme']."' ";
                        }

                        if($_POST['dob'] != "")
                        {            
                            $dob = date('Y-m-d H:i:s',strtotime($_POST['dob']));
                            $sqlquery   =    $sqlquery . " AND users.dob LIKE '".$dob."' ";
                        }

                        if($_POST['gender'] != "")
                        {            
                            $sqlquery   =    $sqlquery . " AND user_details.gender = '".$_POST['gender']."' ";
                        }
                        if( $_POST['marital_status'] != "")
                        {            
                            $sqlquery   =    $sqlquery . " AND user_details.marital_status = '".$_POST['marital_status']."' ";
                        }

                        if($_POST['category'] != "")
                        {            
                            $sqlquery   =    $sqlquery . " AND user_details.category = '".$_POST['category']."' ";
                        }
                        if( $_POST['nationality'] != "")
                        {            
                            $sqlquery   =    $sqlquery . " AND user_details.nationality = '".$_POST['nationality']."' ";
                        }
                        if( $_POST['domicile'] != "")
                        {            
                            $sqlquery   =    $sqlquery . " AND user_details.domicile = '".$_POST['domicile']."' ";
                        }
                        if( $_POST['exam_center1'] != "")
                        {            
                            $sqlquery   =    $sqlquery . " AND user_details.exam_center1 = '".$_POST['exam_center1']."' ";
                        }
                        if( $_POST['exam_center2'] != "")
                        {            
                            $sqlquery   =    $sqlquery . " AND user_details.exam_center2 = '".$_POST['exam_center2']."' ";
                        }
                        if( $_POST['exam_center3'] != "")
                        {            
                            $sqlquery   =    $sqlquery . " AND user_details.exam_center3 = '".$_POST['exam_center3']."' ";
                        }
                        if( $_POST['twelft_pass_year'] != "")
                        {            
                            $sqlquery   =    $sqlquery . " AND user_details.twelft_pass_year = '".$_POST['twelft_pass_year']."' ";
                        }
                        if( $_POST['specialization_choice1'] != "")
                        {            
                            $sqlquery   =    $sqlquery . " AND user_details.specialization_choice1 = '".$_POST['specialization_choice1']."' ";
                        }

                        if( $_POST['specialization_choice2'] != "")
                        {            
                            $sqlquery   =    $sqlquery . " AND user_details.specialization_choice2 = '".$_POST['specialization_choice2']."' ";
                        }
                        if($_POST['specialization_choice3'] != "")
                        {            
                            $sqlquery   =    $sqlquery . " AND user_details.specialization_choice3 = '".$_POST['specialization_choice3']."' ";
                        }

                        $sqlquery = DB::query($sqlquery);
                        $numrows = DB::count();
                                  
                        $maxPage1 = ceil($numrows/$rowsPerPage);
                            $maxPage = 150;
                             if($maxPage1 < $maxPage)
                            {
                                $maxPage2 = $maxPage1;
                            }
                            else
                            {
                                $maxPage2 = $maxPage;
                            }
                            // print the link to access each page
                            $self = $_SERVER['PHP_SELF'];
                            $nav  = '';
                                                //$pagepath = 'B='.$_GET['B'];
                            for($page = 1; $page <= $maxPage2; $page++)
                            {
                                if ($page == $pageNum)
                                {
                                        $nav .= " <li class='active disabled'><a>$page</a></li> "; // no need to create a link to current page
                                }
                                        else
                                {
                                        $nav .= " <li><a onclick=\"pagination(this);\" data-page=\"$page\">$page</a> </li>";
                                } 
                                }

                                // ... still more code coming

                                // creating previous and next link
                                // plus the link to go straight to
                                // the first and last page

                                if ($pageNum > 1)
                                {
                                $page  = $pageNum - 1;
                                $prev  = " <li><a onclick=\"pagination(this);\" data-page=\"$page\"><i class='fa fa-chevron-left'></i></a> </li>";

                                $first = " <li><a onclick=\"pagination(this);\" data-page=\"1\"><i class='fa fa-step-backward'></i></a></li> ";
                                } 
                                else
                                {
                                $prev  = '&nbsp;'; // we're on page one, don't print previous link
                                $first = '&nbsp;'; // nor the first page link
                                }

                                if ($pageNum < $maxPage2)
                            {
                                $page = $pageNum + 1;
                                $next = " <li><a onclick=\"pagination(this);\" data-page=\"$page\"><i class='fa fa-chevron-right'></i></a></li> ";

                                $last = " <li><a onclick=\"pagination(this);\" data-page=\"$maxPage1\" ><i class='fa fa-step-forward'></i></a></li> ";
                                } 
                                else
                            {
                                $next = '&nbsp;'; // we're on the last page, don't print next link
                                $last = '&nbsp;'; // nor the last page link
                                }

                            // print the navigation link
                                echo $first . $prev . $nav . $next . $last;

                          // and close the database connection

                                  // ... and we're done! 
                          ?>
      </ul>
    <?php
             echo "Total recored : ".$numrows;
    	}
    }
    ?>

<script type="text/javascript">
    function UpdateRecord(id){
      jQuery.ajax({
       type: "POST",
       url: "update_user_status.php",
       data: 'id='+id,
       cache: false,
       success: function(response)
       {
         alert("Record successfully updated");
         location.reload();
       }
     });
    }

</script>