<?php require_once('rightusercheck.php'); ?>
<?php if(!empty($_SESSION['adminyncrights']) && trim($_SESSION['adminyncrights']) != "Administrator") { header('Location : index.php'); } ?>
<?php 
    require_once('main.php');
    $DB = new DBConfig();
    $DB -> config();
    $DB -> conn(); 
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>IICD</title>

        <meta name="description" content="IICD">
        <meta name="author" content="">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!-- Page Wrapper -->
        <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <!-- END Preloader -->

            <!-- Page Container -->
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
               	<?php require_once('header.php'); ?>
                    <!-- Page content -->
                    <div id="page-content">
                        <!-- Validation Header -->
                        <div class="content-header">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="header-section">
                                        <h1>Video</h1>
                                    </div>
                                </div>
                                <div class="col-sm-6 hidden-xs">
                                    <div class="header-section">
                                        <ul class="breadcrumb breadcrumb-top">
                                            <li>Home</li>
                                            <li><a href="">Video</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Validation Header -->

                        <!-- Form Validation Content -->
                        <?php
			           		if(!empty($_GET['id'])) 
			                {
			                    $edit     	= (int)$_GET['edit'];
			                    $id       	= (int)$_GET['id'];
			                    $sqlquery = "SELECT videos.* FROM videos WHERE videos.id = ".$id."";
			                    $rsdata   = $DB ->getdata($sqlquery);   
			                    if (mysql_num_rows($rsdata) > 0) 
			                    {            
			                      while($rowdata = mysql_fetch_array($rsdata))
			                      {
			                        $title  	=   $rowdata['title'];
			                        $image  	=   $rowdata['image'];
			                        $videolink   =   $rowdata['videolink'];			                     
			                      }
			                    }
			                }
			                else
			                {
			                		$edit           =   0;
			                        $id             =   0;
			                        $title	        =	null;
			                        $image          =   null;
			                        $videolink      =   null;
			                }
			        	?>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <!-- Form Validation Block -->
                                <div class="block">
                                    <!-- Form Validation Title -->
                                    <div class="block-title">
                                        <h2>Video</h2>
                                    </div>
                                    <!-- END Form Validation Title -->

                                    <!-- Form Validation Form -->
                                    <form id="form-validation" action="videosave.php" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
                                    	<input type="hidden" name="edit" id="edit" value="<?php echo $edit;?>" />
                                        <input type="hidden" name="id" id="id" value="<?php echo $id;?>" /> 
                                      	<div class="form-group">
                                            <label class="col-md-3 control-label" >Video  Title <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" id="title" name="title" value="<?php echo $title; ?>" class="form-control required" placeholder="Enter video title">
                                            </div>
                                        </div>
                                      
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" >Cover Image <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
      	                                          <input type="file" id="image" name="image" class="form-control" style="padding-top: 0 !important;">
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label class="col-md-3 control-label" >Video <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                  <input type="file" id="videolink" name="videolink" class="form-control" style="padding-top: 0 !important;">
                                                  <span style="color: #ff0000;">(Best upload upto 20MB size )</span>
                                            </div>
                                        </div>
                                      
                                      
                                        <div class="form-group form-actions">
                                            <div class="col-md-8 col-md-offset-3">
                                                <button type="submit" class="btn btn-effect-ripple btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END Form Validation Form -->
                                </div>
                                <!-- END Form Validation Block -->
                            </div>
                        </div>
                        <!-- END Form Validation Content -->
                    </div>
                    <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>
		<script src="js/plugins/ckeditor/ckeditor.js"></script>
        <!-- Load and execute javascript code used only in this page -->
        <script src="js/pages/formsValidation.js"></script>
        <script>$(function(){ FormsValidation.init(); });</script>
        
	
    </body>
</html>
<?php $DB -> close(); ?>