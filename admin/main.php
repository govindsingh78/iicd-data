<?php
error_reporting(E_ALL ^ E_DEPRECATED);
$SiteUrl = 'http://localhost/iicdlocal/admin';
    final class DBConfig {

        var $host;
        var $user;
        var $pass;
        var $db;
        var $db_link;
        var $conn = false;
        var $persistant = false;
        var $badStrings = array("Content-Type:", "MIME-Version:", "content-type:", "mime-version:", "multipart/mixed", "content-transfer-encoding:", "to:", "Content-Transfer-Encoding:", "bcc:", "cc:", "href=");

        public $error = false;

        public function config()
        { // class config
            $this->error = true;
            $this->persistant = false;
        }

        public function conn($host='localhost',$user='iicdac_profile',$pass='Ga9OSV0ZlT?r',$db='iicdac_profile')
        { // connection function
            $this->host = $host;
            $this->user = $user;
            $this->pass = $pass;
            $this->db = $db;

            // Establish the connection.
            if ($this->persistant)
                $this->db_link = mysql_pconnect($this->host, $this->user, $this->pass, true);
            else
                $this->db_link = mysql_connect($this->host, $this->user, $this->pass, true);

            if (!$this->db_link) {
                if ($this->error) {
                    $this->error($type=1);
                }
                return false;
            } else {
                if (empty($db)) {
                    if ($this->error) {
                        $this->error($type=2);
                    }
                } else {
                    $db = mysql_select_db($this->db, $this->db_link); // select db
                    if (!$db) {
                        if ($this->error) {
                            $this->error($type=2);
                        }
                        return false;
                    }
                    $this->conn = true;
                }
                return $this->db_link;
            }
        }

        public function close()
        { // close connection
            if ($this->conn){ // check connection
                if ($this->persistant) {
                    $this->conn = false;
                }
                else {
                    mysql_close($this->db_link);
                    $this->conn = false;
                }
            }
            else {
                if ($this->error) {
                    return $this->error($type=4);
                }
            }
        }

        public function error($type='')
        { //Choose error type
            if (empty($type)) {
                return false;
            }
            else {
                if ($type==1)
                    echo "<strong>Database could not connect</strong> ";
                else if ($type==2)
                    echo "<strong>mysql error</strong> " . mysql_error();
                    else if ($type==3)
                        echo "<strong>error </strong>, Proses has been stopped";
                        else
                            echo "<strong>error </strong>, no connection !!!";
            }
        }

        public function getdata($dataquery)
        {    
            //Get Data            
            mysql_query('SET character_set_results=utf8');       
            mysql_query('SET names=utf8');
            mysql_query('SET character_set_client=utf8');       
            mysql_query('SET character_set_connection=utf8');
            mysql_query('SET collation_connection=utf8_general_ci');
            if (!mysql_query($dataquery, $this->db_link)){
                die('Error: ' . mysql_error());
            } else {
                $database_data = mysql_query($dataquery);
                return $database_data;
            }
        }

        public function savedata($savequery) 
        {
            mysql_query('SET character_set_results=utf8');       
            mysql_query('SET names=utf8');
            mysql_query('SET character_set_client=utf8');       
            mysql_query('SET character_set_connection=utf8');
            mysql_query('SET collation_connection=utf8_general_ci');
            if (!mysql_query($savequery, $this->db_link)) {
                die('Error: ' . mysql_error());
            } else {
                return true;
            }
        }

        public function updatedata($updatequery)
        {         
            mysql_query('SET character_set_results=utf8');       
            mysql_query('SET names=utf8');
            mysql_query('SET character_set_client=utf8');       
            mysql_query('SET character_set_connection=utf8');
            mysql_query('SET collation_connection=utf8_general_ci');
            if (!mysql_query($updatequery, $this->db_link)) {
                die('Error: ' . mysql_error());
            } else {
                return true;
            }
        }

        public function deletedata($deletequery)
        {      
            if (!mysql_query($deletequery, $this->db_link)) {
                die('Error: ' . mysql_error());
            } else {
                return true;
            }
        }

        public function cleardata($dataset)
        {      
            mysql_free_result($dataset);
        }

        
        
    }
?>