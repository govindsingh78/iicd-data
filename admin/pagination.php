<?php
function displayPaginationBelow($per_page,$page){
      //  print_r($where); die;
        $page_url="?";
        $pageid="#";

         $sqlquery   =    "SELECT * FROM users";

         /*if (!empty($where)) {
         $sqlquery   =    $sqlquery.$where;
         }*/
       //  echo $sqlquery; die;

      //  $sqlquery   =    "SELECT * FROM users";
        $sqlquery = DB::query($sqlquery);

        $total = DB::count();

        $adjacents = "2";

        $page = ($page == 0 ? 1 : $page);  
        $start = ($page - 1) * $per_page;                                                               
                
        $prev = $page - 1;                                                      
        $next = $page + 1;
        $setLastpage = ceil($total/$per_page);
        $lpm1 = $setLastpage - 1;
        
        $setPaginate = "";
        if($setLastpage > 1)
        {       
                $setPaginate .= "<ul class='pagination pagination-sm remove-margin' style='float: left;'>";
                $setPaginate .= "<li class='setPage'>Showing page $page of $setLastpage</li>";
                $setPaginate .= "</ul>";
                $setPaginate .= "<ul class='pagination pagination-sm remove-margin' id='pagination' style='float: right;'>";
                    
                if ($setLastpage < 7 + ($adjacents * 2))
                {       
                        $setPaginate.= "<li class='prev'><a href='{$page_url}page=$prev{$pageid}pagination'><i class='fa fa-chevron-left'></i></a></li>";
                        for ($counter = 1; $counter <= $setLastpage; $counter++)
                        {
                                if ($counter == $page)
                                        $setPaginate.= "<li class='active'><a class='current_page'>$counter</a></li>";
                                else
                                        $setPaginate.= "<li><a href='{$page_url}page=$counter{$pageid}pagination'>$counter</a></li>";                                   
                        }
                }
                elseif($setLastpage > 5 + ($adjacents * 2))
                {
                        if($page < 1 + ($adjacents * 2))                
                        {
                                $setPaginate.= "<li class='prev'><a href='{$page_url}page=$prev{$pageid}pagination'><i class='fa fa-chevron-left'></i></a></li>";
                                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                                {
                                        if ($counter == $page)
                                                $setPaginate.= "<li class='active'><a class='current_page'>$counter</a></li>";
                                        else
                                                $setPaginate.= "<li><a href='{$page_url}page=$counter{$pageid}pagination'>$counter</a></li>";                                   
                                }
                                $setPaginate.= "<li class='dot'>...</li>";
                                $setPaginate.= "<li><a href='{$page_url}page=$lpm1'>$lpm1</a></li>";
                                $setPaginate.= "<li><a href='{$page_url}page=$setLastpage'>$setLastpage</a></li>";              
                        }
                        elseif($setLastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
                        {
                                $setPaginate.= "<li class='prev'><a href='{$page_url}page=$prev'><i class='fa fa-chevron-left'></i></a></li>";
                                $setPaginate.= "<li><a href='{$page_url}page=1{$pageid}pagination'>1</a></li>";
                                $setPaginate.= "<li><a href='{$page_url}page=2{$pageid}pagination'>2</a></li>";
                                $setPaginate.= "<li class='dot'>...</li>";
                                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                                {
                                        if ($counter == $page)
                                                $setPaginate.= "<li class='active'><a class='current_page'>$counter</a></li>";
                                        else
                                                $setPaginate.= "<li><a href='{$page_url}page=$counter{$pageid}pagination'>$counter</a></li>";                                   
                                }
                                //$setPaginate.= "<li class='dot'>..</li>";
                                //$setPaginate.= "<li><a href='{$page_url}page=$lpm1'>$lpm1</a></li>";
                                $setPaginate.= "<li><a href='{$page_url}page=$setLastpage{$pageid}pagination'>$setLastpage</a></li>";           
                        }
                        else
                        {
                                $setPaginate.= "<li class='prev'><a href='{$page_url}page=$prev{$pageid}pagination'><i class='fa fa-chevron-left'></i></a></li>";
                                $setPaginate.= "<li><a href='{$page_url}page=1{$pageid}pagination'>1</a></li>";
                                $setPaginate.= "<li><a href='{$page_url}page=2{$pageid}pagination'>2</a></li>";
                                $setPaginate.= "<li class='dot'>..</li>";
                                for ($counter = $setLastpage - (2 + ($adjacents * 2)); $counter <= $setLastpage; $counter++)
                                {
                                        if ($counter == $page)
                                                $setPaginate.= "<li class='active'><a class='current_page'>$counter</a></li>";
                                        else
                                                $setPaginate.= "<li><a href='{$page_url}page=$counter{$pageid}pagination'>$counter</a></li>";                                   
                                }
                        }
                }
                
                if ($page < $counter - 1){ 
                        $setPaginate.= "<li><a href='{$page_url}page=$next{$pageid}pagination'>Next</a></li>";
                $setPaginate.= "<li><a href='{$page_url}page=$setLastpage{$pageid}pagination'>Last</a></li>";
                }else{
                        $setPaginate.= "<li class='active'><a class='current_page'>Next</a></li>";
                $setPaginate.= "<li class='active'><a class='current_page'>Last</a></li>";
            }

                $setPaginate.= "</ul>\n";               
        }
    
    
        return $setPaginate;
    } 
?>