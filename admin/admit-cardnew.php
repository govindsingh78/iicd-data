<?php require_once('rightusercheck.php'); ?>
<?php
  require_once('../meekrodb.2.3.class.php');
  //include('../phpmailer/PHPMailerAutoload.php');
    require_once ('phpmailer/class.phpmailer.php'); 
    $path = "https://www.iicd.ac.in/";

   $id = $_POST['id'];


    $hrEmail = 'rajpalsingh.info@gmail.com';

     $sqlquery   =    "SELECT users.id AS UserID, users.*, user_details.Programme,user_details.fathers_name,user_details.dob,user_details.phone,user_details.exam_center1,user_details.applicant_photo,user_details.dob_certificate,user_details.id_proof,user_details.signatures,exam_centers.center_name,exam_centers.city  FROM users";

            $sqlquery   =    $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";
            $sqlquery   =    $sqlquery . " LEFT JOIN exam_centers ON user_details.exam_center1 = exam_centers.city";
            $sqlquery   =    $sqlquery . " WHERE users.status = 1 AND users.id = '".$id."'";
            $result = DB::queryFirstRow($sqlquery);

            $first_name = $result['first_name'];
            $middle_name = $result['middle_name'];
            $last_name = $result['last_name'];
            $fullname = $first_name.' '.$middle_name.' '.$last_name;
            $fathers_name = $result['fathers_name'];
            $Programme = $result['Programme'];
            $enroll_id = $result['enroll_id'];

            if ($Programme == 'PG') {
            $pname = "Master of Vocation in Crafts and Design";
              }elseif($Programme == 'UG'){
                $pname = "4 Year Integrated Bachelors Programme (CFPD + B. VOC)";
              }else{
                $pname = "5 Year Integrated Masters Programme (CFPD + B. VOC. + M. VOC.)";
              }

            $dob = date('d/m/Y',strtotime($result['dob']));
            $phone = $result['phone'];
            $exam_center1 = $result['exam_center1'];
            $center_name = $result['center_name'];
            $applicant_photo = $path.'images/'.$result['applicant_photo'];
            $dob_certificate = $path.'images/'.$result['dob_certificate'];
            $id_proof = $path.'images/'.$result['id_proof'];
            $signatures = $path.'images/'.$result['signatures'];

            $examcenter = "SELECT address FROM exam_centers WHERE city = '".$result['exam_center1']."'";
              $centerName = DB::queryFirstRow($examcenter);
              $address = $centerName['address'];

              $settings = "SELECT * FROM settings WHERE id = 1";
              $settingsName = DB::queryFirstRow($settings);

              $settings2 = "SELECT * FROM settings WHERE id = 2";
              $settingsName2 = DB::queryFirstRow($settings2);

              $value = $settingsName['value'];
              $value2 = $settingsName2['value'];
            //  $template_back = DB::queryFirstRow("SELECT * FROM `email_templates` WHERE slug='email-body'");
        //$temp = $template_back['body'];
        $temp = $value2;

    $newbody = '<div style="
background-image: url(../images/main-body-bg.png);
background-position: center;
background-repeat: repeat;
background-attachment: fixed;">
        <div style="width: 800px;margin: 0 auto;padding: 10px 0;">
                             <div style="float: left;width: 100px;">
                                    <a href="#">
                                        <img style="width: 100px; display: block;" src="'.$path.'images/logo1.png" alt=""> 
                                    </a>
                                </div>
                                 <div style="float: left;width: 450px;">
                                    <a href="#">
                                        <img style="width: 450px; display: block;" src="'.$path.'images/indian1.png" alt=""> 
                                    </a>
                                </div>
                                <div style="float: left;width: 120px;">
                                    <a href="#">
                                        <img style="width: 120px; display: block;" src="'.$path.'images/admit1.png" alt=""> 
                                    </a>
                                </div>
                        </div>
                        <div class="clearfix"></div>
       <div class="admit-card-form">
         <div style="width: 800px;margin: 0 auto;">
           <div style="padding: 0px;border-top: 1px solid #000;border-left: 1px solid #000;float: left;list-style: none;">
             <div style="border-bottom: 1px solid #000;float: left;width: 800px;">
              <div style="height:40px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Name</div>
              <div style="height:40px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 160px;box-sizing: border-box; text-transform: capitalize;"><strong> '.$fullname.' </strong></div>
              <div style="height:40px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Enrollment No.</div>
              <div style="height:40px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 155px;box-sizing: border-box;"><strong>'.$enroll_id.'</strong></div>
            </div>
             <div style="border-bottom: 1px solid #000;float: left;width: 800px;">
              <div style="height:80px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Father`s Name</div>
              <div style="height:80px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 160px;box-sizing: border-box; text-transform: capitalize;"><strong>'.$fathers_name.'</strong></div>
              <div style="height:80px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Programme</div>
              <div style="height:80px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 155px;box-sizing: border-box;"><strong>'.$pname.'</strong></div>
            </div>
             <div style="border-bottom: 1px solid #000;float: left;width: 1170px;">
              <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Date of Birth</div>
              <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 160px;box-sizing: border-box;"><strong>'.$dob.'</strong></div>
              <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Mobile No.</div>
              <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 155px;box-sizing: border-box;"><strong>'.$phone.'</strong></div>
            </div>
             <div style="border-bottom: 1px solid #000;float: left;width: 1170px;">
              <div style="height:60px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Date of Entrance Test Part A :</div>
              <div style="height:60px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 160px;box-sizing: border-box;"><strong>15/04/2018</strong></div>
              <div style="height:60px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Test Center</div>
              <div style="height:60px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 155px;box-sizing: border-box;"><strong>'.$center_name.'</strong></div>
            </div>
             <div style="border-bottom: 1px solid #000;float: left;width: 1170px;">
              <div style="height:60px;float: left;height: 60px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Address of Test Center</div>
              <div style="height:60px;float: left;min-height: 84px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 497px;box-sizing: border-box;"><strong>'.$address.'</strong></div>
              
            </div>
           </div>
           <h3 style="text-align: center;font-size: 20px;clear: both;">Entrance Test Attendance:</h3>
           <div style="float: left; width: 800px;">
            <div style="padding: 0px;border-top: 1px solid #000;border-left: 1px solid #000;float: left;list-style: none;width: 800px;">
             <div style="border-bottom: 1px solid #000;float: left;width: 800px;">
              <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 11px;width: 225px;box-sizing: border-box;text-align: center;"><h6 style="text-align: center;
margin: 0;
font-size: 20px;">Part A </h6>General Awareness, Creativity & Perception Test <strong style="display: block;
text-align: center;"> <br> (10:00 AM  to  1:00  PM) </strong></div>
              <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 11px;width: 200px;box-sizing: border-box;text-align: center;"><h6 style="text-align: center;
margin: 0;
font-size: 20px;">Part B </h6>Material,  Color & Conceptual  Test <strong style="display: block;
text-align: center;"> <br> (10:00 AM  to  1:00  PM) </strong></div>
              <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 11px;width: 190px;box-sizing: border-box;text-align: center;"><h6 style="text-align: center;
margin: 0;
font-size: 20px;">Part C </h6>Personal Interview <br> <strong style="display: block;
text-align: center;"> (2:00  PM  onwards) </strong></div>
             
            </div>
 <div style="border-bottom: 1px solid #000;float: left;width: 1170px;">
              <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 225px;box-sizing: border-box;text-align: center;"><h5 style="margin: 0;font-size: 20px;color: #ccc;">Invigilator Initial </h5></div>
            <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 200px;box-sizing: border-box;text-align: center;"><h5 style="margin: 0;font-size: 20px;color: #ccc;">Invigilator Initial </h5></div>
          <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 190px;box-sizing: border-box;text-align: center;"><h5 style="margin: 0;font-size: 20px;color: #ccc;">Invigilator Initial </h5></div></div>
          </div>
         </div>
          <div style="float: left; width: 800px;">
            <div style="padding: 0px;border-top: 1px solid #000;border-left: 1px solid #000;float: left;list-style: none;width: 800px;margin-top:30px;">
              <div style="border-bottom: 1px solid #000;float: left;width: 800px;">
              <div style="float: left;height: 150px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 148px;box-sizing: border-box;text-align: center;"><img src="'.$applicant_photo.'" height="150" ></div>
            <div style="float: left;height: 150px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 148px;box-sizing: border-box;text-align: center;"><img src="'.$dob_certificate.'" height="150"></div>
          <div style="float: left;height: 150px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 148px;box-sizing: border-box;text-align: center;"><img src="'.$id_proof.'" height="150"></div>
          <div style="float: left;height: 150px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 148px;box-sizing: border-box;text-align: center;"><img src="'.$signatures.'" height="150"></div>
          </div>

            </div>
          </div>
         <h5 style="text-align: right;font-size: 18px;margin: 40px 0;clear: both;float: right;
width: 800px;"> <img style="max-width: 220px; display: block;float: right;" src="'.$path.'images/signature.png" alt=""></h5>
        '.$value.'
         </div>
       </div>
    </div>';

include("mpdf/mpdf.php");

//include your mpdf library here

 $mpdf=new mPDF('utf-8', 'A4-L');

// create an object of the class mpdf
$mpdf=new mPDF("c"); 
// write the html to the file

$mpdf->WriteHTML($newbody);
 $filename = 'IICD'.time().$id.'.pdf';
// generate the output
      $rootpath =  __DIR__.'/admit_cards/'.$filename;
      
        $output = $mpdf->Output($rootpath,'F');
          
             $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->Port = 587;
            $mail->SMTPSecure = 'tls';
            $mail->SMTPAuth = true;
             $mail->Username = 'admissions@iicd.ac.in';
            $mail->Password = 'admission915';
            $mail->setFrom($hrEmail, 'IICD');
            $mail->addReplyTo($hrEmail, 'IICD');
            $mail->addAddress($result['email']);//tl mail
            $mail->AddCC('admissions@iicd.ac.in'); 
            $mail->addAttachment($rootpath);
            $mail->Subject = 'Admission 2018-2019 : Admit Card and Test Center details';
            $mail->msgHTML($temp);
            if (!$mail->send()) {
                 $result['failed'] = 'failed';
                $result['message'] = 'Failed';
                echo json_encode($result);exit;
            } else {

                $update = DB::update('users', array('admit_card' => $filename,'pdf_sent' => 1), "id=%i", $id);
                 $result['success'] = 'success';
                $result['message'] = 'Mail Sent Successfully';
                echo json_encode($result);exit;
            }
     
?>