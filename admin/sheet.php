<?php require_once('rightusercheck.php'); ?>
<?php if(!empty($_SESSION['adminyncrights']) && trim($_SESSION['adminyncrights']) != "Administrator") { header('Location : index.php'); } ?>
<?php 
    require_once('../meekrodb.2.3.class.php');
    /*$DB = new DBConfig();
    $DB -> config();
    $DB -> conn(); */
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>IICD</title>

        <meta name="description" content="IICD">
        <meta name="author" content="">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
        <!-- END Stylesheets -->
        <link rel="stylesheet" type="text/css" href="../css/stylead.css">

        <!-- Modernizr (browser feature detection library) -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <!-- END Preloader -->

            <!-- Page Container -->
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
                <?php require_once('header.php'); ?>
                    <!-- Page content -->
                    <div id="page-content">
                        <!-- Datatables Block -->
                        <!-- Datatables is initialized in js/pages/uiTables.js -->
                        <div class="block full" style="height: 150px;">

                            <div class="block-title">
                                <h2>Settings</h2>
                            	<!-- <a href="add_examcenter.php" class="btn btn-success" style="float: right; margin-right: 10px; margin-top: 3px;"><i class="fa fa-plus"></i> </a> -->
                            </div>

                            <form  method="POST">
                                 <div class="form-group col-md-3">
                                            <select class="form-control" id="Programme" placeholder="Programme" name="Programme">
                                                <option value="">Select Programme</option>
                                                <option <?php if (isset($_POST['Programme']) && $_POST['Programme'] == 'UG') {  echo "selected";} ?> value="UG">4 Year Integrated Bachelors Programme (CFPD + B. VOC)</option>
                                                <option <?php if (isset($_POST['Programme']) && $_POST['Programme'] == 'INTG') {  echo "selected";} ?> value="INTG">5 Year Integrated Masters Programme (CFPD + B. VOC. + M. VOC.)</option>
                                                <option <?php if (isset($_POST['Programme']) && $_POST['Programme'] == 'PG') {  echo "selected";} ?> value="PG">Master of Vocation in Crafts and Design</option>
                                            </select>
                                    </div>

                                    <?php $centers = DB::query("select * from exam_centers"); ?>
                                    <div class="form-group col-md-3">
                                        <select id="exam_center1" name="exam_center1" class="form-control">
                                        <option value="">Center Name</option>
                                          <?php
                                          foreach ($centers as $val) {
                                              $selected = '';
                                              if($val['city']==$_POST['exam_center1']){
                                                  $selected = 'selected="selected"';
                                              }
                                              echo '<option value="'.$val['city'].'" '.$selected.'>'.$val['city'].'</option>';
                                          }
                                          ?>
                                      </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                      <button id="sheetData" class="btn btn-primary" type="button"><i class="fa fa-filter"></i> Filter</button>
                                    </div>
                            </form>
                        </div>
                        <div class="table-responsive sheet" id='DivIdToPrint'>
                        </div>
                        <input type='button' id='btn' value='Print' onclick='printDiv();'>

                        <!-- END Datatables Block -->
                    </div>
                    <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>

        <!-- Load and execute javascript code used only in this page -->
        <script src="js/pages/uiTables.js"></script>
        <script>$(function(){ UiTables.init(); });</script>
    </body>
</html>
<script type="text/javascript">
	$( document ).ready(function() {
		$('.detelebtn').click(function() {
			$('#modal-fade').fadeIn("fast");
			var id=$(this).attr('id');
			var username=$(this).attr('username');
			$('#modal-fade .showuser').text(username);
			$('#modal-fade .myprocesdelete').attr('href','delete_examcenter.php?id='+id+'&delete=1');
		});
	});
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#sheetData").click(function(){
           var program = $('#Programme').val();
           var center_name = $('#exam_center1').val(); 

            $.ajax({
                type: 'POST',
                dataType: 'html',
                url: 'attendancesheet.php',
                data:{'program':program,'center_name':center_name},
                success: function(data) {
                    
                    $(".sheet").html(data);

                }
            });
   });
});
</script>
<script type="text/javascript">
    function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>
<?php /*$DB -> close();*/ ?>