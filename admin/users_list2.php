<?php require_once('rightusercheck.php'); ?>
<?php if(!empty($_SESSION['adminyncrights']) && trim($_SESSION['adminyncrights']) != "Administrator") { header('Location : index.php'); } ?>
<?php 
    require_once('../meekrodb.2.3.class.php');
    require_once('pagination.php');
    /*$DB = new DBConfig();
    $DB -> config();
    $DB -> conn(); */
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>IICD</title>

        <meta name="description" content="IICD">
        <meta name="author" content="">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
        <!-- END Stylesheets -->
        <style type="text/css">
            .block .block {
    height: 115px;
    border: 1px solid #dae0e8;
    -webkit-box-shadow: none;
    box-shadow: none;
}
        </style>

        <!-- Modernizr (browser feature detection library) -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <!-- END Preloader -->

            <!-- Page Container -->
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
                <?php require_once('header.php'); ?>
                    <!-- Page content -->
                    <div id="page-content">
                        <!-- Datatables Block -->
                        <!-- Datatables is initialized in js/pages/uiTables.js -->
                        <div class="block full">
                            <div class="row">
                                 <div class="row">
                            <div class="col-sm-12">
                                <!-- Block -->
                                <div class="block">
                                    <!-- Block Title -->
                                    <div class="block-title">
                                        <h2>Users</h2>
                                    </div>
                                    <!-- END Block Title -->

                                    <!-- Block Content -->
                            
                       <form method="POST" action="users_list.php" accept-charset="UTF-8"> 
                        <div class="form-group col-md-2">
                          <input class="form-control" placeholder="First Name" name="first_name" type="text" value="">
                        </div> 

                        <div class="form-group col-md-2">
                          <input class="form-control" placeholder="Email" name="email" type="text" value="">
                        </div>

                        <div class="form-group col-md-2">
                                <input class="form-control" placeholder="Phone" name="phone" type="text" value="">
                        </div> 

                        <div class="form-group col-md-2">
                            <!-- <select class="form-control" id="planSelect" name="plan"><option value="">Select a plan </option><option value="1">Basic Plan</option><option value="2">Basic Plan </option><option value="3">Premium Plan</option></select> -->
                        </div> 

                        <div class="form-group col-md-2">
                          <button class="btn btn-primary" type="submit"><i class="fa fa-filter"></i> Filter</button>
                        </div>
                        <div class="form-group col-md-2">
                          <a class="btn btn-primary" href="users_list.php"><i class="fa fa-filter"></i> Reset Filter</a>
                         
                        </div>
                        </form> 


                                    <!-- END Block Content -->
                                </div>
                                <!-- END Block -->
                            </div>
                            
                        </div>
                            </div>
                            <div class="row">
                            <div class="table-responsive">
                                <?php

                                    

                                	if(!empty($_GET['page']) && (int)$_GET['page'] > 0){
                                        $page = (int)$_GET['page'];
                                    } else {
                                        $page =  1;
                                    }
                                    $setLimit = 10;
                                    $pageLimit = ($page * $setLimit) - $setLimit;

                                        $sqlquery   =    "SELECT users.*, user_details.* FROM users";

                                        $sqlquery   =    $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";

                                         $sqlquery   =    $sqlquery . " WHERE email_verified = 1";

                                        if(isset($_POST['first_name'] ) && $_POST['first_name'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND users.first_name LIKE '%".$_POST['first_name']."%' ";
                                        }
                                        if(isset($_POST['email']) && $_POST['email']  != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND users.email = '".$_POST['email']."' ";
                                        }
                                        if(isset($_POST['phone']) && $_POST['phone'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.phone = '".$_POST['phone']."' ";
                                        }
                                        if(isset($_POST['created_at']) && $_POST['created_at']  != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND users.created_at = ".$created_at;
                                        }
//print_r($sqlquery);die;
                                         $sqlquery   =    $sqlquery . " ORDER BY users.created_at DESC LIMIT ".$pageLimit.", ".$setLimit."";


                                        $sqlquery = DB::query($sqlquery);

    				                    $counter = DB::count();

    									if ($counter > 0) 
    				                    { 
    				                    	// print the random numbers
    				     		?>
                                <table class="table table-striped table-bordered table-vcenter">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 50px;">S.no</th>
                                            <th>Full Name</th>
                                            <th>Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
		                            	$intcnt=0;
		                              	foreach ($sqlquery as $value)
		                              	{
                                            echo "<pre>";
                                            print_r($value);
		                                 	$intcnt = $intcnt+1;                    
		                          	?>
                                        <tr>
                                            <td class="text-center"><?php echo $intcnt; ?></td>
                                            <td><?php echo ucwords($value['first_name'].' '.$value['middle_name'].' '.$value['last_name']); ?></td>
                                            <td><?php echo $value['email'].' '.$value['id']; ?></td>


                                            <td class="text-center">
                                                <a href="view_userdetail.php?id=<?php echo $value['id'];?>&fullname=<?php echo ucwords($value['first_name'].' '.$value['middle_name'].' '.$value['last_name']); ?>&email=<?php echo $value['email']; ?>" title="View User Details" class="btn btn-effect-ripple btn-xs btn-default"><i class="fa fa-eye"></i></a>

                                             

                                            <a href="delete_spec.php?id=<?php echo $value['id'];?>" data-toggle="modal" title="Delete Specialization" class="btn btn-effect-ripple btn-xs btn-danger detelebtn" onclick="return confirm('Are you sure you want to delete this item?');" username="<?php //  echo $rowdata['title'];?>" id="<?php //  echo $rowdata['id'];?>"><i class="fa fa-times"></i></a>
                                            </td>
                                         
                                        </tr>
                                        <div id="modal-fade" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	                                        <div class="modal-dialog">
	                                            <div class="modal-content">
	                                                <div class="modal-header">
	                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                                                    <h3 class="modal-title"><strong>Delete File</strong></h3>
	                                                </div>
	                                                <div class="modal-body">
	                                               		Are You want to delete Specialization <span class="showuser" style="font-weight: bold;"></span> ?.
	                                                </div>
	                                                <div class="modal-footer">
	                                                    <a href="#" class="btn btn-effect-ripple btn-primary myprocesdelete">Delete</a>
	                                                    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Close</button>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
                                	<?php
                                		}
                                	?>
                                    </tbody>
                                </table>
                              	<?php
                              		}else{										
                              	?>
                              	<table class="table table-striped table-bordered table-vcenter">
                              		<tr>
                              			<td align="center">No Record Found</td>
                              		</tr>
                              	</table>
                              	<?php } ?>
                                <table class="table table-striped table-bordered">
                                    <tbody>
                                        <tr>
                                            <td colspan="15">
                                                <?php
                                                    echo displayPaginationBelow($setLimit,$page);
                                                ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                        <!-- END Datatables Block -->
                    </div>
                    <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>

        <!-- Load and execute javascript code used only in this page -->
        <script src="js/pages/uiTables.js"></script>
        <script>$(function(){ UiTables.init(); });</script>
    </body>
</html>
<script type="text/javascript">
	$( document ).ready(function() {
		$('.detelebtn').click(function() {
			$('#modal-fade').fadeIn("fast");
			var id=$(this).attr('id');
			var username=$(this).attr('username');
			$('#modal-fade .showuser').text(username);
			$('#modal-fade .myprocesdelete').attr('href','deletevideo.php?id='+id+'&delete=1');
		});
	});
</script>

<?php /*$DB -> close();*/ ?>