<?php 
    session_start();
    require_once('../meekrodb.2.3.class.php');

    $years = array('2001'=>'2001','2002'=>'2002','2003'=>'2003','2004'=>'2004','2005'=>'2005','2006'=>'2006','2007'=>'2007','2008'=>'2008','2009'=>'2009','2010'=>'2010','2011'=>'2011','2012'=>'2012','2013'=>'2013','2014'=>'2014','2015'=>'2015','2016'=>'2016','2017'=>'2017','2018'=>'2018');

    $specialisations =  array('Fired Material Design' =>'Fired Material Design' ,'Soft Material Design' =>'Soft Material Design' ,'Hard Material Design' =>'Hard Material Design' ,'Fashion Design' =>'Fashion Design','Fired Material Specialization' =>'Fired Material Specialization' ,'Hard Material Specialization' =>'Hard Material Specialization' ,'Soft Material Specialization' =>'Soft Material Specialization');

 
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>IICD</title>

        <meta name="description" content="IICD">
        <meta name="author" content="">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
            <script src="http://code.jquery.com/jquery-1.9.1.js"></script>

            <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

        <!-- END Stylesheets -->
        <style type="text/css">
            .block .block {
                height: 400px;
                border: 1px solid #dae0e8;
                -webkit-box-shadow: none;
                box-shadow: none;
            }
            input{
                    padding-left: 6px;
            }
        </style>

        <!-- Modernizr (browser feature detection library) -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <!-- END Preloader -->

            <!-- Page Container -->
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
                <?php require_once('header.php'); ?>
                    <!-- Page content -->
                    <div id="page-content">
                        <!-- Datatables Block -->
                        <!-- Datatables is initialized in js/pages/uiTables.js -->
                        <div class="block full">
                            <div class="row">
                              
                                 <div class="row">
                            <div class="col-sm-12">
                                <!-- Block -->
                                <div class="block">
                                     
                                    <!-- Block Title -->
                                    <div class="block-title">
                                        <h2>Users</h2>
                                        
                                    </div>
                                    <!-- END Block Title -->

                                    <!-- Block Content -->
                            
                                   <form method="POST" action="users_list.php" accept-charset="UTF-8"> 

                                    <div class="form-group col-md-2">
                                      <input class="form-control" placeholder="First Name" name="first_name" type="text" value="<?php if (isset($_POST['first_name']) && $_POST['first_name'] != '') {  echo $_POST['first_name'];} ?>">
                                    </div> 

                                    <div class="form-group col-md-3">
                                      <input class="form-control" placeholder="Email" name="email" type="text" value="<?php if (isset($_POST['email']) && $_POST['email'] != '') {  echo $_POST['email'];} ?>">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <input class="form-control" placeholder="Phone" name="phone" type="text" value="<?php if (isset($_POST['phone']) && $_POST['phone'] != '') {  echo $_POST['phone'];} ?>">
                                    </div> 

                                    <div class="form-group col-md-2">
                                            <select class="form-control" placeholder="Payment Status" name="payment_status">
                                            	<option value="">Select Payment status</option>
                                            	<option <?php if (isset($_POST['payment_status']) && $_POST['payment_status'] == 'notdone') {  echo "selected";} ?> value="notdone">Not Done</option>
                                            	<option <?php if (isset($_POST['payment_status']) && $_POST['payment_status'] == 'success') {  echo "selected";} ?> value="success">Success</option>
                                            	<option <?php if (isset($_POST['payment_status']) && $_POST['payment_status'] == 'failure') {  echo "selected";} ?> value="failure">Failure</option>
                                            </select>
                                    </div> 

                                    <div class="form-group col-md-2">
                                            <select class="form-control" placeholder="Status" name="status">
                                                <option value="">Select Status</option>
                                                <option <?php if (isset($_POST['status']) && $_POST['status'] == '1') {  echo "selected";} ?> value="1">Approved</option>
                                                <option <?php if (isset($_POST['status']) && $_POST['status'] == '0') {  echo "selected";} ?> value="0">Pending</option>
                                            </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                           <p class="search_input">
                                            <input type="text" placeholder="From Date (yy-mm-dd)" id="from" name="from_date"  value="<?php if (isset($_POST['from_date']) && $_POST['from_date'] != '') {  echo $_POST['from_date'];} ?>" class="input-control" />
                                            <input type="text" placeholder="To Date (yy-mm-dd)" id="to" name="to_date" style="margin-left:5px"  value="<?php if (isset($_POST['to_date']) && $_POST['to_date'] != '') {  echo $_POST['to_date'];} ?>" class="input-control"  />             
                                            </p>
                                    </div>

                                    <div class="form-group col-md-3">
                                            <select class="form-control" placeholder="Programme" name="Programme">
                                                <option value="">Select Programme</option>
                                                <option <?php if (isset($_POST['Programme']) && $_POST['Programme'] == 'UG') {  echo "selected";} ?> value="UG">4 Year Integrated Bachelors Programme (CFPD + B. VOC)</option>
                                                <option <?php if (isset($_POST['Programme']) && $_POST['Programme'] == 'INTG') {  echo "selected";} ?> value="INTG">5 Year Integrated Masters Programme (CFPD + B. VOC. + M. VOC.)</option>
                                                <option <?php if (isset($_POST['Programme']) && $_POST['Programme'] == 'PG') {  echo "selected";} ?> value="PG">Master of Vocation in Crafts and Design</option>
                                            </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                           <p class="search_input">
                                            <input type="text" placeholder="DOB (yy-mm-dd)" id="dob" name="dob"  value="<?php if (isset($_POST['dob']) && $_POST['dob'] != '') {  echo $_POST['dob'];} ?>" class="input-control" />
                                                       
                                            </p>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <select class="form-control" placeholder="Gender" name="gender">
                                            <option value="">Select Gender</option>
                                            <option <?php if (isset($_POST['gender']) && $_POST['gender'] == 'male') {  echo "selected";} ?> value="male">Male</option>
                                            <option <?php if (isset($_POST['gender']) && $_POST['gender'] == 'female') {  echo "selected";} ?> value="female">Female</option>
                                        </select>
                                    </div>
                                   
                                    <div class="form-group col-md-3">
                                            <select class="form-control" placeholder="Marital Status" name="marital_status">
                                                <option value="">Select Marital Status</option>
                                                <option <?php if (isset($_POST['marital_status']) && $_POST['marital_status'] == 'Single') {  echo "selected";} ?> value="Single">Single</option>
                                                <option <?php if (isset($_POST['marital_status']) && $_POST['marital_status'] == 'Married') {  echo "selected";} ?> value="Married">Married</option>
                                                <option <?php if (isset($_POST['marital_status']) && $_POST['marital_status'] == 'Separated') {  echo "selected";} ?> value="Separated">Separated</option>
                                                <option <?php if (isset($_POST['marital_status']) && $_POST['marital_status'] == 'Widowed') {  echo "selected";} ?> value="Widowed">Widowed</option>
                                                <option <?php if (isset($_POST['marital_status']) && $_POST['marital_status'] == 'Divorced') {  echo "selected";} ?> value="Divorced">Divorced</option>
                                            </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                            <select class="form-control" placeholder="category" name="category">
                                                <option value="">Select Category</option>

                                                <option <?php if (isset($_POST['category']) && $_POST['category'] == 'General') {  echo "selected";} ?> value="General">General</option>
                                                <option <?php if (isset($_POST['category']) && $_POST['category'] == 'OBC') {  echo "selected";} ?> value="OBC">OBC</option>
                                                <option <?php if (isset($_POST['category']) && $_POST['category'] == 'SC') {  echo "selected";} ?> value="SC">SC</option>
                                                <option <?php if (isset($_POST['category']) && $_POST['category'] == 'ST') {  echo "selected";} ?> value="ST">ST</option>
                                                <option <?php if (isset($_POST['category']) && $_POST['category'] == 'PH') {  echo "selected";} ?> value="PH">PH</option>
                                            </select>
                                    </div>
                                    
                                    <div class="form-group col-md-3">
                                         <select id="nationality" name="nationality" class="form-control">
                                            <option value="">Select Nationality</option>  
                                            <?php
                                            $countries = DB::query("select * from country");
                                              foreach ($countries as $val) {
                                                  $selected = '';
                                                  if($val['country_name']==$_POST['nationality']){
                                                      $selected = 'selected="selected"';
                                                  }
                                                  echo '<option value="'.$val['country_name'].'" '.$selected.'>'.$val['country_name'].'</option>';
                                              }
                                              ?>
                                          </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <select class="form-control" placeholder="domicile" name="domicile">
                                            <option value="">Domicile Residence </option>
                                            <option <?php if (isset($_POST['domicile']) && $_POST['domicile'] == 'yes') {  echo "selected";} ?> value="yes">Yes</option>
                                            <option <?php if (isset($_POST['domicile']) && $_POST['domicile'] == 'no') {  echo "selected";} ?> value="no">No</option>
                                        </select>
                                    </div>

                                    <?php $centers = DB::query("select * from exam_centers"); ?>
                                    <div class="form-group col-md-3">
                                        <select id="exam_center1" name="exam_center1" class="form-control">
                                        <option value="">Center Name First Choice</option>
                                          <?php
                                          foreach ($centers as $val) {
                                              $selected = '';
                                              if($val['city']==$_POST['exam_center1']){
                                                  $selected = 'selected="selected"';
                                              }
                                              echo '<option value="'.$val['city'].'" '.$selected.'>'.$val['city'].'</option>';
                                          }
                                          ?>
                                      </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <select id="exam_center2" name="exam_center2" class="form-control">
                                        <option value="">Center Name Second Choice</option>
                                          <?php
                                          foreach ($centers as $val) {
                                              $selected = '';
                                              if($val['city']==$_POST['exam_center2']){
                                                  $selected = 'selected="selected"';
                                              }
                                              echo '<option value="'.$val['city'].'" '.$selected.'>'.$val['city'].'</option>';
                                          }
                                          ?>
                                      </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <select id="exam_center3" name="exam_center3" class="form-control">
                                        <option value="">Center Name Third Choice</option>
                                          <?php
                                          foreach ($centers as $val) {
                                              $selected = '';
                                              if($val['city']==$_POST['exam_center3']){
                                                  $selected = 'selected="selected"';
                                              }
                                              echo '<option value="'.$val['city'].'" '.$selected.'>'.$val['city'].'</option>';
                                          }
                                          ?>
                                      </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <select id="twelft_pass_year" name="twelft_pass_year" class="form-control">
                                            <option value="">Select Twelft Pass Year</option>
                                              <?php
                                              foreach ($years as $key => $val) {
                                                  $selected = '';
                                                  if($key ==$_POST['twelft_pass_year']){
                                                      $selected = 'selected="selected"';
                                                  }
                                                  echo '<option value="'.$key.'" '.$selected.'>'.$key.'</option>';
                                              }
                                              ?>
                                          </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <select id="specialization_choice1" name="specialization_choice1" class="form-control" >
                                            <option value="">Select Specialization Choice1</option>
                                              <?php
                                              foreach ($specialisations as $val) {
                                                  $selected = '';
                                                  if($val==$_POST['specialization_choice1']){
                                                      $selected = 'selected="selected"';
                                                  }
                                                  echo '<option value="'.$val.'" '.$selected.'>'.$val.'</option>';
                                              }
                                              ?>
                                            </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <select id="specialization_choice2" name="specialization_choice2" class="form-control" >
                                            <option value="">Select Specialization Choice2</option>
                                              <?php
                                              foreach ($specialisations as $val) {
                                                  $selected = '';
                                                  if($val==$_POST['specialization_choice2']){
                                                      $selected = 'selected="selected"';
                                                  }
                                                  echo '<option value="'.$val.'" '.$selected.'>'.$val.'</option>';
                                              }
                                              ?>
                                            </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <select id="specialization_choice3" name="specialization_choice3" class="form-control" >
                                            <option value="">Select Specialization Choice3</option>

                                              <?php
                                              foreach ($specialisations as $val) {
                                                  $selected = '';
                                                  if($val==$_POST['specialization_choice3']){
                                                      $selected = 'selected="selected"';
                                                  }
                                                  echo '<option value="'.$val.'" '.$selected.'>'.$val.'</option>';
                                              }
                                              ?>
                                            </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                      <button class="btn btn-primary" type="submit"><i class="fa fa-filter"></i> Filter</button>
                                    </div>
                                    <div class="form-group col-md-2">
                                      <a class="btn btn-primary" href="users_list.php"><i class="fa fa-filter"></i> Reset</a>
                                     
                                    </div>
                                    <div class="form-group col-md-2" >
                                          <a class="btn btn-primary" onclick="Myexcel(this);"><i class="fa fa-download"></i> Download</a>
                                         
                                        </div>
                                    </form> 
                                    

                                    <!-- END Block Content -->
                                </div>
                                <!-- END Block -->
                            </div>
                            
                            <form action="user-export.php" method="post" class="myexcel">
                                <input type="hidden" name="search_first_name" value="<?php if(isset($_POST['first_name'])){ echo $_POST['first_name']; }else{ echo ''; } ?>">

                                <input type="hidden" name="search_email" value="<?php if(isset($_POST['email'])){ echo $_POST['email']; }else{ echo ''; } ?>">

                                <input type="hidden" name="search_phone" value="<?php if(isset($_POST['phone'])){ echo $_POST['phone']; }else{ echo ''; } ?>">

                                <input type="hidden" name="search_payment" value="<?php if(isset($_POST['payment_status'])){ echo $_POST['payment_status']; }else{ echo ''; } ?>"> 

                                <input type="hidden" name="search_status"  value="<?php if(isset($_POST['status'])){ echo $_POST['status']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_from_date"  value="<?php if(isset($_POST['from_date'])){ echo $_POST['from_date']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_to_date"  value="<?php if(isset($_POST['to_date'])){ echo $_POST['to_date']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_Programme"  value="<?php if(isset($_POST['Programme'])){ echo $_POST['Programme']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_dob"  value="<?php if(isset($_POST['dob'])){ echo $_POST['dob']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_gender"  value="<?php if(isset($_POST['gender'])){ echo $_POST['gender']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_marital_status"  value="<?php if(isset($_POST['marital_status'])){ echo $_POST['marital_status']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_category"  value="<?php if(isset($_POST['category'])){ echo $_POST['category']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_nationality"  value="<?php if(isset($_POST['nationality'])){ echo $_POST['nationality']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_domicile"  value="<?php if(isset($_POST['domicile'])){ echo $_POST['domicile']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_exam_center1"  value="<?php if(isset($_POST['exam_center1'])){ echo $_POST['exam_center1']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_exam_center2"  value="<?php if(isset($_POST['exam_center2'])){ echo $_POST['exam_center2']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_exam_center3"  value="<?php if(isset($_POST['exam_center3'])){ echo $_POST['exam_center3']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_twelft_pass_year"  value="<?php if(isset($_POST['twelft_pass_year'])){ echo $_POST['twelft_pass_year']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_specialization_choice1"  value="<?php if(isset($_POST['specialization_choice1'])){ echo $_POST['specialization_choice1']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_specialization_choice2"  value="<?php if(isset($_POST['specialization_choice2'])){ echo $_POST['specialization_choice2']; }else{ echo ''; } ?>" >

                                <input type="hidden" name="search_specialization_choice3"  value="<?php if(isset($_POST['specialization_choice3'])){ echo $_POST['specialization_choice3']; }else{ echo ''; } ?>" >
                            </form>

                             

                            
                        </div>
                            </div>
                            <div class="row">
                            <div class="table-responsive">
                                <?php
									    // how many rows to show per page
					                	$rowsPerPage = 20;
					              	    // by default we show first page
					                	$pageNum = 1;
					              	    // if $_GET['page'] defined, use it as page number
					                    // if $_GET['page'] defined, use it as page number
					                	if(!empty($_GET['page']) && (int)$_GET['page'] > 0){
					                    	$pageNum = (int)$_GET['page'];
					                  	} else {
					                    	$pageNum =  1;
					               		}
					                        
					              	    // counting the offset
				                	$offset = ($pageNum - 1) * $rowsPerPage;

                                        $sqlquery   =    "SELECT users.id AS UserID, users.*, user_details.* FROM users";

                                        $sqlquery   =    $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";

                                         $sqlquery   =    $sqlquery . " WHERE email_verified = 1";

                                        if(isset($_POST['first_name'] ) && $_POST['first_name'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND users.first_name LIKE '%".$_POST['first_name']."%' ";
                                        }
                                        if(isset($_POST['email']) && $_POST['email']  != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND users.email LIKE  '%".str_replace(' ', '', $_POST['email'])."%' ";
                                        }
                                        if(isset($_POST['phone']) && $_POST['phone'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.phone LIKE '%".$_POST['phone']."%' ";
                                        }
                                        if(isset($_POST['payment_status']) && $_POST['payment_status']  != "" )
                                        {            
                                            
                                            if ($_POST['payment_status']  == "notdone") {
                                                 $sqlquery   =    $sqlquery . " AND users.payment_status IS NULL";
                                            }else{

                                            $sqlquery   =    $sqlquery . " AND users.payment_status = '".$_POST['payment_status']."'";
                                            }
                                        }
                                        if(isset($_POST['status']) && $_POST['status']  != "" )
                                        {            
                                          $sqlquery   =    $sqlquery . " AND users.status = '".$_POST['status']."'";
                                        }
                                        if($_POST['from_date'] != "" && $_POST['to_date']  != "" )
                                        {            
                                            $from = date('Y-m-d H:i:s',strtotime($_POST['from_date']));
                                            $to = date('Y-m-d H:i:s',strtotime($_POST['to_date']));

                                            $sqlquery   =    $sqlquery . " AND users.updated_at BETWEEN '".$from."' AND '".$to."'";
                                        }
                                        if( $_POST['Programme'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.Programme = '".$_POST['Programme']."' ";
                                        }

                                        if($_POST['dob'] != "")
                                        {            
                                            $dob = date('Y-m-d H:i:s',strtotime($_POST['dob']));
                                            $sqlquery   =    $sqlquery . " AND users.dob LIKE '".$dob."' ";
                                        }

                                        if( $_POST['gender'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.gender = '".$_POST['gender']."' ";
                                        }
                                        if( $_POST['marital_status'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.marital_status = '".$_POST['marital_status']."' ";
                                        }

                                        if($_POST['category'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.category = '".$_POST['category']."' ";
                                        }
                                        if( $_POST['nationality'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.nationality = '".$_POST['nationality']."' ";
                                        }
                                        if( $_POST['domicile'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.domicile = '".$_POST['domicile']."' ";
                                        }
                                        if( $_POST['exam_center1'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.exam_center1 = '".$_POST['exam_center1']."' ";
                                        }
                                        if( $_POST['exam_center2'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.exam_center2 = '".$_POST['exam_center2']."' ";
                                        }
                                        if( $_POST['exam_center3'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.exam_center3 = '".$_POST['exam_center3']."' ";
                                        }
                                        if( $_POST['twelft_pass_year'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.twelft_pass_year = '".$_POST['twelft_pass_year']."' ";
                                        }
                                        if($_POST['specialization_choice1'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.specialization_choice1 = '".$_POST['specialization_choice1']."' ";
                                        }

                                        if($_POST['specialization_choice2'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.specialization_choice2 = '".$_POST['specialization_choice2']."' ";
                                        }
                                        if( $_POST['specialization_choice3'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.specialization_choice3 = '".$_POST['specialization_choice3']."' ";
                                        }

                                        $sqlquery   =    $sqlquery . " ORDER BY users.id DESC LIMIT $offset, $rowsPerPage";

                                        $sqlquery = DB::query($sqlquery);

    				                    $counter = DB::count();

    									if ($counter > 0) 
    				                    { 
    				     		?>
                                <table class="table table-striped table-bordered table-vcenter">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 50px;">S.no</th>
                                            <th>Full Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Enroll. no.</th>
                                            <th>Payment Status</th>
                                            <th>Status</th>
                                            <th class="text-center" style="width: 100px;"><i class="fa fa-flash"></i></th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody class="mytabledata">
                                    <?php
		                            	$intcnt=0;
		                              	foreach ($sqlquery as $value)
		                              	{
		                                 	$intcnt = $intcnt+1;                    
		                          	?>
                                        <tr>
                                            <td class="text-center"><?php echo $intcnt; ?></td>

                                            <td><?php echo ucwords($value['first_name'].' '.$value['middle_name'].' '.$value['last_name']); ?></td>

                                            <td><?php echo $value['email']; ?></td>
                                            <td><?php echo $value['phone']; ?></td>
                                            <td><?php echo $value['enroll_id']; ?></td>
                                            <td><?php if ($value['payment_status'] != NULL) {
                                            	echo $value['payment_status'];
                                            }else{
                                            	echo "Not Done";
                                            }  ?></td>
                                            
                                            <td>
                                                 <?php if($value['status']==0){?>
                                                    <span class="label label-warning">Pending..</span>
                                                    <?php } else { ?>
                                                    <span class="label label-success">Approved</span>
                                                    <?php } ?>
                                            </td>
                                          
                                            <td class="text-center">
                                                <?php if ($value['status'] == 0 && $value['email_sent'] == 1 &&  $value['payment_status'] == 'success') {?>
                                                <a href="#" title="Approve" class="btn btn-effect-ripple btn-xs btn-default" onClick="UpdateRecord(<?php echo $value['UserID']; ?>);"><i class="fa fa-check"></i></a>
                                                <?php } else{ ?>

                                                <a href="#" title="Approve" class="btn btn-effect-ripple btn-xs btn-default" onClick="UpdateRecord(<?php echo $value['UserID']; ?>);" style="display: none;"><i class="fa fa-check"></i></a>

                                                
                                                <?php } ?>

                                                <a href="view_userdetail.php?id=<?php echo $value['UserID'];?>" title="View User Details" class="btn btn-effect-ripple btn-xs btn-default"><i class="fa fa-eye"></i></a>

                                            <a href="delete_user.php?id=<?php echo $value['UserID'];?>" data-toggle="modal" title="Delete User" class="btn btn-effect-ripple btn-xs btn-danger detelebtn" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-times"></i></a>
                                            </td>
                                           
                                        </tr>
                                        <div id="modal-fade" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	                                        <div class="modal-dialog">
	                                            <div class="modal-content">
	                                                <div class="modal-header">
	                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                                                    <h3 class="modal-title"><strong>Delete File</strong></h3>
	                                                </div>
	                                                <div class="modal-body">
	                                               		Are You want to delete Specialization <span class="showuser" style="font-weight: bold;"></span> ?.
	                                                </div>
	                                                <div class="modal-footer">
	                                                    <a href="#" class="btn btn-effect-ripple btn-primary myprocesdelete">Delete</a>
	                                                    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Close</button>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
                                	<?php
                                		}
                                	?>
                                    </tbody>
                                </table>
                              	<?php
                              		}else{										
                              	?>
                              	<table class="table table-striped table-bordered table-vcenter">
                              		<tr>
                              			<td align="center">No Record Found</td>
                              		</tr>
                              	</table>
                              	<?php } ?>
                                
                                <div class="col-md-12">
                                <div class="dataTables_paginate paging_bootstrap mypagidataul" id="sample_1_paginate" style="float: center;">
                                    <ul class="pagination pagination-sm remove-margin" style="visibility: visible;">
                                            <?php
				                                    // ... the previous code
				                                // how many rows we have in database
				                                $sqlquery   =   "SELECT * FROM users";
				                                $sqlquery   =    $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";

		                                         $sqlquery   =    $sqlquery . " WHERE email_verified = 1";

		                                        if(isset($_POST['first_name'] ) && $_POST['first_name'] != "")
		                                        {            
		                                            $sqlquery   =    $sqlquery . " AND users.first_name LIKE '%".$_POST['first_name']."%' ";
		                                        }
		                                        if(isset($_POST['email']) && $_POST['email']  != "")
		                                        {            
		                                            $sqlquery   =    $sqlquery . " AND users.email LIKE '%".str_replace(' ', '', $_POST['email'])."%' ";
		                                        }
		                                        if(isset($_POST['phone']) && $_POST['phone'] != "")
		                                        {            
		                                            $sqlquery   =    $sqlquery . " AND user_details.phone LIKE '%".$_POST['phone']."%' ";
		                                        }
                                                if(isset($_POST['payment_status']) && $_POST['payment_status']  != "" )
                                                {            
                                            
                                                if ($_POST['payment_status']  == "notdone") 
                                                {
                                                     $sqlquery   =    $sqlquery . " AND users.payment_status IS NULL";
                                                }else
                                                {

                                                    $sqlquery   =    $sqlquery . " AND users.payment_status = '".$_POST['payment_status']."'";
                                                }
                                                }
                                                if(isset($_POST['status']) && $_POST['status']  != "" )
                                                {            
                                                  $sqlquery   =    $sqlquery . " AND users.status = '".$_POST['status']."'";
                                                }
                                                if($_POST['from_date'] != "" && $_POST['to_date']  != "" )
                                                {            
                                                    $from = date('Y-m-d H:i:s',strtotime($_POST['from_date']));
                                                    $to = date('Y-m-d H:i:s',strtotime($_POST['to_date']));

                                                    $sqlquery   =    $sqlquery . " AND users.updated_at BETWEEN '".$from."' AND '".$to."'";
                                                }
                                                if($_POST['Programme'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.Programme = '".$_POST['Programme']."' ";
                                        }

                                        if($_POST['dob'] != "")
                                        {            
                                            $dob = date('Y-m-d H:i:s',strtotime($_POST['dob']));
                                            $sqlquery   =    $sqlquery . " AND users.dob LIKE '".$dob."' ";
                                        }

                                        if( $_POST['gender'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.gender = '".$_POST['gender']."' ";
                                        }
                                        if($_POST['marital_status'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.marital_status = '".$_POST['marital_status']."' ";
                                        }

                                        if($_POST['category'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.category = '".$_POST['category']."' ";
                                        }
                                        if($_POST['nationality'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.nationality = '".$_POST['nationality']."' ";
                                        }
                                        if($_POST['domicile'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.domicile = '".$_POST['domicile']."' ";
                                        }
                                        if( $_POST['exam_center1'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.exam_center1 = '".$_POST['exam_center1']."' ";
                                        }
                                        if( $_POST['exam_center2'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.exam_center2 = '".$_POST['exam_center2']."' ";
                                        }
                                        if( $_POST['exam_center3'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.exam_center3 = '".$_POST['exam_center3']."' ";
                                        }
                                        if($_POST['twelft_pass_year'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.twelft_pass_year = '".$_POST['twelft_pass_year']."' ";
                                        }
                                        if( $_POST['specialization_choice1'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.specialization_choice1 = '".$_POST['specialization_choice1']."' ";
                                        }

                                        if( $_POST['specialization_choice2'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.specialization_choice2 = '".$_POST['specialization_choice2']."' ";
                                        }
                                        if( $_POST['specialization_choice3'] != "")
                                        {            
                                            $sqlquery   =    $sqlquery . " AND user_details.specialization_choice3 = '".$_POST['specialization_choice3']."' ";
                                        }
		                                        $sqlquery = DB::query($sqlquery);
		                                        $numrows = DB::count();
				                                
				                                 $maxPage1 = ceil($numrows/$rowsPerPage);
                                                 $maxPage = 150;
	                                             if($maxPage1 < $maxPage)
	                                            {
	                                                $maxPage2 = $maxPage1;
	                                            }
	                                            else
	                                            {
	                                                $maxPage2 = $maxPage;
	                                            }
	                                            // print the link to access each page
	                                            $self = $_SERVER['PHP_SELF'];
	                                            $nav  = '';
	                                            for($page = 1; $page <= $maxPage2; $page++)
	                                            {
	                                                if ($page == $pageNum)
	                                                {
	                                                        $nav .= " <li class='active disabled'><a>$page</a></li> "; // no need to create a link to current page
	                                                }
	                                                        else
	                                                {
	                                                        $nav .= " <li><a onclick=\"pagination(this);\" data-page=\"$page\">$page</a> </li>";
	                                                } 
	                                                }

	                                                // creating previous and next link
	                                                // plus the link to go straight to
	                                                // the first and last page

	                                                if ($pageNum > 1)
	                                                {
	                                                $page  = $pageNum - 1;
	                                                $prev  = " <li><a onclick=\"pagination(this);\" data-page=\"$page\"><i class='fa fa-chevron-left'></i></a> </li>";

	                                                $first = " <li><a onclick=\"pagination(this);\" data-page=\"1\"><i class='fa fa-step-backward'></i></a></li> ";
	                                                } 
	                                                else
	                                                {
	                                                $prev  = '&nbsp;'; // we're on page one, don't print previous link
	                                                $first = '&nbsp;'; // nor the first page link
	                                                }

	                                                if ($pageNum < $maxPage2)
	                                            {
	                                                $page = $pageNum + 1;
	                                                $next = " <li><a onclick=\"pagination(this);\" data-page=\"$page\"><i class='fa fa-chevron-right'></i></a></li> ";

	                                                $last = " <li><a onclick=\"pagination(this);\" data-page=\"$maxPage1\" ><i class='fa fa-step-forward'></i></a></li> ";
	                                                } 
	                                                else
	                                            {
	                                                $next = '&nbsp;'; // we're on the last page, don't print next link
	                                                $last = '&nbsp;'; // nor the last page link
	                                                }

	                                            // print the navigation link
	                                                echo $first . $prev . $nav . $next . $last;

				                                    // and close the database connection

                                                    // ... and we're done! 
                                            ?>
                                    </ul>
                                    <?php  echo "Total recored : ".$numrows; ?>
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                        <!-- END Datatables Block -->
                    </div>
                    <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>


        <!-- Load and execute javascript code used only in this page -->
        <script src="js/pages/uiTables.js"></script>
        <script>$(function(){ UiTables.init(); });</script>
        <script src="js/pages/formsComponents.js"></script>
        <script>$(function(){ FormsComponents.init(); });</script>
    </body>
</html>
<script type="text/javascript">
	$( document ).ready(function() {
		$('.detelebtn').click(function() {
			$('#modal-fade').fadeIn("fast");
			var id=$(this).attr('id');
			var username=$(this).attr('username');
			$('#modal-fade .showuser').text(username);
			$('#modal-fade .myprocesdelete').attr('href','deletevideo.php?id='+id+'&delete=1');
		});
	});
</script>
<script>
	function pagination(elem){
        var page = $(elem).attr('data-page');
        var email = '<?php echo $_POST['email']; ?>';
        var first_name = '<?php echo $_POST['first_name']; ?>';
        var phone = '<?php echo $_POST['phone']; ?>';
        var payment_status = '<?php echo $_POST['payment_status']; ?>';
        var status = '<?php echo $_POST['status']; ?>';
        var from_date = '<?php echo $_POST['from_date']; ?>';
        var to_date = '<?php echo $_POST['to_date']; ?>';
        var Programme = '<?php echo $_POST['Programme']; ?>';
        var dob = '<?php echo $_POST['dob']; ?>';
        var gender = '<?php echo $_POST['gender']; ?>';
        var marital_status = '<?php echo $_POST['marital_status']; ?>';
        var category = '<?php echo $_POST['category']; ?>';
        var nationality = '<?php echo $_POST['nationality']; ?>';
        var domicile = '<?php echo $_POST['domicile']; ?>';
        var exam_center1 = '<?php echo $_POST['exam_center1']; ?>';
        var exam_center2 = '<?php echo $_POST['exam_center2']; ?>';
        var exam_center3 = '<?php echo $_POST['exam_center3']; ?>';
        var twelft_pass_year = '<?php echo $_POST['twelft_pass_year']; ?>';
        var specialization_choice1 = '<?php echo $_POST['specialization_choice1']; ?>';
        var specialization_choice2 = '<?php echo $_POST['specialization_choice2']; ?>';
        var specialization_choice3 = '<?php echo $_POST['specialization_choice3']; ?>';
			$.ajax({
				type: "POST",
				url: "ajax.php",
		data:"page="+page+"&first_name="+first_name+"&phone="+phone+"&email="+email+"&payment_status="+payment_status+"&status="+status+"&from_date="+from_date+"&to_date="+to_date+"&Programme="+Programme+"&dob="+dob+"&gender="+gender+"&marital_status="+marital_status+"&category="+category+"&nationality="+nationality+"&domicile="+domicile+"&exam_center1="+exam_center1+"&exam_center2="+exam_center2+"&exam_center3="+exam_center3+"&twelft_pass_year="+twelft_pass_year+"&specialization_choice1="+specialization_choice1+"&specialization_choice2="+specialization_choice2+"&specialization_choice3="+specialization_choice3+"&action=users",
				success: function (result) 
					{
						var aee = result.split('*');
		   				var a1 = aee[0];
				   		var a2 = aee[1];
				   		$('.mytabledata').html(a1);
				   		$('.mypagidataul').html('');
				   		$('.mypagidataul').html(a2);
						//$(".make-switch.init").removeClass('init').bootstrapSwitch();
					}
			});
	}
</script>
<script>
function MyReset(elem){
//alert(1);
    $('.myreset').submit();
}
function Myexcel(elem){
    $('.myexcel').submit();
}
</script>
<script type="text/javascript">
    function UpdateRecord(id){
      jQuery.ajax({
       type: "POST",
       url: "update_user_status.php",
       data: 'id='+id,
       cache: false,
       success: function(response)
       {
         alert("Successfully updated");
         location.reload();
       }
     });
    }
</script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
$.datepicker.setDefaults({
showOn: "button",
buttonImage: "datepicker.png",
buttonText: "Date Picker",
buttonImageOnly: true,
dateFormat: 'yy-mm-dd'  
});
$(function() {
$("#from").datepicker();
$("#to").datepicker();
$("#dob").datepicker();
});
</script>
<?php /*$DB -> close();*/ ?>