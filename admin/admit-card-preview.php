<?php require_once('rightusercheck.php'); ?>
<?php 
    require_once('../meekrodb.2.3.class.php');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>IICD | Admit Card</title>

<link rel="stylesheet" type="text/css" href="../css/stylead.css">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0-RC1/css/bootstrap-datepicker3.standalone.min.css'>
  <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>
<body>
	<div class="admission-form">
        <div class="container header-first">
        <div class="mrg-btm">
                            <div class="col-md-2 text-center">
                             <div class="company-logo">
                                    <a href="#">
                                        <img src="../images/logo1.png" alt=""> 
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-7 text-center">
                             <div class="company-logo">
                                    <a href="#">
                                        <img src="../images/indian1.png" alt=""> 
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                             <div class="company-logo">
                                    <a href="#">
                                        <img src="../images/admit1.png" alt=""> 
                                    </a>
                                </div>
                            </div>
                        </div>
                      </div>
          <div class="clearfix"></div>
          <?php
          $id = $_GET['id'];
          $sqlquery   =    "SELECT users.id AS UserID, users.*, user_details.Programme,user_details.fathers_name,user_details.dob,user_details.phone,user_details.exam_center1,user_details.applicant_photo,user_details.dob_certificate,user_details.id_proof,user_details.signatures,exam_centers.center_name,exam_centers.city  FROM users";

            $sqlquery   =    $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";
            $sqlquery   =    $sqlquery . " LEFT JOIN exam_centers ON user_details.exam_center1 = exam_centers.city";
            $sqlquery   =    $sqlquery . " WHERE users.status = 1 AND users.id = '".$id."'";
            $result = DB::queryFirstRow($sqlquery);

           ?>
       <div class="admit-card-form">
         <div class="container">
           <ul class="first-form">
             <li class="clearfix">
              <span>Name</span>
              <span><strong><?php echo  $result['first_name'].' '.$result['middle_name'].' '.$result['last_name'];?></strong></span>
              <span>Enrollment No.</span>
              <span><?php echo  $result['enroll_id'];?></span>
            </li>
             <li class="clearfix">
              <span>Father's Name</span>
              <span><strong><?php echo  $result['fathers_name'];?></strong></span>
              <span>Programme</span>
              <span><?php echo  $result['Programme'];?></span>
            </li>
             <li class="clearfix">
              <span>Date of Birth</span>
              <span><strong><?php echo   date('d/m/Y',strtotime($result['dob']));?></strong></span>
              <span>Mobile No.</span>
              <span><?php echo  $result['phone'];?></span>
            </li>
             <li class="clearfix">
              <span>Date of Entrance Test (Part A)</span>
              <span><strong>15/04/2018</strong></span>
              <span>Test Center</span>
              <span><?php echo  $result['center_name'];?></span>
            </li>
             <li class="clearfix">
              <span>Address of Test Center</span>
              <?php $examcenter = "SELECT address FROM exam_centers WHERE city = '".$result['exam_center1']."'";
              $centerName = DB::queryFirstRow($examcenter);

                ?>
              <span><strong><?php echo $centerName['address']; ?></strong></span>
              
            </li>
           </ul>
           <h3>Entrance Test Attendance:</h3>
           <div class="secound-form-box clearfix">
           <ul class="clearfix">
             <li><h1>Part A</h1>
             <p>General Awareness, Creativity & Perception Test</p>
             <h2>(10:00 AM to 1:00 PM)</h2>
             </li>
              
                <li><h1>Part B</h1>
             <p>Material, Color & Conceptual Test</p>
             <h2>(10:00 AM to 1:00 PM)</h2>
             </li>
               <li><h1>Part B</h1>
             <p>Personal Interview</p>
             <h2>(2:00 PM onwards)</h2>
             </li>
              <li><h4>Invigilator Initial</h4></li>
              <li><h4>Invigilator Initial</h4></li>
              <li><h4>Invigilator Initial</h4></li>
           </ul>
         </div>
         <div class="secound-form-photo">
         <ul>
         <li><img src="../images/<?php echo $result['applicant_photo']; ?>" width="180" height="240" alt="Applicant Photo"/></li>
         <li><img src="../images/<?php echo $result['signatures']; ?>" width="180" height="240" alt="Applicant Signature"/></li>
         <li><img src="../images/<?php echo $result['dob_certificate']; ?>" width="180" height="240" alt="Tenth Mark sheet"/></li>
         <li><img src="../images/<?php echo $result['id_proof']; ?>" width="180" height="240" alt="Photo ID"/></li>
         </ul>
         <div class="clearfix"></div>
         </div>
         
         <h5>Deputy Registration Academics
         
           <img src="../images/signature.png" alt="signature"/> </h5>

           <?php $settings = "SELECT * FROM settings WHERE id = 1";
              $settingsName = DB::queryFirstRow($settings);

                ?>
         
         <?php echo $settingsName['value']; ?>
         </div>
       </div>
       <div class="clearfix"></div>
       <hr>
    
   	</div>

<script type="text/javascript">
    function SendEmail(id){
      event.preventDefault();
        $("button[name='admitcard_send']").text("wait...");
        $("button[name='admitcard_send']").attr("disabled", "disabled");
      $.ajax({
       type: "POST",
       dataType:"json",
       url: "send_admitcard.php",
       data:"id="+id,
       success: function(response)
       {   
          alert('Email sent successfully');
              location.reload();
       }
     });
    }

</script>

</body>
</html>
