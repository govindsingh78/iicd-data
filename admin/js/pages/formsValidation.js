/*
 *  Document   : formsValidation.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Forms Validation page
 */

var FormsValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-pullUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                ignore: [],
              	debug: false,
                rules: {
                    'fullname': {
                        required: true,
                        minlength: 3
                    },
                    'username': {
                        required: true,
                        minlength: 3
                    },
                    'email': {
                        required: true,
                        email: true
                    },
                    'password': {
                        required: true,
                        minlength: 5
                    },
                    'cpassword': {
                        required: true,
                        equalTo: '#password'
                    },
                    'address': {
                        required: true,
                        minlength: 5
                    },
                    'rights': {
                        required: true
                    },
                    'status': {
                        required: true
                    },
                    'phoneno': {
                        required: true,
                        number: true
                    },
                    'allfielsrequired' : {
						required: true,
					}
                },
                messages: {
                    'fullname': {
                        required: 'Please enter a fullname',
                        minlength: 'Your fullname must consist of at least 3 characters'
                    },
                    'username': {
                        required: 'Please enter a username',
                        minlength: 'Your username must consist of at least 3 characters'
                    },
                    'email': 'Please enter a valid email address',
                    'password': {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 5 characters long'
                    },
                    'cpassword': {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 5 characters long',
                        equalTo: 'Please enter the same password as above'
                    },
                    'mobile': {
                        required: 'Please enter a number!',
                        minlength: 'Enter your 10 digit mobile number'
                    },
                    'address': 'Enter your full address',
                    'allfielsrequired': 'This field is required',
                    'rights': 'Please select a user role!',
                    'status': 'Please select a status!',
                    'phoneno': 'Please enter a number!'
                }
            });
        }
    };
}();