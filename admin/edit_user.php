<?php require_once('rightusercheck.php'); ?>
<?php if(!empty($_SESSION['adminyncrights']) && trim($_SESSION['adminyncrights']) != "Administrator") { header('Location : index.php'); } ?>
<?php 
    require_once('../meekrodb.2.3.class.php');
    $id = $_GET['id'];
    $fullname = $_GET['fullname'];
    $email = $_GET['email'];

    /*$DB = new DBConfig();
    $DB -> config();
    $DB -> conn(); */
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>IICD</title>

        <meta name="description" content="IICD">
        <meta name="author" content="">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        
    </head>
    <body>
        <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <!-- END Preloader -->

            <!-- Page Container -->
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
                <?php require_once('header.php'); ?>
                    <!-- Page content -->
                    <div id="page-content">
                        <!-- Datatables Block -->
                        <!-- Datatables is initialized in js/pages/uiTables.js -->
                        <div class="block full">
                        <form action="save_edit_user.php" method="post">
                         <div class="table-responsive">
                            <?php
                            $user = DB::queryFirstRow("SELECT * FROM user_details WHERE user_id=%i", $id);
                            ?>
                            <table class="table table-hover">
                              <tr>
                                <th colspan="2" style="text-align: center;font-size: 20px;background-color: #eee;">User Details</th>
                              </tr>
                              <tr>
                                <th>Full Name</th>
                                <td><?php echo $fullname; ?></td>
                              </tr>
                              <tr>
                                <th>Email</th>
                                <td><?php echo $email; ?></td>
                              </tr>
                              <tr>
                                <th>Programme</th>
                                <td>
                                  <?php 
                                    if (!empty($user['Programme'])) {
                                     echo $user['Programme'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Date of Birth</th>
                                <td>
                                <?php
                                  if (!empty($user['dob'])) {
                                   echo $user['dob'] ;
                                  }else{
                                    echo 'Not Available';
                                  }
                                ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Gender</th>
                                <td>
                                  <?php
                                    if (!empty($user['gender'])) {
                                     echo $user['gender'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                 </td>
                              </tr>
                              <tr>
                                <th>Marital Status</th>
                                <td>
                                  <?php
                                      if($user['marital_status'] == 1){
                                       echo 'Married'; 
                                      } else {
                                        echo 'Single';
                                      }
                                   ?>
                                 </td>
                              </tr>
                              <tr>
                                <th>Category</th>
                                <td>
                                  <?php 
                                    if (!empty($user['category'])) {
                                     echo $user['category'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Nationality</th>
                                <td>
                                  <?php
                                    if (!empty($user['nationality'])) {
                                     echo $user['nationality'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                 </td>
                              </tr>
                              <tr>
                                <th>Domicile</th>
                                <td>
                                  <?php
                                    if (!empty($user['domicile'])) {
                                     echo $user['domicile'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Fathers Name</th>
                                <td>
                                  <?php
                                    if (!empty($user['fathers_name'])) {
                                     echo $user['fathers_name'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                 </td>
                              </tr>
                              <tr>
                                <th>Mothers Name</th>
                                <td>
                                  <?php 
                                    if (!empty($user['mothers_name'])) {
                                     echo $user['mothers_name'] ;
                                    }else{
                                      echo 'Not Available';
                                    } 
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Phone Parents</th>
                                <td>
                                  <?php 
                                    if (!empty($user['phone_parents'])) {
                                     echo $user['phone_parents'] ;
                                    }else{
                                      echo 'Not Available';
                                    } 
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Email Parents</th>
                                <td>
                                  <?php 
                                    if (!empty($user['email_parents'])) {
                                     echo $user['email_parents'] ;
                                    }else{
                                      echo 'Not Available';
                                    } 
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Guardians Name</th>
                                <td>
                                  <?php 
                                    if (!empty($user['guardians_name'])) {
                                     echo $user['guardians_name'] ;
                                    }else{
                                      echo 'Not Available';
                                    } 
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Guardians Relation</th>
                                <td>
                                  <?php
                                    if (!empty($user['guardians_relation'])) {
                                     echo $user['guardians_relation'] ;
                                    }else{
                                      echo 'Not Available';
                                    } 
                                  ?>
                                 </td>
                              </tr>
                              <tr>
                                <th>Guardian Phone</th>
                                <td>
                                  <?php
                                    if (!empty($user['phone_guardian'])) {
                                     echo $user['phone_guardian'] ;
                                    }else{
                                      echo 'Not Available';
                                    } 
                                  ?>
                                 </td>
                              </tr>
                              <tr>
                                <th>Guardian Email</th>
                                <td>
                                  <?php
                                    if (!empty($user['email_guardian'])) {
                                     echo $user['email_guardian'] ;
                                    }else{
                                      echo 'Not Available';
                                    } 
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Craft Relation</th>
                                <td>
                                  <?php 
                                    if($user['craft_relation'] == 0){
                                        echo 'No'; 
                                    } else {
                                        echo 'Yes';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Family Income</th>
                                <td>
                                  <?php
                                    if (!empty($user['family_income'])) {
                                     echo $user['family_income'] ;
                                    }else{
                                      echo 'Not Available';
                                    } 
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Medical Info</th>
                                <td>
                                  <?php
                                    if (!empty($user['medical_info'])) {
                                     echo $user['medical_info'] ;
                                    }else{
                                      echo 'Not Available';
                                    } 
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th rowspan="2">Address</th>
                                <td><?php echo $user['address_line1']; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $user['address_line2']; ?></td>
                              </tr>
                              <tr>
                                <th>city</th>
                                <td>
                                  <?php 
                                    if (!empty($user['city'])) {
                                     echo $user['city'] ;
                                    }else{
                                      echo 'Not Available';
                                    } 
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>State</th>
                                <td>
                                  <?php 
                                    if (!empty($user['state'])) {
                                     echo $user['state'] ;
                                    }else{
                                      echo 'Not Available';
                                    } 
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Pin Code</th>
                                <td>
                                  <?php 
                                    if (!empty($user['pin_code'])) {
                                     echo $user['pin_code'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th rowspan="3">Exam Center</th>
                                <td><?php echo $user['exam_center1']; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $user['exam_center2']; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $user['exam_center3']; ?></td>
                              </tr>
                              <tr>
                                <th>Twelft pass year</th>
                                <td>
                                  <?php 
                                    if (!empty($user['twelft_pass_year'])) {
                                     echo $user['twelft_pass_year'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Twelft Stream</th>
                                <td>
                                  <?php
                                    if (!empty($user['twelft_stream'])) {
                                     echo $user['twelft_stream'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Twelft other stream</th>
                                <td>
                                  <?php 
                                    if (!empty($user['twelft_other_stream'])) {
                                     echo $user['twelft_other_stream'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Twelft board name</th>
                                <td>
                                  <?php 
                                    if (!empty($user['twelft_board_name'])) {
                                     echo $user['twelft_board_name'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Twelft Grade</th>
                                <td>
                                  <?php 
                                    if (!empty($user['twelft_grade'])) {
                                     echo $user['twelft_grade'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Degree pass year</th>
                                <td>
                                  <?php 
                                    if (!empty($user['degree_pass_year'])) {
                                     echo $user['degree_pass_year'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Degree stream</th>
                                <td>
                                  <?php 
                                    if (!empty($user['degree_stream'])) {
                                     echo $user['degree_stream'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Degree other stream</th>
                                <td>
                                  <?php 
                                    if (!empty($user['degree_other_stream'])) {
                                     echo $user['degree_other_stream'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Degree college university name</th>
                                <td>
                                  <?php
                                    if (!empty($user['degree_col_univ'])) {
                                     echo $user['degree_col_univ'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Degree Grade</th>
                                <td>
                                  <?php 
                                    if (!empty($user['degree_grade'])) {
                                     echo $user['degree_grade'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Language Hindi</th>
                                <td><?php 
                                    if($user['language_hindi'] == 0){
                                            echo 'No'; 
                                        } else {
                                            echo 'Yes';
                                        }
                                ?></td>
                              </tr>
                              <tr>
                                <th>Language English</th>
                                <td><?php
                                    if($user['language_english'] == 0){
                                                echo 'No'; 
                                            } else {
                                                echo 'Yes';
                                            }
                                 ?></td>
                              </tr>
                              <tr>
                                <th>Language Other</th>
                                <td>
                                  <?php 
                                    if (!empty($user['language_other'])) {
                                     echo $user['language_other'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th rowspan="3">Specialization Choice</th>
                                <td>
                                  <?php 
                                    if (!empty($user['specialization_choice1'])) {
                                     echo $user['specialization_choice1'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <?php 
                                    if (!empty($user['specialization_choice2'])) {
                                     echo $user['specialization_choice2'] ;
                                    }else{
                                      echo 'Not Available';
                                    }
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <?php
                                    if (isset($user['specialization_choice3'])) {
                                    echo $user['specialization_choice3'] ;
                                    }else{
                                    echo "Not Available";
                                    }  
                                  ?>
                                  </td>
                              </tr>
                              <tr>
                                <th>Applicant Photo</th>
                                <td>
                                  <?php 
                                    if (!empty($user['applicant_photo'])) {
                                     echo $user['applicant_photo'] ;
                                    }else{
                                    echo "Not Available";
                                    }  
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>DOB Certificate</th>
                                <td>
                                  <?php 
                                    if (!empty($user['dob_certificate'])) {
                                     echo $user['dob_certificate'] ;
                                    }else{
                                    echo "Not Available";
                                    }  
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>ID Proof</th>
                                <td>
                                  <?php 
                                    if (!empty($user['id_proof'])) {
                                     echo $user['id_proof'] ;
                                    }else{
                                    echo "Not Available";
                                    }  
                                  ?>
                                </td>
                              </tr>
                              <tr>
                                <th>Declaration</th>
                                <td>
                                  <?php
                                      if($user['declaration'] == 0){
                                          echo 'No'; 
                                      } else {
                                          echo 'Yes';
                                      }
                                  ?>
                                 </td>
                              </tr>
                            </table>
                            </div>
                        </div>
                        </form>
                        <!-- END Datatables Block -->
                    </div>
                    <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>

        <!-- Load and execute javascript code used only in this page -->
        <script src="js/pages/uiTables.js"></script>
        <script>$(function(){ UiTables.init(); });</script>
    </body>
</html>
<script type="text/javascript">
    $( document ).ready(function() {
        $('.detelebtn').click(function() {
            $('#modal-fade').fadeIn("fast");
            var id=$(this).attr('id');
            var username=$(this).attr('username');
            $('#modal-fade .showuser').text(username);
            $('#modal-fade .myprocesdelete').attr('href','deletevideo.php?id='+id+'&delete=1');
        });
    });
</script>

<?php /*$DB -> close();*/ ?>