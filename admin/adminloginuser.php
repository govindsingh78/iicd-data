<?php require_once('rightusercheck.php'); ?>
<?php if(!empty($_SESSION['adminyncrights']) && trim($_SESSION['adminyncrights']) != "Administrator") { header('Location : index.php'); } ?>
<?php 
    require_once('main.php');
    $DB = new DBConfig();
    $DB -> config();
    $DB -> conn();
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>IICD</title>

        <meta name="description" content="IICD">
        <meta name="author" content="">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!-- Page Wrapper -->
        <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <!-- END Preloader -->

            <!-- Page Container -->
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
               	<?php require_once('header.php'); ?>
                    <!-- Page content -->
                    <div id="page-content">
                        <!-- Validation Header -->
                        <div class="content-header">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="header-section">
                                        <h1>Admin User</h1>
                                    </div>
                                </div>
                                <div class="col-sm-6 hidden-xs">
                                    <div class="header-section">
                                        <ul class="breadcrumb breadcrumb-top">
                                            <li>Home</li>
                                            <li><a href="">Admin User</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Validation Header -->

                        <!-- Form Validation Content -->
                        <?php
                        	if(!empty($_GET['id'])) 
			                {
			                    $edit     = (int)$_GET['edit'];
			                    $id       = (int)$_GET['id'];
			                    $sqlquery = "SELECT tbladminuser.* FROM tbladminuser WHERE id = ".$id." order by id";
			                    $rsdata   = $DB ->getdata($sqlquery);   
			                    if (mysql_num_rows($rsdata) > 0) 
			                    {            
			                      while($rowdata = mysql_fetch_array($rsdata))
			                      {
			                        $fullname	=   trim($rowdata['fullname']);
			                        $username   =   trim($rowdata['username']);
			                        $email      =   trim($rowdata['email']);
			                        $cpassword  =   trim($rowdata['cpassword']);
			                        $password   =   null;
			                        $phoneno    =   trim($rowdata['phoneno']);
			                        $address    =   trim($rowdata['address']);
			                        $rights     =   trim($rowdata['rights']);
			                        $status     =   trim($rowdata['status']);
			                      }
			                    }
			                }
			                else
			                {
			                		$edit           =   0;
			                        $id             =   0;
			                        $fullname	    =   null;
			                        $username       =   null;
			                        $email          =   null;
			                        $password       =   null;
			                        $cpassword      =   null;
			                        $phoneno	    =   null;
			                        $address        =   null;
			                        $rights         =   null;
			                        $status         =   707;
			                }
			        	?>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <!-- Form Validation Block -->
                                <div class="block">
                                    <!-- Form Validation Title -->
                                    <div class="block-title">
                                        <h2>Admin User</h2>
                                    </div>
                                    <!-- END Form Validation Title -->

                                    <!-- Form Validation Form -->
                                    <form id="form-validation" action="adminloginusersave.php" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
                                    	<input type="hidden" name="edit" id="edit" value="<?php echo $edit;?>" />
                                    	<input type="hidden" name="id" id="id" value="<?php echo $id;?>" /> 
                                    	<div class="form-group">
                                            <label class="col-md-3 control-label" for="fullname">Full Name <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" id="fullname" name="fullname" value="<?php echo $fullname; ?>" class="form-control" placeholder="Choose a nice fullname..">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="val-username">Username <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" id="username" name="username" value="<?php echo $username; ?>" class="form-control" placeholder="Choose a nice username..">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="val-email">Email <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" id="email" name="email" value="<?php echo $email; ?>" class="form-control" placeholder="Enter your email..">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="val-password">Password <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="password" id="password" name="password" value="<?php echo $cpassword;?>" class="form-control" placeholder="Choose a good one..">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="cpassword">Confirm Password <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="password" id="cpassword" name="cpassword" value="<?php echo $cpassword;?>" class="form-control" placeholder="..and confirm it to be safe!">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="phoneno">Phone No. <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" id="phoneno" name="phoneno" value="<?php echo $phoneno; ?>" class="form-control" placeholder="Enter your mobile number..">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="address">Address <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <textarea id="address" name="address" rows="7" class="form-control" placeholder="Enter your full address.."><?php echo $address; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="val-skill">User Role <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <select id="rights" name="rights" class="form-control">
                                                    <option value="Administrator" <?php if ($rights == "Administrator") { echo "selected"; } ?>>Administrator</option>
                                                    <option value="Manager" <?php if ($rights == "Manager") { echo "selected"; } ?>>Manager</option>
				                            		<option value="Employee" <?php if ($rights == "Employee") { echo "selected"; } ?>>Employee</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="val-skill">Status <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <select id="val-skill" name="status" class="form-control">
                                                    <option value="707" <?php if ((int) $status == 707) { echo "selected"; } ?>>Enable</option>
				                            		<option value="505" <?php if ((int) $status == 505) { echo "selected"; } ?>>Disable</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group form-actions">
                                            <div class="col-md-8 col-md-offset-3">
                                                <button type="submit" class="btn btn-effect-ripple btn-primary">Submit</button>
                                                <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END Form Validation Form -->
                                </div>
                                <!-- END Form Validation Block -->
                            </div>
                        </div>
                        <!-- END Form Validation Content -->
                    </div>
                    <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

<?php require_once('footer.php'); ?>

    </body>
</html>
<?php $DB -> close(); ?>