<!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
        <script src="js/jquery.min.js"></script>
		<script src="js/jquery.fastLiveFilter.js"></script>
		<script>
		    $(function() {
		        $('#search_input').fastLiveFilter('#search_list');
		    });
		</script>
        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>
		
		<!-- Load and execute javascript code used only in this page -->
        <script src="js/pages/formsValidation.js"></script>
        <script>$(function(){ FormsValidation.init(); });</script>
        
        
        <!-- Load and execute javascript code used only in this page -->
        <script src="js/pages/uiTables.js"></script>
        <script>$(function(){ UiTables.init(); });</script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
        <script type="text/javascript" src="js/i18n/jquery-ui-timepicker-addon-i18n.min.js"></script>
