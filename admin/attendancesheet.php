<?php
require_once('../meekrodb.2.3.class.php');

      ?>
                              
            
                                <?php
                                  // how many rows to show per page
                              $rowsPerPage = 30;

                            // by default we show first page
                              $pageNum = 1;

                            // if $_GET['page'] defined, use it as page number
                              if(!empty($_GET['page']) && (int)$_GET['page'] > 0){
                                  $pageNum = (int)$_GET['page'];
                                } else {
                                  $pageNum =  1;
                              }
                                    
                            // counting the offset
                              $offset = ($pageNum - 1) * $rowsPerPage;
                                        $sqlquery   =    "SELECT users.id AS UserID, users.*, user_details.Programme,user_details.fathers_name,user_details.dob,user_details.phone,user_details.exam_center1,user_details.applicant_photo FROM users";

                                        $sqlquery   =    $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";
                                        $sqlquery   =    $sqlquery . " WHERE user_details.Programme = '".$_POST['program']."' AND user_details.exam_center1 = '".$_POST['center_name']."' AND users.pdf_sent = 1 ORDER BY RIGHT(users.enroll_id, 3) ASC";
                                        $result = DB::query($sqlquery);

                                $counter = DB::count();
                                if ($counter > 0) 
                                    { 
                                      $i = 1;
                                      $j = 0;
                                      ?>
                                      <div style="clear: both;width: 100%;float: left;margin-bottom: 4px;">
                                      <?php

                                            foreach ($result as $key=> $value)
                                            {
                                            
                                            if ($value['Programme'] == 'PG') {
                                                $pname = "Master of Vocation in Crafts and Design";
                                              }elseif($value['Programme'] == 'UG'){
                                                $pname = "4 Year Integrated Bachelors Programme (CFPD + B. VOC)";
                                              }else{
                                                $pname = "5 Year Integrated Masters Programme (CFPD + B. VOC. + M. VOC.)";
                                              }

                                              if (($j%10) === 0 ) { 
                                          // print the random numbers
                                    ?>

                                      <div style="height: 20px;font-size: 11px;margin-bottom: 5px;color: #000;"> Date : 15/04/2018 | Attendance Sheet : <?php echo  $pname;?>    Paper(Part A)<span style="float: right;margin-right: 30px;">Test Center <?php echo  $_POST['center_name'];?></span></div>
                                      <?php  } ?>
                                            
                                    
                                     <div style="float: left;width: 48%;border: 1px solid #000;margin-bottom: 2px;">
                                       <h5 style="border-bottom: 1px solid #000;margin: 0;font-size: 11px;padding: 5px 10px;"><span style="display: block;font-size: 10px;margin-bottom: 5px;">Name</span><strong style="text-transform: uppercase;"><?php echo  $value['first_name'].' '.$value['middle_name'].' '.$value['last_name']; ?></strong></h5>
                                       <div style="float: left;width: 50%;">
                                         
                                         
                                          <h5 style="border-bottom: 1px solid #000;border-right: 1px solid #000;margin: 0;font-size: 11px;padding: 5px 10px;"><span style="display: block;    font-size: 10px;margin-bottom: 5px;">Enrollment No.</span><strong><?php echo  $value['enroll_id'];?></strong></h5>
                                          
                                          <h5 style="border-bottom: 1px solid #000;border-right: 1px solid #000;margin: 0;font-size: 11px;padding: 5px 10px;"><span style="display: block;    font-size: 10px;margin-bottom: 5px;">Date of Birth</span><?php echo  date('d/m/Y',strtotime($value['dob']));?></h5>
                                         
                                         <h5 style="border-bottom: 1px solid #000;border-right: 1px solid #000;margin: 0;font-size: 11px;padding: 5px 10px;"><span style="display: block;    font-size: 10px;margin-bottom: 5px;">Mobile No.</span><?php echo  $value['phone'];?></h5>
                                                                        
                                         
                                         <h5 style="border-right: 1px solid #000;margin: 0;font-size: 11px;padding: 5px 10px;"><span style="display: block;    font-size: 9px;margin-bottom: 5px;">Candidate Signature</span><strong> &nbsp;</strong></h5>
                                         
                                       </div>
                                       <div style="float: left;width: 50%;"> <img style="margin: 2px auto;height: 140px; display: table;max-width: 100%;" src="../images/<?php echo  $value['applicant_photo'];?>" alt="" height="150">  </div>
                                     </div>
                                  
                                   <?php 

                                   if (($i%10) === 0 ) { ?>
                                   <p style="font-size: 11px;color: #000;text-align: center;margin: 0;">Indian Institute of Craft & Design, Jaipur</p>

                                   <?php  } ?>

                                        <?php

                                        $i++;
                                        $j++;
                                      } ?>
                                       </div>

                                      <?php
                                          }else{                    
                                        ?>
                                        <table class="table table-striped table-bordered table-vcenter">
                                          <tr>
                                            <td align="center">No Record Found</td>
                                          </tr>
                                        </table>
                                <?php } ?>

