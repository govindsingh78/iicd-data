<link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" media="all" type="text/css" href="css/jquery-ui-timepicker-addon.css" />

<div id="sidebar">
    <!-- Sidebar Brand -->
    <div id="sidebar-brand" class="themed-background">
        <a href="index.php" class="sidebar-title">
            <i class="fa fa-cube"></i> <span class="sidebar-nav-mini-hide"><strong>IICD</strong></span>
            <!--<img src="img/logo-burgerfarm.png" width="100" height="auto">-->
        </a>
    </div>
    <!-- END Sidebar Brand -->

    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Sidebar Navigation -->
            <ul class="sidebar-nav">
                <li>
                    <a href="index.php" class=" active"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
                </li>
<!-- 
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-graduation-cap sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Specialization</span></a>
                    <ul>
                        <li>
                            <a href="specializations_list.php">List Specializations</a>
                        </li>
                      
                    </ul>
                </li> -->

                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-graduation-cap sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Exam Center</span></a>
                    <ul>
                        <li>
                            <a href="examcenter_list.php">List Exam Center</a>
                        </li>
                      
                    </ul>
                </li>

                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-graduation-cap sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Candidates</span></a>
                    <ul>
                        <li>
                            <a href="users_list.php">Candidates List</a>
                        </li>
                      
                    </ul>
                </li>
<li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-graduation-cap sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Settings</span></a>
                    <ul>
                        <li>
                            <a href="settings.php">Settings</a>
                        </li>
                      
                    </ul>
                </li>
</li>
<li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-graduation-cap sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Attendance Sheet</span></a>
                    <ul>
                        <li>
                            <a href="sheet.php">Attendance Sheet</a>
                        </li>
                      
                    </ul>
                </li>
 <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-graduation-cap sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Table Sheet</span></a>
                    <ul>
                        <li>
                            <a href="tablesheet.php">Table Sheet</a>
                        </li>
                      
                    </ul>
                </li>
                
                
               
            </ul>
            <!-- END Sidebar Navigation -->
		</div>
        <!-- END Sidebar Content -->
    </div>
    <!-- END Wrapper for scrolling functionality -->
</div>
<!-- END Main Sidebar -->

<!-- Main Container -->
<div id="main-container">
    <!-- Header -->
    <header class="navbar navbar-inverse navbar-fixed-top">
        <!-- Left Header Navigation -->
        <ul class="nav navbar-nav-custom">
            <!-- Main Sidebar Toggle Button -->
            <li>
                <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');
                        this.blur();">
                    <i class="fa fa-ellipsis-v fa-fw animation-fadeInRight" id="sidebar-toggle-mini"></i>
                    <i class="fa fa-bars fa-fw animation-fadeInRight" id="sidebar-toggle-full"></i>
                </a>
            </li>
            <!-- END Main Sidebar Toggle Button -->

            <!-- Header Link -->
            <li class="hidden-xs animation-fadeInQuick">
                <a href=""><strong>WELCOME  </strong>(<?php echo $_SESSION['adminyncfullname'];?>)</a>
            </li>
            <!-- END Header Link -->
        </ul>
        <!-- END Left Header Navigation -->

        <!-- Right Header Navigation -->
        <ul class="nav navbar-nav-custom pull-right">
            <!-- User Dropdown -->
            <li class="dropdown">
                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="gi gi-settings"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="dropdown-header">
                        <strong>ADMINISTRATOR</strong>
                    </li>
                    <li>
                        <a href="changepassword.php">
                            <i class="gi gi-lock fa-fw pull-right"></i>
                            Change Password
                        </a>
                    </li>
                    <li>
                        <a href="logout.php">
                            <i class="fa fa-power-off fa-fw pull-right"></i>
                            Log out
                        </a>
                    </li>
                </ul>
            </li>
            <!-- END User Dropdown -->
        </ul>
        <!-- END Right Header Navigation -->
    </header>
    <!-- END Header -->