<?php
  session_start();
  include 'meekrodb.2.3.class.php';
  $maxlifetime = ini_get("session.gc_maxlifetime");
  $expireAfter = 60; 

  if(!isset($_SESSION['uemail'])) {
    header('location: admission.php');
  }

  if(isset($_SESSION['last_action'])){
    $mnt = 59;
    
    $secondsInactive = time() - $_SESSION['last_action'];
    $expireAfterSeconds = $expireAfter * 60;

    $remaining_time = $expireAfterSeconds - $secondsInactive;
    $minutes = $remaining_time / 60;
    $minute = explode(".", $minutes);
    $mnt = $minute[0];
    if($mnt >= 60){
      $mnt = 59;
    }

    $sec = substr($minute[1], 0, 2);
    if($sec <= 60){
      $sec = $sec;
    }
    else{
      $sec = $sec - 59;
    }

    if(!$sec){
      $sec = 59;
    }
    
    if($secondsInactive >= $expireAfterSeconds){
        session_unset();
        session_destroy();
        header('location: admission.php');
    }
  }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta id="viewport" name="viewport">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width">
        <title>IICD | Register Form</title>
        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
        <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>
        <style type="text/css">      

          #img_contain1{
            border:1px solid grey;
            margin-top:10px;
            width:270px;
            
          }

          #imgInp1{
            margin-left:1px;
            padding:10px 0px 10px 0px;
            background-color:#f7f1f1;
            
          }
          #blah1{
            height:200px;
                width: 270px;
            display:block;
            margin-left: auto;
            margin-right: auto;
            padding:5px;
          }

          #img_contain2{
                border:1px solid grey;
                margin-top:10px;
                width:270px;
                
              }

              #imgInp2{
                margin-left:1px;
                padding:10px 0px 10px 0px;
                background-color:#f7f1f1;
                
              }
              #blah2{
                height:200px;
                    width: 270px;
                display:block;
                margin-left: auto;
                 margin-right: auto;
                padding:5px;
              }

              #img_contain3{
                border:1px solid grey;
                margin-top:10px;
                width:270px;
                
              }

              #imgInp3{
                margin-left:1px;
                padding:10px 0px 10px 0px;
                background-color:#f7f1f1;
                
              }
              #blah3{
                height:200px;
                    width: 270px;
                display:block;
                margin-left: auto;
                 margin-right: auto;
                padding:5px;
              }


               #img_contain4{
                border:1px solid grey;
                margin-top:10px;
                width:270px;
                
              }

              #imgInp4{
                margin-left:1px;
                padding:10px 0px 10px 0px;
                background-color:#f7f1f1;
                
              }
              #blah4{
                height:200px;
                    width: 270px;
                display:block;
                margin-left: auto;
                 margin-right: auto;
                padding:5px;
              }
        </style>
        <!-- Analytics Code -->
        <script>  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');  ga('create', 'UA-75923092-1', 'auto');  ga('send', 'pageview'); </script>
          <!-- Facebook Pixel Code -->
          <script>
          ! function(f, b, e, v, n, t, s) {
              if (f.fbq) return;
              n = f.fbq = function() {
                  n.callMethod ?
                      n.callMethod.apply(n, arguments) : n.queue.push(arguments)
              };
              if (!f._fbq) f._fbq = n;
              n.push = n;
              n.loaded = !0;
              n.version = '2.0';
              n.queue = [];
              t = b.createElement(e);
              t.async = !0;
              t.src = v;
              s = b.getElementsByTagName(e)[0];
              s.parentNode.insertBefore(t, s)
          }(window, document, 'script',
              'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '1151196271591907');
          fbq('track', 'PageView');
          </script>
          <noscript>
              <img height="1" width="1" src="https://www.facebook.com/tr?id=1151196271591907&ev=PageView
        &noscript=1" />
          </noscript>
          <!-- End Facebook Pixel Code -->

</head>
    <body>
      <div class="admission-form">
         <div class="btn-std">
                             <a href="logout.php">
                            <i class="fa fa-power-off fa-fw pull-right"></i>
                            Log out
                        </a>
       </div>
      <div class="logout_time">
        <P>Your session will logout in <span id="timer"><?php echo $mnt; ?>:<?php echo $sec; ?></span></P>
      </div>
          <div class="mrg-btm">
                <div class="col-md-12 text-center">
                 <div class="company-logo">
                        <a href="#">
                            <img src="images/company-logo1.png" alt=""> 
                        </a>
                    </div>
                     <h2> Registration For Entrance Test 2018-19 </h2>
                      <div class="form-sub-title">
                          <span> Admission 2018-19 </span><br>
                          <span> Registration will start from  January 14 (Thursday). </span><br>
                          <span> Last Date of Submission 4th April 2018 (Wednesday) , 16:00 hr </span>
                      </div>
                   
                </div>
          </div>
          <div class="col-sm-8 col-sm-offset-2 flash-message"></div>
          <main>
              <fieldset>
                <legend> Step 1 : Select Graduate Programme </legend>
                  <div id="graduate_container" class="form-section"></div>
              </fieldset>

              <fieldset>
                <legend>Step 2 : Personal Details </legend>
                <div id="personal_container" class="form-section"></div>

              </fieldset>

              <fieldset>
                <legend> Step 3 : Address </legend>
                <div id="address_container" class="form-section"></div>
              </fieldset>

              <fieldset>
                <legend>Step 4 : Exam Center</legend>

                <div id="exam_container" class="form-section"></div>
              </fieldset>

              <fieldset>
                <legend> Step 5 : 10+2 / HS </legend>
                <div id="hs_container" class="form-section"></div>
              </fieldset>

              <fieldset id="degree_section">
                <legend> Step <span id="lbl_deg">6</span> : Degree
                </legend>
                <div id="degree_container" class="form-section"></div>
              </fieldset>

              <fieldset>
                <legend> Step <span id="lbl_lng">7</span> : Language Known</legend>
                <div id="language_container" class="form-section"></div>
              </fieldset>
                 
              <fieldset>
                <legend> Step <span id="lbl_spec">8</span> : Preferences For Specialization</legend>
                <div id="specialization_container" class="form-section"></div>
              </fieldset>

              <fieldset>
                <legend> Step <span id="lbl_attach">9</span> : Attachments</legend>
                <div id="attachment_container" class="form-section"></div>
              </fieldset>
                
                <div id="declaration_container"></div>
          </main>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <script src="js/formalize.js" type="text/javascript"></script>
      
       <script type="text/javascript" src="js/jquery.validate.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
  $('#graduate_container').load('form_graduate.php',function(e){
  });

  $.validator.addMethod("customvalidation",
           function(value, element) {
                   return /^[a-zA-Z ]+$/.test(value);
           },
        "Sorry, no special characters and number allowed"
   );

  jQuery.validator.addMethod("phoneno", function(phone_number, element) {
      phone_number = phone_number.replace(/\s+/g, "");
      return this.optional(element) || phone_number.length == 10 && 
      phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
  }, 'Enter valid phone number');

  jQuery.validator.addMethod("validate_pincode",function(value, element) {
    
    if(value !=''){
    if(/(^\d{6}$)/.test( value )){
        return true;
      }else{
        return false;
      }
    }else{
      return true;
    }    
  },"Enter a valid pincode.");

});
</script>



      <script type="text/javascript">
        $(document).ready(function(){
          $('.flash-message').html('<div class="alert alert-success" id="flash-msg">Login Successfully</div>');
          $("#flash-msg").delay(5000).fadeOut("slow");

          document.getElementById('timer').innerHTML = <?php echo $mnt; ?> + ":" + <?php echo $sec; ?>;
          startTimer();

          function startTimer() {
            var presentTime = document.getElementById('timer').innerHTML;
            var timeArray = presentTime.split(/[:]+/);
            var m = timeArray[0];
            var s = checkSecond((timeArray[1] - 1));
            if(s == 59){
              m = m - 1;
            }
            if(m < 0){
              window.location.href = 'https://www.iicd.ac.in/admission.php';
            }
            
            document.getElementById('timer').innerHTML = m + ":" + s;
            setTimeout(startTimer, 1000);
          }

          function checkSecond(sec) {
            if (sec < 10 && sec >= 0) {
            sec = "0" + sec
            }; // add zero in front of numbers < 10
            
            if (sec < 0) {
              sec = "59"
            };
            return sec;
          }

          $('#sectional').formalize({
            timing: 300,
            nextCallBack: function(){
              if (validateEmpty($('#sectional .open'))){
                scrollToNewSection($('#sectional .open'));
                return true;
              };
              return false;
            },
            prevCallBack: function(){
              return scrollToNewSection($('#sectional .open').prev())
            }
          });
          
          $('input').on('keyup change', function(){
            $(this).closest($('.valid')).removeClass('valid');
          });

          function validateEmpty(section){
            var errors = 0;
            section.find($('.required-field')).each(function(){
              var $this = $(this),
                input = $this.find($('input'));
              if (input.val() === ""){
                errors++;
                $this.addClass('field-error');
                $this.append('\<div class="form-error-msg">This field is required!\</div>');
              }
            });
            if (errors > 0){
              section.removeClass('valid');
              return false;
            }
            section.find($('.field-error')).each(function(){
              $(this).removeClass('field-error');
            });
            section.find($('.form-error-msg')).each(function(){
              $(this).remove();
            });
            section.addClass('valid');
            return true;
          }

          function scrollToNewSection(section){
            var top = section.offset().top;
            $("html, body").animate({
              scrollTop: top
            }, '200');
            return true;
          }
        });
      </script>

      <script language="javascript" type="text/javascript">
        $(document).ready(function() {
          $('#country').change(function() {
            $("option:selected", $(this)).each(function () {
              var countryid = $(this).val();
              $.ajax({
                  type: "POST",
                  url: "showstate.php",
                  data: "countryid=" + countryid,
                  success: function (data) {
                          $('#divcountry').html(data);
                          $('#divcountry').slideDown('slow');
                          }
              });
          });
         });
      });
    </script> 

    <script language="javascript" type="text/javascript">
          $(document).on('change', '#state', function() {

        //  $('#state').change(function() {
            $("option:selected", $(this)).each(function () {
              var stateid = $(this).val();
              $.ajax({
                  type: "POST",
                  url: "showcity.php",
                  data: "stateid=" + stateid,
                  success: function (data) {
                          $('#divstate').html(data);
                          $('#divstate').slideDown('slow');
                          }
              });
          });
         });

    </script>
     
<script>
function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah1').attr('src', e.target.result);

      $('#blah1').hide();
      $('#blah1').fadeIn(650);

    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp1").change(function() {
  readURL(this);
});
</script>
<script>
function readURL2(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah2').attr('src', e.target.result);

      $('#blah2').hide();
      $('#blah2').fadeIn(650);

    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp2").change(function() {
  readURL2(this);
});
</script>
<script>
function readURL3(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah3').attr('src', e.target.result);

      $('#blah3').hide();
      $('#blah3').fadeIn(650);

    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp3").change(function() {
  readURL3(this);
});
</script>

    </body>
</html>