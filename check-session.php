<?php
  session_start();
  include 'meekrodb.2.3.class.php';
  $maxlifetime = ini_get("session.gc_maxlifetime");

  $_SESSION['last_action'] = time();

  if($_SESSION['adminyncvalid'])
    unset($_SESSION['adminyncvalid']);

  if($_SESSION['adminyncuserid'])
    unset($_SESSION['adminyncuserid']);

  if($_SESSION['adminyncfullname'])
    unset($_SESSION['adminyncfullname']);
  
  if($_SESSION['adminyncusername'])
    unset($_SESSION['adminyncusername']);
  
  if($_SESSION['adminyncrights'])
    unset($_SESSION['adminyncrights']);

  if($_SESSION['adminynclastlogindate'])
    unset($_SESSION['adminynclastlogindate']);
  
  if($_SESSION['adminynclastlogintime'])
    unset($_SESSION['adminynclastlogintime']);
  
  if($_SESSION['adminynclastloginip'])
    unset($_SESSION['adminynclastloginip']);

  if(isset($_SESSION['uemail'])) {
    header('location: admission-form.php');
  }
  else {
    header('location: admission.php');
  }
?>