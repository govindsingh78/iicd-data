<?php 
session_start();
include 'meekrodb.2.3.class.php';

//echo '<pre>'; print_r($_POST);

unset($_REQUEST['PHPSESSID']);
unset($_POST['sameaddress']);
unset($_REQUEST['wordpress_test_cookie']);
unset($_REQUEST['wp-settings-1']);
unset($_REQUEST['wp-settings-time-1']);
unset($_REQUEST['wordpress_logged_in_26a07b34f79af9f0ef6764526d88de80']);
unset($_REQUEST['_ga']);
unset($_REQUEST['_gid']);

//echo '<pre>'; print_r($_REQUEST); die;

if(isset($_POST['action']) && $_POST['action'] =='save_graduate'){

	$query = "select * from user_details where user_id = '".$_SESSION['user_id']."'";
	$row = DB::queryFirstRow($query);
	
	if(!empty($row)){
		$update_data['Programme'] = $_POST['Programme'];
	    $update = DB::update('user_details', $update_data, "user_id=%s", $_SESSION['user_id']);

	    $result['status'] = '1';
	    $result['msg'] = 'Upadated successfully';
	    echo json_encode($result); die;
 
	}else{
		$insert_data['user_id'] = $_SESSION['user_id'];
		$insert_data['Programme'] = $_POST['Programme'];

		$insert = DB::insert('user_details', $insert_data);

		$result['status'] = '1';
	    $result['msg'] = 'Upadated successfully';
	    echo json_encode($result); die;
	}

}else if(isset($_POST['action']) && $_POST['action'] =='save_personal'){

	unset($_POST['action']);
	$_POST['dob'] = date('Y-m-d', strtotime($_POST['dob']));
	$update = DB::update('user_details', $_POST, "user_id=%s", $_SESSION['user_id']);

    $result['status'] = '1';
    $result['msg'] = 'Upadated successfully';
    echo json_encode($result); die;

}else if(isset($_POST['action']) && $_POST['action'] =='save_address'){

	unset($_POST['action']);
	$update = DB::update('user_details', $_POST, "user_id=%s", $_SESSION['user_id']);

    $result['status'] = '1';
    $result['msg'] = 'Upadated successfully';
    echo json_encode($result); die;

}else if(isset($_POST['action']) && $_POST['action'] =='save_exam'){

	unset($_POST['action']);
	$update = DB::update('user_details', $_POST, "user_id=%s", $_SESSION['user_id']);

    $result['status'] = '1';
    $result['msg'] = 'Upadated successfully';
    echo json_encode($result); die;

}else if(isset($_POST['action']) && $_POST['action'] =='save_hs'){

	unset($_POST['action']);

	$update = DB::update('user_details', $_POST, "user_id=%s", $_SESSION['user_id']);

    $result['status'] = '1';
    $result['msg'] = 'Upadated successfully';
    echo json_encode($result); die;

}else if(isset($_POST['action']) && $_POST['action'] =='save_deg'){

	unset($_POST['action']);

	$update = DB::update('user_details', $_POST, "user_id=%s", $_SESSION['user_id']);

    $result['status'] = '1';
    $result['msg'] = 'Upadated successfully';
    echo json_encode($result); die;

}else if(isset($_POST['action']) && $_POST['action'] =='save_lang'){

	unset($_POST['action']);
	unset($_POST['chk_hindi']);
	unset($_POST['chk_eng']);

	if(isset($_POST['language_hindi'])){
		$_POST['language_hindi']  = implode(',', $_POST['language_hindi']);
	}

	if(isset($_POST['language_english'])){
		$_POST['language_english']  = implode(',', $_POST['language_english']);
	}

	$update = DB::update('user_details', $_POST, "user_id=%s", $_SESSION['user_id']);

    $result['status'] = '1';
    $result['msg'] = 'Upadated successfully';
    echo json_encode($result); die;

}else if(isset($_POST['action']) && $_POST['action'] =='save_spec'){

	unset($_POST['action']);
	$update = DB::update('user_details', $_POST, "user_id=%s", $_SESSION['user_id']);

    $result['status'] = '1';
    $result['msg'] = 'Upadated successfully';
    echo json_encode($result); die;

}else if(isset($_REQUEST['action']) && $_REQUEST['action'] =='save_attach'){

	if($_FILES["applicant_photo"]["error"]==0) {	
		$temp_name=$_FILES["applicant_photo"]["tmp_name"];
		$imgtype=$_FILES["applicant_photo"]["type"];
		$ext= GetImageExtension($imgtype);
		$file_name='applicant_photo/'.time().'_'.rand(0, 99).$ext;
		$target_path = "images/".$file_name;			
		move_uploaded_file($temp_name, $target_path);
	        $_REQUEST['applicant_photo']	 = $file_name;
	}else{
		$_REQUEST['applicant_photo']	 = $_REQUEST['hidden_photo'];
	}

	if ($_FILES["dob_certificate"]["error"]==0) {	
		$temp_name=$_FILES["dob_certificate"]["tmp_name"];
		$imgtype=$_FILES["dob_certificate"]["type"];
		$ext= GetImageExtension($imgtype);
		$file_name='dob_certificate/'.time().'_'.rand(0, 99).$ext;
		$target_path =  "images/".$file_name;			
		move_uploaded_file($temp_name, $target_path);
	    $_REQUEST['dob_certificate']	 = $file_name;
	}else{
		$_REQUEST['dob_certificate']	 = $_REQUEST['hidden_dob'];
	}

	if ($_FILES["id_proof"]["error"]==0) {	
		$temp_name=$_FILES["id_proof"]["tmp_name"];
		$imgtype=$_FILES["id_proof"]["type"];
		$ext= GetImageExtension($imgtype);
		$file_name='id_proof/'.time().'_'.rand(0, 99).$ext;
		$target_path =  "images/".$file_name;			
		move_uploaded_file($temp_name, $target_path);
	    $_REQUEST['id_proof']	 = $file_name;
	}else{
		$_REQUEST['id_proof']	 = $_REQUEST['hidden_id'];
	}

	if ($_FILES["signatures"]["error"]==0) {	
		$temp_name=$_FILES["signatures"]["tmp_name"];
		$imgtype=$_FILES["signatures"]["type"];
		$ext= GetImageExtension($imgtype);
		$file_name='signatures/'.time().'_'.rand(0, 99).$ext;
		$target_path =  "images/".$file_name;			
		move_uploaded_file($temp_name, $target_path);
	    $_REQUEST['signatures']	 = $file_name;
	}else{
		$_REQUEST['signatures']	 = $_REQUEST['hidden_sig'];
	}
          $dataArray = array('applicant_photo'=>$_REQUEST['applicant_photo'],'dob_certificate'=>$_REQUEST['dob_certificate'],'id_proof'=>$_REQUEST['id_proof'],'signatures'=>$_REQUEST['signatures']);
//echo "<pre>";
//print_r($_REQUEST);
//print_r($dataArray); die;
	unset($_REQUEST['action']);
	unset($_REQUEST['file_extension_allow']);
	unset($_REQUEST['hidden_photo']);
	unset($_REQUEST['hidden_id']);
	unset($_REQUEST['hidden_dob']);
	unset($_REQUEST['hidden_sig']);

	$update = DB::update('user_details', $dataArray, "user_id=%s", $_SESSION['user_id']);

    $result['status'] = '1';
    $result['msg'] = 'Upadated successfully';
    echo json_encode($result); die;

}

function GetImageExtension($imagetype){
   if(empty($imagetype)) return false;
   switch($imagetype){
	   case 'image/bmp': return '.bmp';
	   case 'image/gif': return '.gif';
	   case 'image/jpeg': return '.jpg';
	   case 'image/png': return '.png';
	   default: return false;
   }
 }




?>