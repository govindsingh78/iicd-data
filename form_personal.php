<?php 
session_start();
include 'meekrodb.2.3.class.php';


$query = "select * from user_details where user_id = '".$_SESSION['user_id']."'";
$queryu = "select * from users where id = '".$_SESSION['user_id']."'";

$row = DB::queryFirstRow($query);
$rowuser = DB::queryFirstRow($queryu);

$dob = '';

$male_selected = 'selected="selected"';
$female_selected = '';

if($row['dob'] !='' && $row['dob'] !='0000-00-00'){
    $dob = date('m/d/Y', strtotime($row['dob']));    
}

if($row['gender'] !='' && trim($row['gender'])=='female'){
    $male_selected = '';
    $female_selected = 'selected="selected"';
}

$m_status = array('Single'=>'Single', 'Married'=>'Married', 'Separated'=>'Separated', 'Widowed'=>'Widowed', 'Divorced'=>'Divorced');

$category = array('General'=>'General', 'OBC'=>'OBC', 'SC'=>'SC', 'ST'=>'ST','PH'=>'PH');

$countries = DB::query("select * from country");


$domicile = array('yes'=>'Yes', 'no'=>'No');

$income = array('Upto Rs.5,00,000'=>'Upto Rs.5,00,000', 'Rs.5,00,001 to Rs.10,00,000'=>'Rs.5,00,001 to Rs.10,00,000', 'Above Rs.10,00,000'=>'Above Rs.10,00,000');

?>

<link rel="stylesheet" href="css/bootstrap-datepicker3.standalone.min.css">
<script src="js/bootstrap-datepicker.min.js"></script>

<form id="form_personal" name="form_personal">
  <div class="my-dtl-feed">
<div class="col-md-12">
    <div class="group">  
        <div class="col-md-4">  
          <div class="my-input-bx field required-field" >    
              <input type="text" id="dob" name="dob" class="form-control" value="<?=$dob?>" required title="Date of Birth">
              <span class="bar"></span>
              <label>Date of Birth</label>
          </div>
        </div>

        <div class="col-md-4">   
          <div class="my-input-bx field required-field" >   
                <div class="selectContainer" >  
                    <label class="my-label">Gender</label>
                     <span class="bar"></span>
                    <select id="gender" name="gender" class="form-control">
                      <option value="male" <?=$male_selected?> >Male</option>
                      <option value="female" <?=$female_selected?>>Female</option>
                    </select>
                 </div>
            </div>
        </div>
                                           
        <div class="col-md-4 ">  
              <div class="my-input-bx field required-field">    
                <div class="selectContainer"> 
                    <label class="my-label">Marital status</label>
                     <span class="bar"></span>
                      <select name="marital_status" class="form-control">
                      <?php
                      foreach ($m_status as $key => $val) {
                          $selected = '';
                          if($key==$row['marital_status']){
                              $selected = 'selected="selected"';
                          }
                          echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
                      }
                      ?>
                      </select>
                 </div>
              </div>
        </div>
    </div>  

    <div class="group"> 
        <div class="col-md-4 my-select"> 
          <div class="my-input-bx field required-field">     
            <div class="selectContainer"> 
                <label class="my-label">Category <span class="label-subtxt"> (If belonging to OBC,SC,ST or PH, please submit the support <br>   document at the time of admission)</span></label>
                 <span class="bar"></span>
                <select id="category" name="category" class="form-control">
                  <?php
                      foreach ($category as $key => $val) {
                          $selected = '';
                          if($key==$row['category']){
                              $selected = 'selected="selected"';
                          }
                          echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
                      }
                      ?> 
                </select>
             </div>
          </div>
        </div>
                                               
        <div class="col-md-4">  
          <div class="my-input-bx field required-field">    
              <div class="selectContainer"> 
                  <label class="my-label">Nationality</label>
                   <span class="bar"></span>
                  <select id="nationality" name="nationality" class="form-control">
                    <option value="">Select Nationality</option>  
                    <?php
                    $india = false;
                      foreach ($countries as $val) {
                          $selected = '';
                          if($val['country_name']==$row['nationality']){
                              if($row['nationality']=='India'){
                                $india = true;
                              }
                              $selected = 'selected="selected"';
                          }
                          echo '<option value="'.$val['country_name'].'" '.$selected.'>'.$val['country_name'].'</option>';
                      }
                      ?>
                  </select>
               </div>
          </div>
        </div>
                                                
        <div class="col-md-4"> 
          <div class="my-input-bx field required-field">     
            <div class="selectContainer"> 
                <label class="my-label">Domicile Residence of Rajasthan State<br>  <span class="label-subtxt">(If yes, please submit the support document / certificate at the time admission)</span></label>
                 <span class="bar"></span>
                <select id="domicile" name="domicile" class="form-control" required>
                <option value="">Select</option>  
                    <?php
                      foreach ($domicile as $key => $val) {
                          $selected = '';
                          if($key==$row['domicile']){
                              $selected = 'selected="selected"';
                          }
                          echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
                      }
                      ?> 
                </select>
             </div>
          </div>
        </div>
    </div>

    <div class="group">
        <div class="col-md-4">  
           <div class="my-input-bx field required-field">    
              <label class="my-label"> Candidate Name</label>
              <span class="bar"></span>
              <input type="text" id="name" title="Candidate name" class="form-control" disabled="disabled" value="<?php echo $rowuser['first_name'].' '.$rowuser['middle_name'].' '.$rowuser['last_name']?>">
           </div>
        </div>

        <div class="col-md-4">  
           <div class="my-input-bx field required-field">    
              <input type="text" id="phone" name="phone" value="<?=$row['phone']?>" class="form-control" required>
              <span class="bar"></span>
              <label>Mobile Number (Candidate)</label>
           </div>
        </div>
                                            
        <div class="col-md-4">  
           <div class="my-input-bx field required-field"> 
           <label class="my-label"> Candidate Email</label>
              <span class="bar"></span>   
              <input type="text" id="email" name="email" class="form-control" disabled="disabled" value="<?php echo $_SESSION['uemail']?>">
           </div>
        </div>
    </div>

    <div class="group"> 
      <div class="col-md-4">  
         <div class="my-input-bx field required-field">    
            <input type="text" id="fathers_name" name="fathers_name" value="<?=$row['fathers_name']?>" class="form-control" required>
            <span class="bar"></span>
            <label>Father's Name</label>
         </div>
      </div> 

      <div class="col-md-4">  
         <div class="my-input-bx field required-field">    
            <input type="text" id="phone_father" name="phone_father" value="<?=$row['phone_father']?>" class="form-control" required>
            <span class="bar"></span>
            <label>Father`s Mobile Number</label>
         </div>
      </div>

      <div class="col-md-4">  
         <div class="my-input-bx field required-field">    
            <input type="text" id="email_father" name="email_father" value="<?=$row['email_father']?>" class="form-control" required>
            <span class="bar"></span>
            <label>E-mail Id (Father)</label>
         </div>
      </div>
 
    </div>
    <div class="group">    


     <div class="col-md-4">  
         <div class="my-input-bx field required-field">    
            <input type="text" id="mothers_name" name="mothers_name" value="<?=$row['mothers_name']?>" class="form-control" required>
            <span class="bar"></span>
            <label>Mother's Name</label>
         </div>
      </div>

      <div class="col-md-4">  
         <div class="my-input-bx">   
         <label class="my-label">Mother`s Mobile Number</label>
                  <span class="bar"></span>    
            <input type="text" id="phone_mother" name="phone_mother" value="<?=$row['phone_mother']?>" class="form-control"  >
            <span class="bar"></span>
            
         </div>
      </div>

      <div class="col-md-4">  
         <div class="my-input-bx">    
            <input type="text" id="email_mother" name="email_mother" value="<?=$row['email_mother']?>" class="form-control">
            <span class="bar"></span>
            <label>E-mail Id (Mother)</label>
         </div>
      </div>

    </div>
 <div class="group">    

     <div class="col-md-4">  
         <div class="my-input-bx field">    
   
            <input type="text" id="guardians_name" name="guardians_name" value="<?=$row['guardians_name']?>" class="form-control">
           <span class="bar"></span>
            <label>Local Guardians's Name</label>
         </div>
      </div>

      <div class="col-md-4">  
         <div class="my-input-bx">   
         <label class="my-label">Local Guardians`s Mobile Number</label>
                  <span class="bar"></span>    
            <input type="text" id="phone_guardian" name="phone_guardian" value="<?=$row['phone_guardian']?>" class="form-control"  >
            <span class="bar"></span>
            
         </div>
      </div>

      <div class="col-md-4">  
         <div class="my-input-bx">    
          <label class="my-label"> Relation with Local Guardians</label>
          <span class="bar"></span> 
            <input type="text" id="guardians_relation" name="guardians_relation" value="<?=$row['guardians_relation']?>" class="form-control">
            <span class="bar"></span>
         </div>
      </div>
    </div>

<br/>
    <div class="group"> 

        <div class="col-md-4">  
           <div class="my-input-bx ">    
            <label class="my-label">Local Guardian E-mail</label>
            <span class="bar"></span>
             <input type="text" id="email_guardian" name="email_guardian" value="<?=$row['email_guardian']?>" class="form-control">
           </div>
        </div>        

        <div class="col-md-4"> 
          <div class="my-input-bx field required-field">     
            <div class="selectContainer"> 
                  <label class="my-label">Family Income Per Annum (Rs.)<br>                              
                <span class="label-subtxt">(Please submit the support document/certificate at the time admission)</span></label><span class="bar"></span>
                <select id="family_income" name="family_income" class="form-control">
                <option value="">Select Income</option> 
                    <?php
                      foreach ($income as $key => $val) {
                          $selected = '';
                          if($key==$row['family_income']){
                              $selected = 'selected="selected"';
                          }
                          echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
                      }
                      ?>
                </select>
             </div>
          </div>
        </div>

        <div class="col-md-4"> 
         <div class="my-input-bx field required-field">     
          <div class="selectContainer"> 
              <label class="my-label">  Do you belong to a traditional or practicing craft family* <br>  <span class="label-subtxt">(If yes, please submit the support document / certificate at the time admission)</span></label><span class="bar"></span>
              <select id="craft_relation" name="craft_relation" class="form-control"> 
                <option value="">Select</option> 
                  <?php
                    foreach ($domicile as $key => $val) {
                        $selected = '';
                        if($key==$row['craft_relation']){
                            $selected = 'selected="selected"';
                        }
                        echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
                    }
                    ?> 
              </select>
           </div>
          </div>
        </div>

    </div>

     <div class="group" id="craftname"> 
       <div class="col-md-4">  
         <div class="my-input-bx field required-field">    
            <input type="text" id="craft_name" name="craft_name" value="<?=$row['craft_name']?>" class="form-control">
            <span class="bar"></span>
            <label>Craft Name</label>
         </div>
      </div>
     </div>


    <div class="group">  
      <div class="col-md-12">  
        <div class="my-input-bx field required-field">  
          <label class="my-label"> Medical / Health Information
                  <br> 
                  <span class="label-subtxt"> (Please mention any chronic health issue which needs attention) </span> 
          </label>
          <textarea class="form-control" id="medical_info" name="medical_info" cols="12" col-md-12s="15"><?=$row['medical_info']?></textarea>
        </div>
      </div>


      <br>
      <nav class="form-section-nav">
        <input type="hidden" name="action" id="action" value="save_personal">
        <span id="btn_back_personal" class="btn-secondary form-nav-prev"> <img src="images/left-arrow.jpg" alt="left"> Prev </span>
        <span id="btn_next_personal" class="btn-std form-nav-next"> Save & Next <img src="images/right-arrow.jpg" alt="left"></span>
      </nav>
    </div>
</div>
</div>
</form>  

<script type="text/javascript">
$(document).ready(function(){
    $('#dob').datepicker({
      orientation: 'left bottom',
      autoclose: true,
      todayHighlight: true
    });

    $("#btn_back_personal").unbind().click(function() {
        $('#graduate_container').load('form_graduate.php',function(e){
          $("#personal_container" ).slideUp( "slow");
          $('#personal_container').html('');
          $("#graduate_container" ).slideDown( "slow");
        });
    });
     <?php 
    if($india){ ?>
      $("#domicile").prop('disabled',false);
    <?php  }else{ ?>
      $("#domicile").prop('disabled',true);
    <?php  } ?>
   // $("#domicile").val('no');
    $("#nationality").change(function () {
       var val = $(this).val();
       if (val != "India") {
         $("#domicile").prop('disabled',true);
         $("#domicile").val('no');
        }else{
          $("#domicile").prop('disabled',false);
          $("#domicile").val('yes');
        }
      });
    $('#craftname').hide(); 
     $("#craft_relation").change(function () {
       var val = $(this).val();
       if (val != "yes") {
         $('#craftname').hide(); 
        }else{
          $('#craftname').show(); 
        }
      });

     $("#phone_guardian").prop('disabled',true);
     $("#guardians_relation").prop('disabled',true);
     $("#email_guardian").prop('disabled',true);
     $("#guardians_name").blur(function () {
      var val = $(this).val();
      if (val !='') {
         $("#phone_guardian").prop('disabled',false);
         $("#guardians_relation").prop('disabled',false);
         $("#email_guardian").prop('disabled',false);  
      }else{
       $("#phone_guardian").prop('disabled',true);
       $("#guardians_relation").prop('disabled',true);
       $("#email_guardian").prop('disabled',true); 

      }
              
      });


    $("#btn_next_personal").unbind().click(function() { 
        
        if(!$('#form_personal').valid()){
          return false;
        }
      //  var formData = new FormData($('form#form_personal')[0]);
var formData = $('form#form_personal').serialize();
        $.ajax({
            type: "POST",
            url:"admission-save.php",
            data:  formData,
            dataType: "json",
            cache: false,
            success: function(response) {
                if(response.status == 1){
                  $('#address_container').load('form_address.php',function(e){
                    $("#personal_container" ).slideUp( "slow");
                    $('#personal_container').html('');
                    $("#address_container" ).slideDown( "slow");
                  });
                }
            }
        });

    });

    $('#form_personal').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules: 
        {
          "dob": {
            required: true             
          },
          "gender": {
            required: true             
          },
          "marital_status": {
            required: true             
          },
          "category": {
            required: true             
          },
          "nationality": {
            required: true             
          },
          "domicile": {
            required: function(element){
            return $("#nationality option:selected").val() == "India";
            }           
          },
          "phone": {
            required: true,
            phoneno:true             
          },
          "email": {
            required: true,
            email :true             
          },
          "fathers_name": {
            required: true,
            customvalidation: true
          },
          "mothers_name": {
            required: true,
            customvalidation: true
          },
          "phone_parents": {
            required: true,
            phoneno:true
          },
          "email_parents": {
            required: true,
            email :true             
          },
         
          
          "craft_relation": {
             required: true
          },
          "family_income": {
            required: true
          },
          "craft_name": {
            required: function(element){
            return $("#craft_relation option:selected").val() == "yes";
            } 
          },
          "medical_info": {
            required: true
          }
        },
        messages: 
        {
         "dob": {
            required: "DOB is required"
          },
          "gender": {
            required: "Gender is required"
          },
          "marital_status": {
            required: "Marital Status is required"
          },
          "category": {
            required: "Category is required"
          },
           "nationality": {
            required: "Nationality is required"
          },
          "domicile": {
            required: "Domicile is required"
          },
          "phone": {
            required: "Phone is required",
          },
          "email": {
            required: "Email is required",
            email: "Please enter a valid email address"
          },
          "fathers_name": {
            required: "Father`s name is required",
            customvalidation: "Sorry, no special characters and number allowed"
          },
          "mothers_name": {
            required: "Mother`s name is required",
            customvalidation: "Sorry, no special characters and number allowed"
          },
          "phone_parents": {
            required: "Parents phone is required",
          },
          "email_parents": {
            required: "Parents email is required",
            email: "Please enter a valid email address"
          },
          "craft_relation": {
            required: "Relation is required"
          },
          "family_income": {
            required: "Family income is required"
          },
          "craft_name": {
            required: "Craft name is required"
          },
          "medical_info": {
            required: "Medical information is required"
          }
        }
    });


});  
</script>