jQuery(document).ready(function(){


	/************************************
	*********OWL CAROUSEL BUILDER********
	************************************/

	jQuery('#owl-carousel1').owlCarousel({
		loop:true,
		margin:10,
		items: 4,
		singleItem: true,
		autoPlay:false,
		autoPlay: 8000,
		autoPlayHoverPause:true,
		lazyLoad: true,
		navigation:true,
		navigationText: [
			'<i class="fa fa-chevron-left"></i>',
			'<i class="fa fa-chevron-right"></i>'
		]
	})

	jQuery('#owl-carousel2').owlCarousel({
		loop:true,
		margin:10,
		items: 3,
		singleItem: true,
		autoPlay:true,
		autoPlay:8000,
		autoPlayHoverPause:true,
		lazyLoad: true,
		navigation:true,
		navigationText: [
			'<i class="fa fa-chevron-left"></i>',
			'<i class="fa fa-chevron-right"></i>'
		]
	})



	jQuery(".ug_courses_box .panel-default a").click(function(){
		if(jQuery("i.fa-minus" ,this).length > 0){
			jQuery("i" ,this).removeClass('fa-minus')
			jQuery("i" ,this).addClass('fa-plus')
		}
		else{
			jQuery(".ug_courses_box .panel-default a i.fa-minus").addClass('fa-plus').removeClass('fa-minus')
			jQuery("i" ,this).addClass('fa-minus')
		}
	});

	jQuery(".pg_courses_box .panel-default a").click(function(){
		if(jQuery("i.fa-minus" ,this).length > 0){
			jQuery("i" ,this).removeClass('fa-minus')
			jQuery("i" ,this).addClass('fa-plus')
		}
		else{
			jQuery(".pg_courses_box .panel-default a i.fa-minus").addClass('fa-plus').removeClass('fa-minus')
			jQuery("i" ,this).addClass('fa-minus')
		}
	});
})
$('#onspotreg').click(function(){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    return false;
});
// window.onbeforeunload = function (e) {
// 	var message = 'Are you sure you want to leave, cause there are some unsaved changes?';
// 	e = e || window.event;
// 	e.returnValue = message;
// }