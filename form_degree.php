<?php 
  session_start();
  include 'meekrodb.2.3.class.php';

  $query = "select * from user_details where user_id = '".$_SESSION['user_id']."'";
  $row = DB::queryFirstRow($query);

  $streams = array('Science'=>'Science', 'Arts'=>'Arts', 'Commerce'=>'Commerce', 'Engineering'=>'Engineering', 'Other'=>'Other');
?>
<form id="form_degree" name="form_degree">
  <div class="my-dtl-feed">
<div class="col-md-12">
<div class="group"> 
      <div class="col-md-6">  
       <div class="my-input-bx field required-field">    
          <input type="text" id="degree_pass_year" name="degree_pass_year" value="<?=$row['degree_pass_year']?>" class="form-control">
          <span class="bar"></span>
          <label>Year of Passing</label>
       </div>
      </div>
     
      <div class="col-md-6">
        <div class="my-input-bx field required-field">    
            <div class="selectContainer"> 
                <label class="my-label"> 
                Stream of studies at bacholers degree
                </label>
                 <span class="bar"></span>
                <select id="degree_stream" name="degree_stream" class="form-control">
                  <option value="">Select Stream</option>
                  <?php
                  foreach ($streams as $key => $val) {
                      $selected = '';
                      if($key==$row['twelft_stream']){
                          $selected = 'selected="selected"';
                      }
                      echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
                  }
                  ?>
                </select>
             </div>
         </div>
      </div>
      </div>
   <div class="group"> 
     <div class="col-md-4" id="incase">  
       <div class="my-input-bx">    
          <input type="text" id="degree_other_stream" name="degree_other_stream" class="form-control" value="<?=$row['degree_other_stream']?>">
          <span class="bar"></span>
          <label> Incase of other stream pl. specify </label>
       </div>
    </div>                                
    <div class="col-md-4">  
       <div class="my-input-bx field required-field">    
          <input type="text" id="degree_col_univ" name="degree_col_univ" class="form-control"  value="<?=$row['degree_col_univ']?>">
          <span class="bar"></span>
          <label>Name of the university  </label>
       </div>
    </div>

    <div class="col-md-4">  
       <div class="my-input-bx field required-field">    
          <input type="text" id="degree_col_name" name="degree_col_name" class="form-control"  value="<?=$row['degree_col_name']?>">
          <span class="bar"></span>
          <label>Name of the college </label>
       </div>
    </div>        
    
  </div>

  <div class="group"> 
    <div class="col-md-4">  
       <div class="my-input-bx field required-field">    
          <input type="text" id="degree_col_address" name="degree_col_address" class="form-control"  value="<?=$row['degree_col_address']?>">
          <span class="bar"></span>
          <label>College Address </label>
       </div>
    </div>


    <div class="col-md-4">  
       <div class="my-input-bx field required-field">    
          <input type="text" id="degree_grade" name="degree_grade" class="form-control"  value="<?=$row['degree_grade']?>">
          <span class="bar"></span>
          <label>Grade / Class / Divisions / Appeared </label>
       </div>
    </div>

  </div>

    <nav class="form-section-nav">
      <input type="hidden" name="action" id="action" value="save_deg">
      <span id="btn_back_deg" class="btn-secondary form-nav-prev"><img src="images/left-arrow.jpg" alt="left"> Prev</span>
      <span id="btn_next_deg" class="btn-std form-nav-next"> Save & Next <img src="images/right-arrow.jpg" alt="left"></span>
    </nav>
  </div>
  </div>
</form>  

<script type="text/javascript">
$(document).ready(function(){

    $("#btn_back_deg").unbind().click(function() {
      $('#hs_container').load('form_hs.php',function(e){
        $("#degree_container" ).slideUp( "slow");
        $('#degree_container').html('');
        $("#hs_container" ).slideDown( "slow");
      });
    });

    $("#btn_next_deg").unbind().click(function() { 
        
        if(!$('#form_degree').valid()){
          return false;
        }

       // var formData = new FormData($('form#form_degree')[0]);
var formData = $('form#form_degree').serialize();
        $.ajax({
            type: "POST",
            url:"admission-save.php",
            data:  formData,
            dataType: "json",
            cache: false,
            success: function(response) {
              if(response.status == 1){
                $('#language_container').load('form_language.php',function(e){
                  $("#degree_container" ).slideUp( "slow");
                  $('#degree_container').html('');
                  $("#language_container" ).slideDown( "slow");
                });
              }
            }
        });
    });

    $('#form_degree').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules: 
        {
          "degree_pass_year": {
            required: true             
          },
          "degree_stream": {
            required: true             
          },
          "degree_col_univ": {
            required: true             
          },
          "degree_col_name": {
            required: true             
          },
          "degree_col_address": {
            required: true             
          },
          "degree_grade": {
            required: true             
          },
          "degree_other_stream": {
            required: function(element){
            return $("#degree_stream option:selected").val() == "Other";
            } 
          }

        },
        messages: 
        {
         "degree_pass_year": {
            required: "Passing year is required"
          },
          "degree_stream": {
            required: "Stream is required"
          },
          "degree_col_univ": {
            required: "University name is required"
          },
          "degree_col_name": {
            required: "College name is required"
          },
          "degree_col_address": {
            required: "Address is required"
          },
          "degree_grade": {
            required: "Grade is required"
          },
          "degree_other_stream": {
            required: "This is required"
          }
        }
  });

    $('#incase').show(); 
     $("#degree_stream").change(function () {
       var val = $(this).val();
       if (val == "Other") {
         $('#incase').show(); 
        }else{
          $('#incase').hide(); 
        }
      });
   
});
</script>  