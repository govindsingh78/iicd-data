<?php 
  session_start();
  include 'meekrodb.2.3.class.php';

  $query = "select * from user_details where user_id = '".$_SESSION['user_id']."'";
  $row = DB::queryFirstRow($query);

  $streams = array('Science'=>'Science', 'Arts'=>'Arts', 'Commerce'=>'Commerce', 'Engineering'=>'Engineering', 'Other'=>'Other');
?>
<form id="form_hs" name="form_hs">
  <div class="my-dtl-feed">
<div class="col-md-12">
<div class="group"> 
      <div class="col-md-6">  
       <div class="my-input-bx field required-field">    
          <input type="text" id="twelft_pass_year" name="twelft_pass_year" value="<?=$row['twelft_pass_year']?>" class="form-control">
          <span class="bar"></span>
          <label>Year of Passing</label>
       </div>
      </div>
     
      <div class="col-md-6">
        <div class="my-input-bx field required-field">    
            <div class="selectContainer"> 
                <label class="my-label"> 
                Stream of studies at 10+2 or equivalent
                </label>
                 <span class="bar"></span>
                <select id="twelft_stream" name="twelft_stream" class="form-control">
                  <option value="">Select Stream</option>
                  <?php
                  foreach ($streams as $key => $val) {
                      $selected = '';
                      if($key==$row['twelft_stream']){
                          $selected = 'selected="selected"';
                      }
                      echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
                  }
                  ?>
                </select>
             </div>
         </div>
      </div>
      </div>
      <div class="group"> 
         <div class="col-md-4" id="incase">  
           <div class="my-input-bx field required-field">    
              <input type="text" id="twelft_other_stream" name="twelft_other_stream" class="form-control" value="<?=$row['twelft_other_stream']?>" >
              <span class="bar"></span>
              <label> Incase of other stream pl. specify </label>
           </div>
        </div>                                
        <div class="col-md-4">  
           <div class="my-input-bx field required-field">    
              <input type="text" id="twelft_board_name" name="twelft_board_name" class="form-control"  value="<?=$row['twelft_board_name']?>">
              <span class="bar"></span>
              <label>Name of the Board </label>
           </div>
        </div>

         <div class="col-md-4">  
           <div class="my-input-bx field required-field">    
              <input type="text" id="twelft_school_name" name="twelft_school_name" class="form-control"  value="<?=$row['twelft_school_name']?>">
              <span class="bar"></span>
              <label>Name of the School </label>
           </div>
        </div>

       
      </div>
    <div class="group"> 
      <div class="col-md-4">  
           <div class="my-input-bx field required-field">    
              <input type="text" id="twelft_school_address" name="twelft_school_address" class="form-control"  value="<?=$row['twelft_school_address']?>">
              <span class="bar"></span>
              <label>School Address </label>
           </div>
        </div>
       <div class="col-md-4">  
           <div class="my-input-bx field required-field">    
              <input type="text" id="twelft_grade" name="twelft_grade" class="form-control"  value="<?=$row['twelft_grade']?>">
              <span class="bar"></span>
              <label>Grade / Class / Divisions / Appeared </label>
           </div>
        </div>

      </div>

    <nav class="form-section-nav">
      <input type="hidden" name="action" id="action" value="save_hs">
      <span id="btn_back_hs" class="btn-secondary form-nav-prev"> <img src="images/left-arrow.jpg" alt="left"> Prev</span>
      <span id="btn_next_hs" class="btn-std form-nav-next"> Save & Next <img src="images/right-arrow.jpg" alt="left"></span>
    </nav>
    </div>
    </div>
</form>  

<script type="text/javascript">
$(document).ready(function(){


    $("#btn_back_hs").unbind().click(function() {
      $('#exam_container').load('form_exam.php',function(e){
          $("#hs_container" ).slideUp( "slow");
          $('#hs_container').html('');
          $("#exam_container" ).slideDown( "slow");
      });
    });


    $("#btn_next_hs").unbind().click(function() { 
        
        if(!$('#form_hs').valid()){
          return false;
        }

       // var formData = new FormData($('form#form_hs')[0]);
var formData = $('form#form_hs').serialize();
        $.ajax({
            type: "POST",
            url:"admission-save.php",
            data:  formData,
            dataType: "json",
            cache: false,
            success: function(response) {
              if(response.status == 1){

                if("<?=$row['Programme'] =='PG'?>"){
                  $('#degree_container').load('form_degree.php',function(e){
                    $("#hs_container" ).slideUp( "slow");
                    $('#hs_container').html('');
                    $("#degree_container" ).slideDown( "slow");
                  });
                }else{
                  $('#language_container').load('form_language.php',function(e){
                    $("#hs_container" ).slideUp( "slow");
                    $('#hs_container').html('');
                    $("#language_container" ).slideDown( "slow");
                  });
                }  
              }
            }
        });
    });

    $('#form_hs').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules: 
        {
          "twelft_pass_year": {
            required: true             
          },
          "twelft_stream": {
            required: true             
          },
          "twelft_board_name": {
            required: true             
          },
          "twelft_school_name": {
            required: true             
          },
          "twelft_school_address": {
            required: true             
          },
          "twelft_grade": {
            required: true             
          },
          "twelft_other_stream": {
            required: function(element){
            return $("#twelft_stream option:selected").val() == "Other";
            } 
          }

        },
        messages: 
        {
         "twelft_pass_year": {
            required: "Passing year is required"
          },
          "twelft_stream": {
            required: "Steam is required"
          },
          "twelft_board_name": {
            required: "Board / Board name is required"
          },
          "twelft_school_name": {
            required: "School name is required"
          },
          "twelft_school_address": {
            required: "Address is required"
          },
          "twelft_grade": {
            required: "Grade is required"
          },
          "twelft_other_stream": {
            required: "This is required"
          }
        }
  });

    $('#incase').show(); 
     $("#twelft_stream").change(function () {
       var val = $(this).val();
       if (val == "Other") {
         $('#incase').show(); 
        }else{
          $('#incase').hide(); 
        }
      });
    
});
</script>  