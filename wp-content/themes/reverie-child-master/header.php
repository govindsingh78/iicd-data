<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?> "> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?> >
<!--<![endif]-->

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title>
        <?php wp_title('|', true, 'right'); ?>
    </title>
    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Enable Startup Image for iOS Home Screen Web App -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/mobile-load.png" />
    <!-- Startup Image iPad Landscape (748x1024) -->
    <link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/img/devices/reverie-load-ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)" />
    <!-- Startup Image iPad Portrait (768x1004) -->
    <link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/img/devices/reverie-load-ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)" />
    <!-- Startup Image iPhone (320x460) -->
    <link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/img/devices/reverie-load.png" media="screen and (max-device-width: 320px)" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    
    <?php wp_head(); ?>
    <script>  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');  ga('create', 'UA-75923092-1', 'auto');  ga('send', 'pageview'); </script>
    <!-- Facebook Pixel Code -->
    <script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1151196271591907');
    fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=1151196271591907&ev=PageView
&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->
</head>

<body <?php body_class( 'antialiased'); ?>>
    <header>
        <div class="toplink">
            <div class="row">
                <div class="large-6 medium-12 small-12 columns">
                    <div class="topContant">
                        <?php //$headline_top = get_option('headline_top'); if($headline_top) echo $headline_top; ?>
                         <ul class="bxslider">
                    <?php if(get_locale() == 'en_US'){?>   
                    <?php
                     $textsliderArgs = array(
                    'posts_per_page' => 2,
                    'post_type' => 'sliders',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'slider-category',
                            'field' => 'slug',
                            'terms' => 'text-slider',
                            )
                            )
                    );
                $textsliderPosts = get_posts($textsliderArgs);
                foreach ($textsliderPosts as $post){?>
                                <li>
                                    <?php $content = $post->post_content;
                                        $content = strip_tags($content);
                                        echo substr($content, 0, 80).'...' ;?> 
                                    
                                </li>
                                <?php } ?>

                                        <?php } if(get_locale() == 'hi_IN'){?>



<?php
                     $textsliderArgs = array(
                    'posts_per_page' => 2,
                    'post_type' => 'sliders',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'slider-category',
                            'field' => 'slug',
                            'terms' => 'text-slider-hindi',
                            )
                            )
                    );
                $textsliderPosts = get_posts($textsliderArgs);
                foreach ($textsliderPosts as $post){?>
                                <li>
                                    <?php $content = $post->post_content;
                                        $content = strip_tags($content);
                                        echo substr($content, 0, 200).'...' ;?> 
                                    
                                </li>
                                <?php } ?>

                                        <?php } ?>
                        

                        </ul>
                    </div>
                </div>
                <div class="large-1 medium-3 small-12 columns">
                	<center>
                        <?php //echo pll_e( 'Wednesday' );?>
                        <?php if(get_locale() == 'en_US'){?>
                        <a target="_blank" href="https://www.iicd.ac.in/tenders-at-iicd/" style="color: #ffffff;font-weight: 500;font-size: 14px;margin-top: 10px;display: inline-block;padding: 0px 10px;border-left: 1px solid #fff;border-right: 1px solid #fff;line-height: 20px;text-transform: capitalize;">Tenders</a>
                        <?php } if(get_locale() == 'hi_IN'){?>
                            <a target="_blank" href="https://www.iicd.ac.in/tenders-at-iicd/" style="color: #ffffff;font-weight: 500;font-size: 14px;margin-top: 10px;display: inline-block;padding: 0px 10px;border-left: 1px solid #fff;border-right: 1px solid #fff;line-height: 20px;text-transform: capitalize;">निविदाएं</a>
                        <?php } ?>


                               <?php //pll_the_languages(array('dropdown'=>1));  ?>
                    </center>
                </div>
                <div class="large-5 medium-9 small-12 columns">
                    <div class="socialLink"> 
                       <ul class="inline-list" style="display: flex;">
                        
                            <div class="large-5 medium-5 small-12 columns">
                            <li style="display: flex !important; justify-content: center !important;">

                                <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
                               
                            </li>
                            </div>
                            <?php pll_the_languages( array( 'show_flags' => 1,'show_names' => 0 ) ); ?>
                           <div class="large-7 medium-7 small-12 columns"> <?php $mail_id = get_option('mail_id'); if($mail_id){ ?>
                          <center> <ul class="inline-list" style="display: flex !important;justify-content: center !important;">
                            <li><a href="mailto:<?php echo /*$mail_id;*/'admissions@iicd.ac.in' ?>"><i class="fa fa-envelope"></i></a></li>
                            <?php } ?>
                            <li><a href="tel:911412701203"><i class="fa fa-phone"></i></a></li>
                            <?php
                                $twitter_url = get_option('twitter_url'); if($twitter_url){ ?>
                                <li><a href="<?php echo $twitter_url; ?>" target="_black"><i class="fa fa-twitter"></i></a></li>
                                <?php }
                                $facebook_url = get_option('facebook_url'); if($facebook_url){ ?>
                                <li><a href="<?php echo $facebook_url; ?>" target="_black"><i class="fa fa-facebook"></i></a></li>
                                <?php }
                                $insta_url = get_option('insta_url'); if($insta_url){ ?>
                                <li><a href="<?php echo $insta_url; ?>" target="_black"><i class="fa fa-instagram"></i></a></li>
                                <?php } ?>
                                 <li><a href="https://www.linkedin.com/company/13406297/admin/updates/" target="_blank"><i class="fa fa-linkedin"></i></a></li>

                             </div>
                                </ul></center>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php if ( is_home() || is_front_page() ) { 


            ?>
        <div class="logoContiner">
            <div class="row">
                <div class="large-3 medium-3 small-12 columns padd-left-0 padd-right-0">
                    <div class="compnayLogo">
                        <a href="<?php echo site_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/company-logo.png"></a>
                    </div>
                </div>
                <div class="large-6 medium-6 small-12 columns padd-left-0 padd-right-0">
                    <div id="TopSlider" class="owl-carousel owl-theme">
                        <?php
                    $homesliderArgs = array(
                    'posts_per_page' => -1,
                    'post_type' => 'sliders',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'slider-category',
                            'field' => 'slug',
                            'terms' => 'home-slider',
                            )
                            )
                    );
                    $homesliderPosts = get_posts($homesliderArgs);
                    foreach ($homesliderPosts as $post) : setup_postdata($post);
                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                    $url = $thumb['0']; ?>
                            <div class="item"><img src="<?php echo $url; ?>" class="img-responsive"></div>
                            <?php
                              endforeach;
                              wp_reset_postdata();
                            ?>
                    </div>
                </div>
                <div class="large-3 medium-3 small-12 columns padd-left-0 padd-right-0">
                    <div class="ProBanner">
                        <ul class="list-unstyled">
                            <?php
                    $homebannerArgs = array(
                    'posts_per_page' => 2,
                    'post_type' => 'sliders',
                    'order'  => 'asc',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'slider-category',
                            'field' => 'slug',
                            'terms' => 'header-banner',
                            )
                            )
                    );
                $homebannerPosts = get_posts($homebannerArgs );
                foreach ($homebannerPosts as $post) : setup_postdata($post);?>
                                <li>
                                    <div class="programmeDiv">
                                        <?php the_post_thumbnail('full') ?>
                                        <div class="programmeHeading myprograminghead">
                                            <h3 class="banner_title_image">
                                                <?php echo $post->post_content; ?>
                                                </h3>
                                        </div>
                                    </div>
                                </li>
                                <?php
          endforeach;
          wp_reset_postdata();
        ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php } else { ?>
        <div class="logoContiner">
            <div class="row">
                <div class="large-3 medium-3 small-12 columns">
                    <div class="inn-compnayLogo">
                        <a href="<?php echo site_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/company-logo1.png"></a>
                    </div>
                </div>
                <div class="large-9 medium-9 small-12 columns">
                    <div class="InnPageTopSlider">
                        <div id="InnPageTopSlider" class="owl-carousel owl-theme">
                            <?php
      $innerbannerArgs = array(
                    'posts_per_page' => -1,
                    'post_type' => 'sliders',
                    'order'  => 'asc',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'slider-category',
                            'field' => 'slug',
                            'terms' => 'header-slider',
                            )
                            )
                    );
                $innerbannerPosts = get_posts($innerbannerArgs );
                foreach ($innerbannerPosts as $post) : setup_postdata($post);
        ?>
                                <div class="item">
                                    <?php the_post_thumbnail('full'); ?>
                                </div>
                                <?php
          endforeach;
          wp_reset_postdata();
        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <nav>
            <div class="row">
                <div class="large-12 medium-12 small-12 columns">
                    <nav class="top-bar nav-style main-nav" data-topbar="">
            <ul class="title-area">
                <li class="name">
                    
                </li>
                <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
            </ul>
            <section class="top-bar-section ">
                <?php
          wp_nav_menu( array(
              'theme_location' => 'primary',
              'container' => false,
              'depth' => 0,
              'items_wrap' => '<ul class="left onScrollHilight mobile-responsive-menu">%3$s</ul>',
              'fallback_cb' => 'reverie_menu_fallback', // workaround to show a message to set up a menu
              'walker' => new reverie_walker( array(
                  'in_top_bar' => true,
                  'item_type' => 'li',
                  'menu_type' => 'main-menu'
              ) ),
          ) );
      ?>
            </section>
        </nav>
                </div>
            </div>
        </nav>
        
    </header>
    <section id="mainBody">
        <div class="row">
            <?php if ( ! is_front_page() ) { ?>
            <div class="large-12 columns">
                <div class="breadcrumbDiv">
                    <nav>
                        <?php if(function_exists('the_breadcrumbs')) { the_breadcrumbs(); } ?>
                    </nav>
                </div>
            </div>
            <?php } ?>
