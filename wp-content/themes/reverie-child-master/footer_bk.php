  </div>
</section>

<footer>
  <div class="row">
    <div class="large-12 meduim-12 small-12 columns">
<?php $copy_text = get_option('footer_copy'); if($copy_text ){ ?>
      <div class="copyright"> <?php echo $copy_text; ?>  </div>
<?php } ?>
    </div>
    
  </div>
</footer>



<?php wp_footer(); ?>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-listnav.js"></script>
<script>

jQuery( "#acrdn" ).click(function() {
  jQuery( "#acrdncont" ).slideToggle( "slow" );
});

jQuery( "#acrdn1" ).click(function() {
  jQuery( "#acrdncont1" ).slideToggle( "slow" );
});

jQuery( "#acrdn2" ).click(function() {
  jQuery( "#acrdncont2" ).slideToggle( "slow" );
});

  jQuery(function () {
//jQuery('#demoThree li .title strong').prepend('<div style="display:none;">abcd</div>');

//jQuery('#tabpage_3 #demoThree li p, #tabpage_3 #demoFour li p').prepend('<span style="display:none;">abc</span>');

    jQuery('#demoThree').listnav({
        allText: 'All',
        initLetter: 'A',
        includeNums: false,
        noMatchText: 'Nothing matched your filter, please click another letter.',
        prefixes: ['Ms. ','Mr. ']
    });

    jQuery('#demoTwo').listnav({
        allText: 'All',
        initLetter: 'A',
        includeNums: false,
        noMatchText: 'Nothing matched your filter, please click another letter.',
        prefixes: ['Ms. ','Mr. ']
    });

    jQuery('#demoOne').listnav();
    jQuery('.demoTwo').listnav({
      includeAll: false,
      noMatchText: 'There are no matching entries.'
    });

    
    jQuery('#demoFour').listnav({
      initLetter: 'A',
      includeNums: false,
      isAll: false,
    });
//	$('#demoFour').listnav({
//		cookieName: 'cookie-demo',
//		includeAll: false,
//		onClick: function(letter) {
//			$(".myLastClicked").text(letter.toUpperCase());
//		}
//	});
    jQuery('#demoFive').listnav({
      includeOther: true,
      prefixes: ['a', 'The Complete Works of']
    });
    jQuery('#demoSix').listnav({
      filterSelector: '.last-name',
      includeNums: false,
      removeDisabled: true,
      allText: 'Complete Cast of Star Trek TNG'
    });
    jQuery('.demo a').click(function (e) {
      e.preventDefault();
    });
  });

jQuery(document).ready(function(){
	jQuery('.facultyTab ul.tabs li:first-child, .tabs-content .content:first-child').addClass('active');
  });
    

</script>

<script>
  (function ($) {
    $(document).foundation('topbar', 'reflow');
  })(jQuery);
</script>
<script>
  (function ($) {
    $(document).ready(function () {
      $(document).foundation();
      $('#demoThree li .title').on('click', function (e) {
        e.preventDefault();
        $('.divduration').hide();
        $('#demoThree li .title').removeClass('active');
        $(this).addClass('active');
        $(this).parent().children('.divduration').show();
      });
    });
  })(jQuery);
</script>

<script>
jQuery(document).ready(function () {
	jQuery('.readmore').click(function () {
                    if (jQuery(this).text() == 'Read More') {
                      jQuery(this).text('Read Less');
                    } else {
                      jQuery(this).text('Read More')
                    }
                    jQuery(this).prev('.showmore').fadeToggle(800);
                });
});
</script>


<script>
    jQuery(document).on('ready page:load', function () {
jQuery(function(){ jQuery(document).foundation(); });
});
  </script>


<script type="text/javascript">

jQuery(document).ready(function() {
    
     jQuery("#projectongoingopen").hide();
      jQuery("#bestheld").hide();
	  
	  	  
});
  
    jQuery("#projectover").click(function(){
    jQuery("#projectoveropen").toggle('slow');
	 jQuery("#projectongoingopen").hide('slow');
      jQuery("#bestheld").hide('slow');
	
});
     jQuery("#projectongoing").click(function(){
    jQuery("#projectongoingopen").toggle('slow');
	 jQuery("#projectoveropen").hide('slow');
      jQuery("#bestheld").hide('slow');
});


  jQuery("#projectheld").click(function(){
    jQuery("#bestheld").toggle();
	 jQuery("#projectoveropen").hide('slow');
      jQuery("#projectongoingopen").hide('slow');
});

</script>
	
</body>
</html>