<?php 
get_header(); ?>

<?php //while (have_posts()) : the_post(); ?>
   <div class="small-12 columns" style="margin-top: -40px;">
      <ul class="large-12 large-block-grid-2 large-block-grid-2">
      <?php
      		$args = array( 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC', );
          $myposts = get_posts( $args );
          foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
        <li>
          <div class="postDiv">
            <div class="blog-date">  
              <span class="month"><?php the_time('M'); ?></span> 
              <span class="day"> <?php the_time('j'); ?></span> 
              <span class="year"><?php the_time('Y'); ?></span>
            </div>
            <div></div>
            <div class="post-content">
              <h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
              <div class="post-running">
<p> <?php
                                                                    $content = get_the_content();
                                                                    $content = strip_tags($content);
                                                                    echo substr($content, 0, 150);
                                                                ?>... </p>
<a href="<?php the_permalink(); ?>" class="more-news">Read More</a>
              </div>
            </div>
          </div>
        </li>
<?php endforeach; 
wp_reset_postdata();?>
      </ul>
    </div>

<?php //endwhile; // End the loop ?>

<?php get_footer(); ?>
