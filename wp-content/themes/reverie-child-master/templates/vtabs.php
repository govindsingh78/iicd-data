<?php 
/**
 * Template Name: Vtabs Page
 **/
get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
<div class="large-12 columns">
      <h1 class="orange-bdr inn-heading"><?php the_title(); ?></h1>
    </div>
<script>
jQuery(document).ready(function(){
    jQuery('.tabs li:first-child, .tabs-content .content:first-child').addClass('active');
});
</script>
<div class="large-12 medium-12 small-12 columns">
      <div class="facultyTab">
        <ul class="tabs" data-tab>
<?php
                           $pages = get_pages('child_of='.$post->ID.'&sort_column=post_date');

                            // $pages = get_pages(array(
                            // 'post_parent' => $post->ID,
                            // 'sort_column' => 'post_date',
                            // ));

                            // $args = array(
                            // 'child_of' => $post->ID,
                            // 'sort_column' => 'post_date',
                            // );
                            // $pages = new WP_Query($args);
                            // $parent[] = get_post($post->ID);
                            //$pages = get_pages($parent);
                            foreach($pages as $page)
                            {
                            ?>
          <li class="tab-title"><a href="#<?php echo $page->post_name; ?>"><?php echo $page->post_title; ?></a></li>
<?php } ?>
        </ul>
        <div class="tabs-content">
<?php
                            $pages = get_pages('child_of='.$post->ID.'&sort_column=post_date');
                            foreach($pages as $page)
                            {
                            ?>
          <div class="content" id="<?php echo $page->post_name; ?>">
            <div class="tabsHeading">
              <h3><?php echo $page->post_title; ?></h3>
              <div id="tabpage_3" class="tabContainer">
                <div class="listWrapper">
                  <?php echo $page->post_content; ?>
                </div>
              </div>
            </div>
          </div>
<?php } ?>
        </div>
      </div>
    </div>

<?php endwhile; // End the loop ?>

<?php get_footer(); ?>
<script>
  jQuery(document).ready(function(){
	jQuery('.facultyTab ul.tabs li:first-child, .tabs-content .content:first-child').addClass('active');
  })
</script>