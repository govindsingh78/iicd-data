<?php 
/**
 * Template Name: Contact Page
 **/
get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
<div class="large-12 columns">
      <h1 class="orange-bdr inn-heading"><?php the_title(); ?></h1>
    </div>
<div class="large-12 columns">
      <div class="google-map">
        <iframe src="<?php the_field('google_map'); ?>" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="contactHeadingDiv">
      <div class="large-6 small-12 columns controle-heading contactform">
        <?php echo do_Shortcode('[gravityform id="1" title="false" description="false" ajax="true"]'); ?>
      </div>
      <div class="large-6 small-12 columns" >
        <?php the_content(); ?>
        </div>
      </div>
    </div>
  </div>

<?php endwhile; // End the loop ?>

<?php get_footer(); ?>