<?php 
/**
 * Template Name: No Heading
 **/
get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
    <div class="large-12 medium-12 small-12 columns">
      <div class="innpageText">
        <?php the_content(); ?>
      </div>  
    </div>

<?php endwhile; // End the loop ?>

<?php get_footer(); ?>