<?php 
/**
* Template Name: Faculty Page
**/
    get_header();
?>
<?php
    while (have_posts())
    {
        the_post();
?>
<div class="large-12 columns">
    <h1 class="orange-bdr inn-heading"><?php the_title(); ?></h1>
</div>
<div class="large-12 medium-12 small-12 columns">
    <div class="facultyTab">
        <ul class="tabs" data-tab="">
<?php
    $taxonomy = 'faculty-category';
    $terms = get_terms($taxonomy, array('hide_empty' => false, 'exclude' => '7'));

    foreach($terms as $term)
    {
?>
            <li class="tab-title"><a href="#<?php echo $term->slug; ?>"><?php echo pll_e($term->name); ?></a></li>
<?php
    }
?>
        </ul>

        <div class="tabs-content">
<?php
    foreach($terms as $term)
    {
        $term_slug = $term->slug;
?>
            <div class="content" id="<?php echo $term_slug; ?>">
                <div class="tabsHeading">
                    <h3><?php echo pll_e($term->name); ?></h3>
                    <p><?php echo pll_e($term->description); ?></p>
                    <div id="tabpage_3" class="tabContainer">
                        <div class="listWrapper">

<?php
        if( $term->term_id == 27 )
        {
?>
                            <ul class="row" id="demoThree">
<?php
        }
        else
        {
?>
                            <ul class="row" id="demoTwo">
<?php
        }
?>
<?php
        $faculyArgs = array(
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'DESC',
            'post_type' => 'faculties',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'faculty-category',
                    'field' => 'slug',
                    'terms' => $term->slug
                )
            )
        );

        $facultyPosts = get_posts($faculyArgs);

        foreach ($facultyPosts as $fpost)
        {
            $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($fpost->ID), 'full');
            $url = $thumb['0'];
            $designation= get_field('designation', $fpost->ID);
            $email= get_field('email', $fpost->ID);
            $description= get_field('description', $fpost->ID);
            //print_r($fpost);
            if( $term->term_id == 27 )
            {
?>
                                <li class="large-12 medium-12 small-12 columns">
                                    <div class="title"><strong><?php echo $fpost->post_title; ?>,</strong> <?php echo $description; ?></div>
<?php
                if(has_post_thumbnail($fpost->ID))
                {
?>
                                    <!-- <div class="divduration">
                                        <div class="course-bg ">
                                            <div class="studentImg">
                                                <img src="<?php //echo $url; ?>" class="th">
                                            </div>
                                        </div>
                                    </div> -->
<?php
                }
?>
                                </li>
<?php
            }
            else
            {
?>
                                <li class="large-3 medium-3 small-6 columns">
                                    <a href="javascript:;" data-reveal-id="<?php echo $fpost->post_name; ?>">
<?php
                if(has_post_thumbnail($fpost->ID))
                {
?>
                                        <img src="<?php echo $url; ?>" class="th">
<?php
                }
                else
                {
?>
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/demo-img1.jpg" class="th">
<?php
                }
?>
                                    </a>
                                    <h1 class="FacName"><?php echo $fpost->post_title; ?></h1>
<?php
                if($designation)
                {
?>
                                    <h2 class="FacDree"><?php echo $designation; ?></h2>
<?php
                }
                if($email)
                {
?>
                                    <span><a href="mailto:<?php echo $email; ?> "><?php echo $email; ?></a></span>
<?php
                }
?>
                                    <div id="<?php echo $fpost->post_name; ?>" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                                        <div class="img-portfolio">
                                            <div class="large-3 columns">
<?php
                if(has_post_thumbnail($fpost->ID))
                {
?>
                                                <img src="<?php echo $url; ?>" class="th">
<?php
                }
                else
                {
?>
                                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/demo-img1.jpg" class="th">
<?php
                }
?>
                                            </div>
                                            <div class="large-9 columns">
                                                <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th width="200">Name</th>
                                                            <th><?php echo $fpost->post_title; ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
<?php
                if($designation)
                {
?>
                                                        <tr>
                                                            <td>Designation</td>
                                                            <td><?php echo $designation; ?></td>
                                                        </tr>
<?php
                }
                if($email)
                {
?>
                                                        <tr>
                                                            <td>Email Id</td>
                                                            <td><a href="mailto:<?php echo $email; ?>"> <?php echo $email; ?></a></td>
                                                        </tr>
<?php
                }
                if($description)
                {
?>
                                                        <tr>
                                                            <td>Description</td>
                                                            <td><?php echo $description; ?></td>
                                                        </tr>
<?php
                }
?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </li>
<?php
            }
        }
?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
<?php
    }
?>
        </div>
    </div>
</div>
<?php
    }
?>
<?php get_footer(); ?>
