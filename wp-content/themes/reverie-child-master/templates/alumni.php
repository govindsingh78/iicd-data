<?php 
/**
* Template Name: Alumni Page
**/
    get_header();
?>
<?php
    while (have_posts())
    {
        the_post();
    ?>
    <div class="large-12 columns">
        <h1 class="orange-bdr inn-heading"><?php the_title(); ?></h1>
    </div>
    <div class="large-12 medium-12 small-12 columns">
        <div class="expand">
        <?php
                $i = 1;
                $taxonomy = 'alumni-category';
                $terms = get_terms($taxonomy, array('hide_empty' => false, 'exclude' => '7'));
                foreach($terms as $term)
                {
                    $term_slug = $term->slug;
                ?>
                <?php do_shortcode('[expand title="<?php echo $i; ?> <?php echo $term->name; ?>"]'); ?>
                <table style="height: 10px;" width="902">
                    <tbody>
                        <?php
                            $alumniArgs = array(
                                            'posts_per_page' => -1,
                                            'orderby' => 'title',
                                            'order' => 'DESC',
                                            'post_type' => 'alumni',
                                            'post_status' => 'publish',
                                            'tax_query' => array(
                                                array(
                                                    'taxonomy' => 'alumni-category',
                                                    'field' => 'slug',
                                                    'terms' => $term->slug
                                                )
                                            )
                                        );

                                        $alumniArgs = get_posts($alumniArgs);

                                        foreach ($alumniArgs as $apost)
                                        {
                                            $name = get_field('name', $apost->ID);
                                            $mobile = get_field('mobile', $apost->ID);
                                            $email_id = get_field('email_id', $apost->ID);
                                ?>             
                                <tr>
                                    <td colspan="2" width="460">
                                        <?php echo $name ?>    
                                    </td>
                                </tr>
                                <?php  }
                                ?>
                    </tbody>
                </table>
                 <?php do_shortcode('[/expand]');?>
                <?php } ?>
            </div>
</div>
<?php } ?>
<?php get_footer(); ?>
