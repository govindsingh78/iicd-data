<?php 
/**
 * Template Name: Tab Template
 **/
get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

<div class="large-12 columns">
      <h1 class="orange-bdr inn-heading"> <?php the_title(); ?> </h1>
    </div>

<?php endwhile; // End the loop ?>

<div class="row cd-faq" data-equalizer>
    <div class="large-2 medium-6 small-12 columns">
      <ul id="projectTabs" class="tabs vertical" data-tab>
<?php
                            $pages = get_pages('child_of='.$post->ID.'&sort_column=post_date');
                            foreach($pages as $page)
                            {
                            ?>
        <li class="tab-title"><a href="#<?php echo $page->post_name; ?>"><?php echo $page->post_title ?></a></li>
<?php } ?>
      </ul>
    </div>
    <div class="large-10 medium-12 small-12 columns">
      <div class="tabs-content CampusLife cd-faq-group">
<?php
                            $pages = get_pages('child_of='.$post->ID.'&sort_column=post_date');
                            foreach($pages as $page)
                            {
                            ?>
           <div class="content" id="<?php echo $page->post_name; ?>">
          <div class="capusHeading">
            <h1 class="capus-bor-lef" ><?php echo $page->post_title; ?> </h1>
          </div>
          <div class="capusrunningText">
            <?php echo $page->post_content; ?>
          </div>  
        </div>
<?php } ?>
    </div>
  </div>


<?php get_footer(); ?>
<script>
jQuery(document).ready(function(){
    jQuery('.tabs li:first-child, .tabs-content .content:first-child').addClass('active');
});
</script>