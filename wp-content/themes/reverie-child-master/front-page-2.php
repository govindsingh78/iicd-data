
<div class="large-12 medium-12 small-12 columns">
    <div class="outService">
        <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-4" data-equalizer-watch>
            <li>

                 
                  
                <!-- For English use 30-->

                <?php
                    
                  $post_30 = get_post( 30 ); 
                  $title = get_field('home_title', $post_30->ID);
                  $content = get_field('home_content', $post_30->ID);
                  ?>
                    <a href="<?php echo get_post_permalink( $post_30->ID ); ?>">
                        <div class="home-block-img" style="overflow: hidden;">
                           <?php echo get_the_post_thumbnail( $post_30->ID, 'full' ); ?>
                        </div>   
                    </a>
                    <a href="<?php echo get_post_permalink( $post_30->ID ); ?>"><h3 class="home-block-title"><?php echo $title; ?> </h3></a>
                    <p>
                       <?php echo $content; ?>
                    </p>   
                    <div class="clear"></div>
                    <span> <a href="<?php echo get_post_permalink( $post_30->ID ); ?>" onclick="ga('send', 'event', 'button', 'click', 'learn more')"> Learn More</a></span>
                 
            </li>
            <li>
                <?php
                // 4248
                  $post_157 = get_post( 157 ); 
                  $title = get_field('home_title', $post_157->ID);
                  $content = get_field('home_content', $post_157->ID);
                  ?>
                    <a href="<?php echo get_post_permalink( $post_157->ID ); ?>">
                        <div class="home-block-img" style="overflow: hidden;">
                           <?php echo get_the_post_thumbnail( $post_157->ID, 'full' ); ?> 
                        </div>
                    </a>
                    <a href="<?php echo get_post_permalink( $post_157->ID ); ?>"><h3 class="home-block-title"> <?php echo $title; ?> </h3></a>
                    <p>
                        <?php echo $content; ?>
                    </p>
                    <div class="clear"></div>
                    <span> <a href="<?php echo get_post_permalink( $post_157->ID ); ?>" onclick="ga('send', 'event', 'button', 'click', 'learn more')"> Learn More</a></span>
            </li>
            <li>
                <?php
                  $post_35 = get_post( 35 ); 
                  $title = get_field('home_title', $post_35->ID);
                  $content = get_field('home_content', $post_35->ID);
                  ?>
                    <a href="<?php echo get_post_permalink( $post_35->ID ); ?>">
                        <div class="home-block-img" style="overflow: hidden;">
                            <?php echo get_the_post_thumbnail( $post_35->ID, 'full' ); ?>
                        </div>    
                    </a>
                    <a href="<?php echo get_post_permalink( $post_35->ID ); ?>"><h3 class="home-block-title">Placements</h3></a>
                    <p>
                        <?php echo $content; ?>
                    </p>
                    <span> <a href="<?php echo get_post_permalink( $post_35->ID ); ?>" onclick="ga('send', 'event', 'button', 'click', 'learn more')"> Learn More</a></span>
            </li>
            <li>
                <!-- News slider -->
                <div class="holder">
                  <h3><a class="home-block-title" href="/category/news/">News and Updates</a></h3>
                  <ul id="ticker01">
                    <?php 
                      $args = array( 'posts_per_page' => -1, 'category' => 8 );
                      $myposts = get_posts( $args );
                      foreach ( $myposts as $post ) : 
                    ?>
                    <li>
                      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                      <p class="rNewsDate"><?php echo get_the_date('F j, Y'); ?></p>
                    </li>
                   <?php endforeach; 
                   wp_reset_postdata(); ?>
                  </ul>
               </div>
               <!-- News slider ends -->

                <?php
                  // $post_287 = get_post( 287 ); 
                  // $title = get_field('home_title', $post_287->ID);
                  // $content = get_field('home_content', $post_287->ID);
                  ?>
                    <!-- <a href="<?php //echo get_post_permalink( $post_287->ID ); ?>">
                        <div class="home-block-img" style="overflow: hidden;">
                            <?php //echo get_the_post_thumbnail( $post_287->ID, 'full' ); ?>
                        </div>    
                    </a>
                    <a href="<?php //echo get_post_permalink( $post_287->ID ); ?>"><h3 class="home-block-title"><?php echo $title; ?> </h3></a>
                    <p>
                        <?php //echo $content; ?>
                    </p>
                    <span> <a href="<?php //echo get_post_permalink( $post_287->ID ); ?>" onclick="ga('send', 'event', 'button', 'click', 'learn more')"> Learn More</a></span> -->
            </li>
        </ul>
    </div>
</div>
<div class="newletter-bg">
    <div class="large-3 medium-3 small-12 columns">
        <div class="downHeading">
            <a href="<?php the_field('link1'); ?>" target="_black"> <img src="<?php the_field('link1_image'); ?>" onclick="ga('send', 'event', 'button', 'click', 'footer')">  </a>
        </div>
    </div>
    <div class="large-3 medium-3 small-12 columns">
        <div class="downHeading">
            <a href="<?php the_field('link2'); ?>" target="_black"> <img src="<?php the_field('link2_image'); ?>" onclick="ga('send', 'event', 'button', 'click', 'footer')"> </a>
        </div>
    </div>
    <div class="large-3 medium-3 small-12 columns">
        <div class="downHeading">
            <a href="<?php the_field('link3'); ?>"> <img src="<?php the_field('link3_image'); ?>" onclick="ga('send', 'event', 'button', 'click', 'footer')"> </a>
        </div>
    </div>
    <div class="large-3 medium-3 small-12 columns">
        <div class="downHeading">
            <a href="<?php the_field('link4'); ?>"> <img src="<?php the_field('link4_image'); ?>" onclick="ga('send', 'event', 'button', 'click', 'footer')"> </a>
        </div>
    </div>
</div>

 

<div class="large-12 medium-12 small-12 columns">
	<div class="SubinnpageText def-page-text home-about">
      <input type="checkbox" class="read-more-state" id="post-1" /> 
	    <h3 class="home-about-title"><strong>About IICD</strong></h3>
	    <p class="read-more-wrap">Indian Institute of Crafts & Design, Jaipur, is one of the leading crafts and design colleges in India that work towards the evolution of crafts and the artisans in the contemporary socio-economic context. IICD is uniquely positioned and offers undergraduate and postgraduate degree programmes in the field of crafts & design. These programmes provide student with the means to grow as sensitive, creative designers and practitioners of craft with clear goals to contribute towards Indian culture and society as a global citizen.
	    <span class="read-more-target">
	        One of the top-notch crafts and design colleges in India, IICD teaches with a unique pedagogy that synergizes traditional knowledge and skills with contemporary methodologies. IICD offers excellent infrastructural facilities for pursuing creative endeavor, with a beautiful campus located in the ‘craft city’ of India, Jaipur.
          Branding IICD as one of the leading design colleges in India won't be untrue for it provides an experiential learning environment with strong focus on conceptual and practical experience. IICD is one such design institute in India that has an academic culture enriched with competent faculty, experts, practitioners and student community from various parts of the world. The Institute also has a close collaboration with a range of stakeholders in craft sector which helps students gain a greater exposure of the sector.
          As a leading crafts and design colleges in India, IICD institution has close interaction and collaboration with leading crafts & design institutes and organizations that help prepare its students to meet the growing challenges at macro and micro level. IICD, apart from imparting quality education, endeavors to  grooming the personality of its students. The same help them have bright career prospects in the crafts and design industry.
          </span>
	      </p>
        <label for="post-1" class="read-more-trigger"></label>
	    </div>
	</div>
	<div class="large-12 medium-12 small-12 columns">
      <div class="innpageText def-page-text inxtittaghead">
        <h1 class="home-about-title">Designing Courses in India</h1>
        <div class="SubinnpageText def-page-text">
			<p>There is a majority of people who are passionate about designing and wish to pursue a career in same. To turn their passion into a profession, Indian Institute of Crafts &amp; Design, Jaipur (IICD) offers some of the best designing courses in India. Whether an individual aims for a degree in <a href="http://www.iicd.ac.in/fashion-design-fd/">fashion designing</a> or wishes to pursue crafts and design courses, IICD has all the necessary facilities and amenities for the same. IICD’s courses are broadly classified into:</p>
			<ul>
			<li><a href="https://www.iicd.ac.in/under-graduate-design/" target="_blank">Undergraduate Courses</a></li>
			<li><a href="https://www.iicd.ac.in/post-graduate-design/" target="_blank">Postgraduate Courses</a></li>
			</ul>
			<p>The above-mentioned categories offer some of the popular designing courses in India which include courses on:</p>
			<ul>
			<li>Hard materials such as wood, metal, and stone</li>
			<li>Soft materials which involve textile, leather, paper and natural fiber</li>
			<li>Fired materials such as Earthenware, Stoneware, Terracotta, Porcelain</li>
			<li>Fashion Design (UG Only)</li>
			</ul>
			<p>All the aforementioned courses are offered under the guidance of specialist technicians and leading practitioners in integrated workshops and dedicated studios. All these workshops and studios are equipped with state-of-the-art machinery and equipment.</p>
			<p>Branding the courses offered by IICD in India as the best won’t be untrue for they are backed by the combination of self-directed study as well as professional projects in the classroom. Above all, these courses provide opportunities for collaborative and interdisciplinary work in the industry to their students.</p>
			<h2>Degree in Crafts and Design</h2>
			<p>IICD was setup to realize the potential of the craft sector and foster innovation and creative thinking for well-crafted quality products. With a degree in crafts and design, IICD aims to develop high-quality professionals to act as catalysts of change in the crafts sector. IICD is committed to the inclusion of artisans’ wards in the major education programmes. It also urges the State Government entities, Social Enterprises and Non-Government Organizations to support and sponsor candidates who wish to pursue a degree in crafts and design.</p>
			<p>IICD encourages a large number of individuals to pursue a career in the craft and design industry owing to the bright career prospects and rapid growth rate of the industry.</p>
			<ul>
			<li>230 lakh craftsmen engaged in different craft sectors with an estimated 360 craft clusters in India.</li>
			<li>Handicraft and Handloom sectors together form a Rs 24,300 crore industry contributing Rs. 10,000 crore to India’s export.</li>
			<li>As per the 12th plan working document, the craft industry recorded an earning of Rs. 1.62 lakh crore.</li>
			</ul>
			</div>
      </div>  
    </div>

</div> 
 
