<?php
if( ! function_exists( 'reverie_enqueue_style1' ) ) {
	function reverie_enqueue_style1()
	{
		wp_register_style( 'fontawsm', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', array(), '1.0', 'all' );
        wp_enqueue_style( 'fontawsm' );

        wp_register_style( 'lato-font', 'https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic', array(), '1.0', 'all' );
        wp_enqueue_style( 'lato-font' );

        wp_register_style( 'raleway-font', 'https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900', array(), '1.0', 'all' );
        wp_enqueue_style( 'raleway-font' );

        wp_register_style( 'fondation-css', get_stylesheet_directory_uri() . '/css/foundation.min.css', array(), '1.0', 'all' );
        wp_enqueue_style( 'fondation-css' );

        wp_register_style( 'carousel-css', get_stylesheet_directory_uri() . '/css/owl.carouselnew.css', array(), '1.0', 'all' );
        wp_enqueue_style( 'carousel-css' );

        wp_register_style( 'responsive-css', get_stylesheet_directory_uri() . '/css/responsive.css', array(), '1.0', 'all' );
        wp_enqueue_style( 'responsive-css' );

        wp_register_style( 'scss-css', get_stylesheet_directory_uri() . '/scss/sass.css', array(), '1.0', 'all' );
        wp_enqueue_style( 'scss-css' );

        wp_register_style( 'breadcrumb-css', get_stylesheet_directory_uri() . '/css/breadcrumb.css', array(), '1.0', 'all' );
        wp_enqueue_style( 'breadcrumb-css' );

        wp_register_style( 'listnav-css', get_stylesheet_directory_uri() . '/css/listnav.css', array(), '1.0', 'all' );
        wp_enqueue_style( 'listnav-css' );

        wp_register_style( 'magnific-css', get_stylesheet_directory_uri() . '/css/magnific-popup.css', array(), '1.0', 'all' );
        wp_enqueue_style( 'magnific-css' );

        wp_register_style( 'custom-css', get_stylesheet_directory_uri() . '/css/custum.css', array(), '1.0', 'all' );
        wp_enqueue_style( 'custom-css' );
		
	}
}
add_action( 'wp_enqueue_scripts', 'reverie_enqueue_style1' );

if( !function_exists( "wp_reverie_theme_js1" ) ) {  
  function wp_reverie_theme_js1(){

    wp_register_script( 'indexJs', 
      get_stylesheet_directory_uri() . '/js/index.js', 
      array('jquery'), 
      '1.2' );

    wp_register_script( 'foundationJs', 
      get_stylesheet_directory_uri() . '/js/foundation.min.js', 
      array('jquery'), 
      '1.2' );

    wp_register_script( 'customowlJs', 
      get_stylesheet_directory_uri() . '/js/custome-owl.js', 
      array('jquery'), 
      '1.2' );

    wp_register_script( 'equalizerJs', 
      get_stylesheet_directory_uri() . '/js/foundation/foundation.equalizer.js', 
      array('jquery'), 
      '1.2' );

    wp_register_script( 'responsivetblJs', 
      get_stylesheet_directory_uri() . '/js/responsive-tables.js', 
      array('jquery'), 
      '1.2' );

    wp_register_script( 'foundationardnJs', 
      get_stylesheet_directory_uri() . '/js/foundation/foundation.accordion.js', 
      array('jquery'), 
      '1.2' );

    wp_register_script( 'owlJs', 
      get_stylesheet_directory_uri() . '/js/owl.carousel.min.js', 
      array('jquery'), 
      '1.2' );

    wp_register_script( 'megnificJs', 
      get_stylesheet_directory_uri() . '/js/jquery.magnific-popup.js', 
      array('jquery'), 
      '1.2' );

    wp_register_script( 'revealJs', 
      get_stylesheet_directory_uri() . '/js/foundation/foundation.reveal.js', 
      array('jquery'), 
      '1.2' );
  
    wp_enqueue_script( 'indexJs' );
    wp_enqueue_script( 'foundationJs' );
    wp_enqueue_script( 'customowlJs' );
    wp_enqueue_script( 'equalizerJs' );
    wp_enqueue_script( 'responsivetblJs' );
    wp_enqueue_script( 'foundationardnJs' );
    wp_enqueue_script( 'owlJs' );
    wp_enqueue_script( 'megnificJs' );
    wp_enqueue_script( 'revealJs' );
  }
}
add_action( 'wp_enqueue_scripts', 'wp_reverie_theme_js1' );

function add_theme_menu_item()
{
	add_menu_page("Theme Options", "Theme Options", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");

function theme_settings_page()
{
    ?>
	    <div class="wrap">
	    <h1>Theme Panel</h1>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields("section");
	            do_settings_sections("theme-options");      
	            submit_button(); 
	        ?>          
	    </form>
		</div>
	<?php
}


function display_twitter_element()
{
	?>
    	<input type="text" name="twitter_url" id="twitter_url" value="<?php echo get_option('twitter_url'); ?>" style="min-width: 30%;" />
    <?php
}

function display_facebook_element()
{
	?>
    	<input type="text" name="facebook_url" id="facebook_url" value="<?php echo get_option('facebook_url'); ?>" style="min-width: 30%;" />
    <?php
}

function display_insta_element()
{
	?>
    	<input type="text" name="insta_url" id="insta_url" value="<?php echo get_option('insta_url'); ?>" style="min-width: 30%;" />
    <?php
}

function display_topheadline_element()
{
	?>
    	<input type="text" name="headline_top" id="headline_top" value="<?php echo get_option('headline_top'); ?>" style="min-width: 30%;" />
    <?php
}

function display_mail_element()
{
	?>
    	<input type="text" name="mail_id" id="mail_id" value="<?php echo get_option('mail_id'); ?>" style="min-width: 30%;" />
    <?php
}

function display_footer_copyright()
{
	?>
<input type="text" name="footer_copy" id="footer_copy" value="<?php echo get_option('footer_copy'); ?>" style="min-width: 30%;" />
    <?php
}

function display_theme_panel_fields()
{
    add_settings_section("section", "All Settings", null, "theme-options");
	
    add_settings_field("twitter_url", "Twitter Profile Url", "display_twitter_element", "theme-options", "section");
    add_settings_field("facebook_url", "Facebook Profile Url", "display_facebook_element", "theme-options", "section");
    add_settings_field("insta_url", "Instagram Profile Url", "display_insta_element", "theme-options", "section");
    add_settings_field("headline_top", "Top Headline", "display_topheadline_element", "theme-options", "section");
    add_settings_field("mail_id", "Email ID", "display_mail_element", "theme-options", "section");
    add_settings_field("footer_copy", "Footer Copyright", "display_footer_copyright", "theme-options", "section");

    register_setting("section", "twitter_url");
    register_setting("section", "facebook_url");
    register_setting("section", "insta_url");
    register_setting("section", "headline_top");
    register_setting("section", "mail_id");
    register_setting("section", "footer_copy");
}

add_action("admin_init", "display_theme_panel_fields");

add_action( 'init', 'alumni' );
function alumni() {
  register_post_type( 'alumni',
    array(
      'labels' => array(
        'name' => __( 'Alumni' ),
        'singular_name' => __( 'Alumni' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'alumni-post'),
      'taxonomies' => array( 'post_tag', "alumni-category"),
	  'supports' => array('title','editor','author','thumbnail','excerpt','comments')
    )
  );
}
register_taxonomy( 'alumni-category', 'Test', array( 'hierarchical' => true, 'label' => 'Alumni Category', 'query_var' => true, 'rewrite' => true ) );

add_action( 'init', 'sliders' );
function sliders() {
  register_post_type( 'sliders',
    array(
      'labels' => array(
        'name' => __( 'Sliders' ),
        'singular_name' => __( 'Slider' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'sliders-post'),
      'taxonomies' => array( 'post_tag', "slider-category"),
	  'supports' => array('title','editor','author','thumbnail','excerpt','comments')
    )
  );
}
register_taxonomy( 'slider-category', 'Test', array( 'hierarchical' => true, 'label' => 'Sliders Category', 'query_var' => true, 'rewrite' => true ) );




add_action( 'init', 'faculties' );
function faculties() {
  register_post_type( 'faculties',
    array(
      'labels' => array(
        'name' => __( 'Faculties' ),
        'singular_name' => __( 'Faculty' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'faculty-post'),
      'taxonomies' => array( 'post_tag', "faculty-category"),
	  'supports' => array('title','author','thumbnail','excerpt','comments')
    )
  );
}
register_taxonomy( 'faculty-category', 'Test', array( 'hierarchical' => true, 'label' => 'Faculty Category', 'query_var' => true, 'rewrite' => true ) );



function the_breadcrumbs() {
 echo "<ol class='cd-breadcrumb'>";
        global $post;
 
        if (!is_home()) {
 
            echo "<li><a href='";
            echo get_option('home');
            echo "'>";
            echo "Home";
            echo "</a></li>";
 
            if (is_category() || is_single()) {
 
                $cats = get_the_category( $post->ID );
 
                foreach ( $cats as $cat ){
echo "<li>";
                    echo "<a href=" . wp_get_referer() . ">";
                    echo $cat->cat_name . "</a></li>";
                }
                if (is_single()) {
echo "<li>";
                    the_title();
echo "</li>";
                }
            } elseif (is_page()) {
 
                if($post->post_parent){
                    $anc = get_post_ancestors( $post->ID );
                    $anc_link = get_page_link( $post->post_parent );
 
                    foreach ( $anc as $ancestor ) {
                        $output = " <li> <a href=".$anc_link.">".get_the_title($ancestor)."</a> </li> ";
                    }
 
                    echo $output;
echo "<li>";
                    the_title();
echo "</li>";
 
                } else {
                    echo ' <li> ';
                    echo the_title();
echo "</li>";
                }
            }
        }
echo "</ol>";
}



//pll_register_string($name, $string, $group, $multiline);
add_action( 'after_setup_theme','register_custom_string');
    function register_custom_string(){
        pll_register_string('sunday','Sunday');
        pll_register_string('monday','Monday');
        pll_register_string('tuesday','Tuesday');
        pll_register_string('wednesday','Wednesday');
        pll_register_string('thursday','Thursday');
        pll_register_string('friday','Friday');
        pll_register_string('saturday','Saturday');
        pll_register_string('placements','Placements');
        pll_register_string('inhousefaculty','In House Faculty');
        pll_register_string('visitingfaculty','Visiting Faculty');
        pll_register_string('iicdfacultytext','IICD invites eminent experts from academia, social sector and industries who continuously engaged with learning process at the institute.');
        
    }


//Register translated strings
  include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
  if ( is_plugin_active(  'polylang/polylang.php'  ) ) {
    register_custom_string();
  }


  

add_action( 'init', 'update_my_custom_type', 99 );
function update_my_custom_type() {
	global $wp_post_types;
	if ( post_type_exists( 'sliders' ) ) {
		$wp_post_types['sliders']->exclude_from_search = true;
	}
}





?>
