<?php 
get_header();
// $lang_type = get_locale(); //hi_IN 
// if($lang_type == 'hi_IN'){

// $url = 'http://192.168.1.63/IICD-DATA/?page_id=5&lang=hi';
// if ( wp_redirect( $url ) ) {
//     exit;
// }

// }
?>
<div class="large-12 medium-12 small-12 columns">
    <div class="outService">
        <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-4" data-equalizer-watch>
            <li>

                <!-- For Hindi use 4114-->

                <?php
                  $lang_type = get_locale(); //hi_IN 
                  if($lang_type == 'hi_IN'){
                  $post_4114 = get_post( 4114 ); 
                  $title = get_field('home_title', $post_4114->ID);
                  $content = get_field('home_content', $post_4114->ID);
                  ?>
                    <a href="<?php echo get_post_permalink( $post_30->ID ); ?>">
                        <div class="home-block-img" style="overflow: hidden;">
                           <?php echo get_the_post_thumbnail( $post_4114->ID, 'full' ); ?>
                        </div>   
                    </a>
                    <a href="<?php echo get_post_permalink( $post_4114->ID ); ?>"><h3 class="home-block-title"><?php echo $title; ?> </h3></a>
                    <p>
                       <?php echo $content; ?>
                    </p>   
                    <div class="clear"></div>
                    <span> <a href="<?php echo get_post_permalink( $post_4114->ID ); ?>" onclick="ga('send', 'event', 'button', 'click', 'learn more')"> Learn More</a></span>
                  
                <!-- For English use 30-->

                <?php
                   }
                  if($lang_type == 'en_US'){
                  $post_30 = get_post( 30 ); 
                  $title = get_field('home_title', $post_30->ID);
                  $content = get_field('home_content', $post_30->ID);
                  ?>
                    <a href="<?php echo get_post_permalink( $post_30->ID ); ?>">
                        <div class="home-block-img" style="overflow: hidden;">
                           <?php echo get_the_post_thumbnail( $post_30->ID, 'full' ); ?>
                        </div>   
                    </a>
                    <a href="<?php echo get_post_permalink( $post_30->ID ); ?>"><h3 class="home-block-title"><?php echo $title; ?> </h3></a>
                    <p>
                       <?php echo $content; ?>
                    </p>   
                    <div class="clear"></div>
                    <span> <a href="<?php echo get_post_permalink( $post_30->ID ); ?>" onclick="ga('send', 'event', 'button', 'click', 'learn more')"> Learn More</a></span>
                 <?php
                  }
                  ?>
            </li>
            <li>
                <?php
                  $post_157 = get_post( 157 ); 
                  $title = get_field('home_title', $post_157->ID);
                  $content = get_field('home_content', $post_157->ID);
                  ?>
                    <a href="<?php echo get_post_permalink( $post_157->ID ); ?>">
                        <div class="home-block-img" style="overflow: hidden;">
                           <?php echo get_the_post_thumbnail( $post_157->ID, 'full' ); ?> 
                        </div>
                    </a>
                    <a href="<?php echo get_post_permalink( $post_157->ID ); ?>"><h3 class="home-block-title"> <?php echo $title; ?> </h3></a>
                    <p>
                        <?php echo $content; ?>
                    </p>
                    <div class="clear"></div>
                    <span> <a href="<?php echo get_post_permalink( $post_157->ID ); ?>" onclick="ga('send', 'event', 'button', 'click', 'learn more')"> Learn More</a></span>
            </li>
            <li>
                <?php
                  $post_35 = get_post( 35 ); 
                  $title = get_field('home_title', $post_35->ID);
                  $content = get_field('home_content', $post_35->ID);
                  ?>
                    <a href="<?php echo get_post_permalink( $post_35->ID ); ?>">
                        <div class="home-block-img" style="overflow: hidden;">
                            <?php echo get_the_post_thumbnail( $post_35->ID, 'full' ); ?>
                        </div>    
                    </a>
                    <a href="<?php echo get_post_permalink( $post_35->ID ); ?>"><h3 class="home-block-title">Placements</h3></a>
                    <p>
                        <?php echo $content; ?>
                    </p>
                    <span> <a href="<?php echo get_post_permalink( $post_35->ID ); ?>" onclick="ga('send', 'event', 'button', 'click', 'learn more')"> Learn More</a></span>
            </li>
            <li>
                <!-- News slider -->
                <div class="holder">
                  <h3><a class="home-block-title" href="/category/news/">News and Updates</a></h3>
                  <ul id="ticker01">
                    <?php 
                      $args = array( 'posts_per_page' => -1, 'category' => 8 );
                      $myposts = get_posts( $args );
                      foreach ( $myposts as $post ) : 
                    ?>
                    <li>
                      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                      <p class="rNewsDate"><?php echo get_the_date('F j, Y'); ?></p>
                    </li>
                   <?php endforeach; 
                   wp_reset_postdata(); ?>
                  </ul>
               </div>
               <!-- News slider ends -->

                <?php
                  // $post_287 = get_post( 287 ); 
                  // $title = get_field('home_title', $post_287->ID);
                  // $content = get_field('home_content', $post_287->ID);
                  ?>
                    <!-- <a href="<?php //echo get_post_permalink( $post_287->ID ); ?>">
                        <div class="home-block-img" style="overflow: hidden;">
                            <?php //echo get_the_post_thumbnail( $post_287->ID, 'full' ); ?>
                        </div>    
                    </a>
                    <a href="<?php //echo get_post_permalink( $post_287->ID ); ?>"><h3 class="home-block-title"><?php echo $title; ?> </h3></a>
                    <p>
                        <?php //echo $content; ?>
                    </p>
                    <span> <a href="<?php //echo get_post_permalink( $post_287->ID ); ?>" onclick="ga('send', 'event', 'button', 'click', 'learn more')"> Learn More</a></span> -->
            </li>
        </ul>
    </div>
</div>
<div class="newletter-bg">
    <div class="large-3 medium-3 small-12 columns">
        <div class="downHeading">
            <a href="<?php the_field('link1'); ?>" target="_black"> <img src="<?php the_field('link1_image'); ?>" onclick="ga('send', 'event', 'button', 'click', 'footer')">  </a>
        </div>
    </div>
    <div class="large-3 medium-3 small-12 columns">
        <div class="downHeading">
            <a href="<?php the_field('link2'); ?>" target="_black"> <img src="<?php the_field('link2_image'); ?>" onclick="ga('send', 'event', 'button', 'click', 'footer')"> </a>
        </div>
    </div>
    <div class="large-3 medium-3 small-12 columns">
        <div class="downHeading">
            <a href="<?php the_field('link3'); ?>"> <img src="<?php the_field('link3_image'); ?>" onclick="ga('send', 'event', 'button', 'click', 'footer')"> </a>
        </div>
    </div>
    <div class="large-3 medium-3 small-12 columns">
        <div class="downHeading">
            <a href="<?php the_field('link4'); ?>"> <img src="<?php the_field('link4_image'); ?>" onclick="ga('send', 'event', 'button', 'click', 'footer')"> </a>
        </div>
    </div>
</div>


<?php  if($lang_type == 'en_US'){ ?>

<div class="large-12 medium-12 small-12 columns">
	<div class="SubinnpageText def-page-text home-about">
      <input type="checkbox" class="read-more-state" id="post-1" /> 
	    <h3 class="home-about-title"><strong>About IICD</strong></h3>
	    <p class="read-more-wrap">Indian Institute of Crafts & Design, Jaipur, is one of the leading crafts and design colleges in India that work towards the evolution of crafts and the artisans in the contemporary socio-economic context. IICD is uniquely positioned and offers undergraduate and postgraduate degree programmes in the field of crafts & design. These programmes provide student with the means to grow as sensitive, creative designers and practitioners of craft with clear goals to contribute towards Indian culture and society as a global citizen.
	    <span class="read-more-target">
	        One of the top-notch crafts and design colleges in India, IICD teaches with a unique pedagogy that synergizes traditional knowledge and skills with contemporary methodologies. IICD offers excellent infrastructural facilities for pursuing creative endeavor, with a beautiful campus located in the ‘craft city’ of India, Jaipur.
          Branding IICD as one of the leading design colleges in India won't be untrue for it provides an experiential learning environment with strong focus on conceptual and practical experience. IICD is one such design institute in India that has an academic culture enriched with competent faculty, experts, practitioners and student community from various parts of the world. The Institute also has a close collaboration with a range of stakeholders in craft sector which helps students gain a greater exposure of the sector.
          As a leading crafts and design colleges in India, IICD institution has close interaction and collaboration with leading crafts & design institutes and organizations that help prepare its students to meet the growing challenges at macro and micro level. IICD, apart from imparting quality education, endeavors to  grooming the personality of its students. The same help them have bright career prospects in the crafts and design industry.
          </span>
	      </p>
        <label for="post-1" class="read-more-trigger"></label>
	    </div>
	</div>
	<div class="large-12 medium-12 small-12 columns">
      <div class="innpageText def-page-text inxtittaghead">
        <h1 class="home-about-title">Designing Courses in India</h1>
        <div class="SubinnpageText def-page-text">
			<p>There is a majority of people who are passionate about designing and wish to pursue a career in same. To turn their passion into a profession, Indian Institute of Crafts &amp; Design, Jaipur (IICD) offers some of the best designing courses in India. Whether an individual aims for a degree in <a href="http://www.iicd.ac.in/fashion-design-fd/">fashion designing</a> or wishes to pursue crafts and design courses, IICD has all the necessary facilities and amenities for the same. IICD’s courses are broadly classified into:</p>
			<ul>
			<li><a href="https://www.iicd.ac.in/under-graduate-design/" target="_blank">Undergraduate Courses</a></li>
			<li><a href="https://www.iicd.ac.in/post-graduate-design/" target="_blank">Postgraduate Courses</a></li>
			</ul>
			<p>The above-mentioned categories offer some of the popular designing courses in India which include courses on:</p>
			<ul>
			<li>Hard materials such as wood, metal, and stone</li>
			<li>Soft materials which involve textile, leather, paper and natural fiber</li>
			<li>Fired materials such as Earthenware, Stoneware, Terracotta, Porcelain</li>
			<li>Fashion Design (UG Only)</li>
			</ul>
			<p>All the aforementioned courses are offered under the guidance of specialist technicians and leading practitioners in integrated workshops and dedicated studios. All these workshops and studios are equipped with state-of-the-art machinery and equipment.</p>
			<p>Branding the courses offered by IICD in India as the best won’t be untrue for they are backed by the combination of self-directed study as well as professional projects in the classroom. Above all, these courses provide opportunities for collaborative and interdisciplinary work in the industry to their students.</p>
			<h2>Degree in Crafts and Design</h2>
			<p>IICD was setup to realize the potential of the craft sector and foster innovation and creative thinking for well-crafted quality products. With a degree in crafts and design, IICD aims to develop high-quality professionals to act as catalysts of change in the crafts sector. IICD is committed to the inclusion of artisans’ wards in the major education programmes. It also urges the State Government entities, Social Enterprises and Non-Government Organizations to support and sponsor candidates who wish to pursue a degree in crafts and design.</p>
			<p>IICD encourages a large number of individuals to pursue a career in the craft and design industry owing to the bright career prospects and rapid growth rate of the industry.</p>
			<ul>
			<li>230 lakh craftsmen engaged in different craft sectors with an estimated 360 craft clusters in India.</li>
			<li>Handicraft and Handloom sectors together form a Rs 24,300 crore industry contributing Rs. 10,000 crore to India’s export.</li>
			<li>As per the 12th plan working document, the craft industry recorded an earning of Rs. 1.62 lakh crore.</li>
			</ul>
			</div>
      </div>  
    </div>

</div> 
<?php  } if($lang_type == 'hi_IN'){ ?>

<div class="large-12 medium-12 small-12 columns">
  <div class="SubinnpageText def-page-text home-about">
      <input type="checkbox" class="read-more-state" id="post-1" /> 
      <h3 class="home-about-title"><strong>IICD के बारे में</strong></h3>
      <p class="read-more-wrap">भारतीय शिल्प संस्थान & डिजाइन, जयपुर, भारत में अग्रणी शिल्प और डिजाइन कॉलेजों में से एक है जो समकालीन सामाजिक-आर्थिक संदर्भ में शिल्प और कारीगरों के विकास की दिशा में काम करते हैं । IICD अद्वितीय रूप से तैनात है और शिल्प & डिजाइन के क्षेत्र में स्नातक और स्नातकोत्तर डिग्री कार्यक्रम प्रदान करता है । इन कार्यक्रमों में स्पष्ट लक्ष्यों के साथ शिल्प के संवेदनशील, रचनात्मक डिजाइनरों और चिकित्सकों के रूप में विकसित करने के लिए एक वैश्विक नागरिक के रूप में भारतीय संस्कृति और समाज के प्रति योगदान के साधन के साथ छात्र प्रदान करते हैं ।
      <span class="read-more-target">
         भारत में शीर्ष पायदान शिल्प और डिजाइन कॉलेजों में से एक, IICD एक अद्वितीय अध्यापन कि synergizes पारंपरिक ज्ञान और समकालीन तरीके के साथ कौशल के साथ सिखाता है । IICD भारत, जयपुर के ' क्राफ्ट सिटी ' में स्थित एक सुंदर परिसर के साथ रचनात्मक प्रयास को आगे बढ़ाने के लिए उत्कृष्ट ढांचागत सुविधाएं प्रदान करता है ।
          भारत में अग्रणी डिजाइन कॉलेजों में से एक के रूप में ब्रांडिंग IICD यह वैचारिक और व्यावहारिक अनुभव पर मजबूत ध्यान केंद्रित के साथ एक अनुभव सीखने वातावरण प्रदान करता है के लिए असत्य नहीं होगा । IICD भारत में एक ऐसी डिजाइन संस्थान है कि एक अकादमिक संस्कृति सक्षम संकाय, विशेषज्ञों, चिकित्सकों और दुनिया के विभिंन भागों से छात्र समुदाय के साथ समृद्ध है । संस्थान में शिल्प क्षेत्र में हितधारकों की एक सीमा के साथ घनिष्ठ सहयोग भी है जो छात्रों को इस क्षेत्र का अधिक से अधिक जोखिम हासिल करने में मदद करता है ।
          भारत में अग्रणी शिल्प और डिजाइन कॉलेजों के रूप में, IICD संस्था ने अग्रणी शिल्प & डिजाइन संस्थानों और संगठनों के साथ घनिष्ठ संपर्क और सहयोग किया है जो अपने छात्रों को मैक्रो और सूक्ष्म स्तर पर बढ़ती चुनौतियों को पूरा करने के लिए तैयार करने में मदद करते हैं । IICD, बीटेक क्वालिटी एजुकेशन के अलावा अपने छात्रों के व्यक्तित्व को संवारने के लिए प्रयास करते हैं । वही उनकी मदद शिल्प और डिजाइन उद्योग में उज्ज्वल कैरियर की संभावनाओं है ।
          </span>
        </p>
        <label for="post-1" class="read-more-trigger"></label>
      </div>
  </div>
  <div class="large-12 medium-12 small-12 columns">
      <div class="innpageText def-page-text inxtittaghead">
        <h1 class="home-about-title">भारत में पाठ्यक्रम डिजाइनिंग</h1>
        <div class="SubinnpageText def-page-text">
      <p>वहां लोग हैं, जो डिजाइन के बारे में भावुक है और एक ही में एक कैरियर को आगे बढ़ाने की इच्छा के बहुमत है । अपने जुनून को पेशे में तब्दील करने के लिए, इंडियन इंस्टिट्यूट ऑफ क्राफ्ट एंड डिजाइन, जयपुर (IICD) भारत में कुछ बेहतरीन डिजाइनिंग कोर्स प्रदान करता है । चाहे एक व्यक्ति में एक डिग्री के लिए करना है <a href="http://www.iicd.ac.in/fashion-design-fd/">फैशन डिजाइनिंग</a>  या शिल्प और डिजाइन पाठ्यक्रमों को आगे बढ़ाने के लिए इच्छाओं, IICD सभी आवश्यक सुविधाओं और सुविधाओं के लिए एक ही है । IICD के पाठ्यक्रमों में मोटे तौर पर वर्गीकृत कर रहे हैं:</p>
      <ul>
      <li><a href="https://www.iicd.ac.in/under-graduate-design/" target="_blank">स्नातक पाठ्यक्रम</a></li>
      <li><a href="https://www.iicd.ac.in/post-graduate-design/" target="_blank">स्नातकोत्तर पाठ्यक्रम</a></li>
      </ul>
      <p>उपर्युक्त श्रेणियां भारत में लोकप्रिय डिजाइनिंग पाठ्यक्रमों में से कुछ प्रदान करती हैं, जिन पर पाठ्यक्रम शामिल हैं:</p>
      <ul>
      <li>लकड़ी, धातु, और पत्थर के रूप में कठिन सामग्री</li>
      <li>नरम सामग्री है जो वस्त्र, चमड़े, कागज और प्राकृतिक फाइबर शामिल</li>
      <li>बरतन, Stoneware, टेराकोटा, चीनी मिट्टी के बरतन जैसे सामग्री निकाली</li>
      <li>फैशन डिजाइन (केवल यूजी)</li>
      </ul>
      <p>सभी aforementioned पाठ्यक्रम विशेषज्ञ तकनीशियनों और एकीकृत कार्यशालाओं और समर्पित स्टूडियो में अग्रणी चिकित्सकों के मार्गदर्शन में की पेशकश कर रहे हैं । इन सभी कार्यशालाओं और स्टूडियो राज्य के अत्याधुनिक मशीनरी और उपकरणों के साथ सुसज्जित हैं ।</p>
      <p>सर्वश्रेष्ठ के रूप में भारत में IICD द्वारा की पेशकश की पाठ्यक्रम ब्रांडिंग वे स्वयं के संयोजन के द्वारा समर्थित है के लिए सही नहीं होगा-निर्देशित अध्ययन के रूप में के रूप में अच्छी तरह से कक्षा में व्यावसायिक परियोजनाओं । इन सबसे ऊपर, इन पाठ्यक्रमों अपने छात्रों को उद्योग में सहयोगात्मक और अनुशासनात्मक काम के लिए अवसर प्रदान करते हैं ।</p>
      <h2>शिल्प और डिजाइन में डिग्री</h2>
      <p>IICD के लिए शिल्प क्षेत्र और बढ़ावा नवाचार और अच्छी तरह से तैयार की गुणवत्ता के उत्पादों के लिए रचनात्मक सोच की क्षमता का एहसास सेटअप था । शिल्प और डिजाइन में एक डिग्री के साथ, IICD उच्च गुणवत्ता पेशेवरों विकसित करने के लिए शिल्प क्षेत्र में परिवर्तन के उत्प्रेरक के रूप में कार्य करना है । IICD प्रमुख शिक्षा कार्यक्रमों में शिल्पकारों के वार्डों को शामिल करने के लिए प्रतिबद्ध है । इसके साथ ही राज्य सरकार संस्थाओं, सामाजिक उद्यमों और गैर सरकारी संगठनों से भी आग्रह करती है कि वे ऐसे उम्मीदवारों को समर्थन और प्रायोजक करें जो शिल्प और डिजाइन में डिग्री बढ़ाने की इच्छा रखते हैं ।</p>
      <p>IICD व्यक्तियों की एक बड़ी संख्या को प्रोत्साहित करने के लिए शिल्प और डिजाइन उद्योग उज्ज्वल कैरियर की संभावनाओं और उद्योग की तेजी से विकास दर के कारण में एक कैरियर का पीछा ।</p>
      <ul>
      <li>भारत में अनुमानित ३६० शिल्प समूहों के साथ विभिन्न शिल्प क्षेत्रों में लगे २३० लाख कारीगरों ।</li>
      <li>हस्तशिल्प और हथकरघा क्षेत्र एक साथ २४,३०० करोड़ रु. के उद्योग भारत के निर्यात के लिए १०,००० करोड़ रुपए की कालाबाजारी कर रहे हैं ।</li>
      <li>12 वीं योजना के काम दस्तावेज के अनुसार शिल्प उद्योग ने १.६२ लाख करोड़ रुपये की कमाई दर्ज की ।</li>
      </ul>
      </div>
      </div>  
    </div>

</div>  
<?php } ?>

<?php get_footer(); ?>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.newsTicker.js"></script>

<script type="text/javascript">
  var multilines = jQuery('ul#ticker01').newsTicker({
    row_height: 70,
    speed: 2000,
    duration: 4000,
    max_rows: 4
  });
</script>