  $(document).ready(function(){
  $(".search").click(function(){
  if(!$(".search-box").hasClass("active") || $(".search-field").val() == ''){
    $(".search-box").addClass('active');$(".search-field").focus();
    $(".clear-icon").click(function(){
      ClearSearchField();
      OnInput();
    });
    return false;
  } else {
    $(".text").html("You were trying to search for: <br/>" + $(".search-field").val());
    ClearSearchField();
    $(".search-box").removeClass('active');
    $(".clear-icon").removeClass("show");
   OnInput();
  }
});
$("html").click(function(e){
  if(!$(e.target).is('.search-field, .clear-icon, .search'))
  {
    $(".search-field").focus();
    ClearSearchField();
    OnInput();
    $(".search-box").removeClass('active');
  }
});
});
function OnInput() {
  if(!$(".search-field").val() == '') {
    $(".clear-icon").addClass("show");
  } else {
    $(".clear-icon").removeClass("show");
  }
}
function ClearSearchField() {
  $(".search-field").val('');
  $(".search-field").focus();
}
