jQuery(document).ready(function() {
  var owl = jQuery('#TopSlider');
  owl.owlCarousel({
    items: 1,
    loop: true,
    smartSpeed: 2000,
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,
    navSpeed: 2000,
    autoHeight: true,
    nav: true,
    navText: [
      "<i class='fa fa-chevron-left fa-1x'></i>",
      "<i class='fa fa-chevron-right fa-1x'></i>"
    ],
  });

  jQuery("#slideshow > div:gt(0)").hide();

  setInterval(function() {
    jQuery('#slideshow > div:first')
      .fadeOut(1000)
      .next().delay(800)
      .fadeIn(1000)
      .end()
      .appendTo('#slideshow');
  }, 3000);

  var owl = jQuery('#InnPageTopSlider');
  owl.owlCarousel({
    items: 1,
    loop: true,
    smartSpeed: 2000,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    navSpeed: 2000,
    nav: true,
    navText: [
      "<i class='fa fa-chevron-left fa-1x'></i>",
      "<i class='fa fa-chevron-right fa-1x'></i>"
    ],
  });
  jQuery('.popup-gallery').magnificPopup({
    delegate: 'a',
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
    },
    image: {
      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
      titleSrc: function(item) {
        return item.el.attr('title') + '';
      }
    }
  });
  jQuery('.campusLife').owlCarousel({
    loop: true,
    margin: 10,
    item: 1,
    nav: true,
    navText: [
      "<i class='fa fa-angle-left fa-3x'></i>",
      "<i class='fa fa-angle-right fa-3x'></i>"
    ],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 1
      }
    }
  });
  var owl = jQuery('#CampusFaci');
  owl.owlCarousel({
    items: 1,
    loop: true,
    smartSpeed: 2000,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    navSpeed: 2000,
    nav: true,
    navText: [
      "<i class='fa fa-chevron-left fa-1x'></i>",
      "<i class='fa fa-chevron-right fa-1x'></i>"
    ],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 5
      }
    }
  });
  jQuery('.toggleOnTabClick').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: [
      "<i class='fa fa-angle-left fa-3x'></i>",
      "<i class='fa fa-angle-right fa-3x'></i>"
    ],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }
  });





  // New Class
  jQuery('#projectTabs').on('toggled', function(event, tab) {
    event.preventDefault();
    console.log('dinesh');
    jQuery('.toggleOnTabClick').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
    jQuery('.toggleOnTabClick').find('.owl-stage-outer').children().unwrap();
    // $('.toggleOnTabClick').data('owlCarousel').destroy();
    // $('.toggleOnTabClick').data('owlCarousel').initialize();
    jQuery('.toggleOnTabClick').owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      navText: [
        "<i class='fa fa-angle-left fa-3x'></i>",
        "<i class='fa fa-angle-right fa-3x'></i>"
      ],
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 3
        },
        1000: {
          items: 4
        }
      }
    });
  });


});

function refreshOwl() {

  // Projects Held Slider 
  jQuery('#projects').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
  jQuery('#projects').find('.owl-stage-outer').children().unwrap();
  jQuery('#projects').owlCarousel({

    loop: true,
    margin: 10,
    nav: true,
    navText: [
      "<i class='fa fa-angle-left fa-3x'></i>",
      "<i class='fa fa-angle-right fa-3x'></i>"
    ],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }
  });

  jQuery('#projects .owl-stage-outer  .owl-item').find('p:first').parent().hide();
  jQuery('#projects .owl-stage-outer  .owl-item').find('a').show();



}

function refreshOwl2() {
  // Heritage 1 slider

  jQuery('#heritage-1').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
  jQuery('#heritage-1').find('.owl-stage-outer').children().unwrap();
  jQuery('#heritage-1').owlCarousel({

    loop: true,
    margin: 10,
    nav: true,
    navText: [
      "<i class='fa fa-angle-left fa-3x'></i>",
      "<i class='fa fa-angle-right fa-3x'></i>"
    ],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }
  });

  jQuery('#heritage-1 .owl-stage-outer  .owl-item').find('p:first').parent().hide();
  jQuery('#heritage-1 .owl-stage-outer  .owl-item ').find('a').show();


  // Heritage 2 slider

  jQuery('#heritage-2').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
  jQuery('#heritage-2').find('.owl-stage-outer').children().unwrap();
  jQuery('#heritage-2').owlCarousel({

    loop: true,
    margin: 10,
    nav: true,
    navText: [
      "<i class='fa fa-angle-left fa-3x'></i>",
      "<i class='fa fa-angle-right fa-3x'></i>"
    ],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }
  });

  jQuery('#heritage-2 .owl-stage-outer  .owl-item').find('p:first').parent().hide();
  jQuery('#heritage-2 .owl-stage-outer  .owl-item ').find('a').show();

  // training  slider

  jQuery('#training').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
  jQuery('#training').find('.owl-stage-outer').children().unwrap();
  jQuery('#training').owlCarousel({

    loop: true,
    margin: 10,
    nav: true,
    
    navText: [
      "<i class='fa fa-angle-left fa-3x'></i>",
      "<i class='fa fa-angle-right fa-3x'></i>"
    ],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 5
      }
    }
  });

  jQuery('#training .owl-stage-outer  .owl-item').find('p:first').parent().hide();
  jQuery('#training .owl-stage-outer  .owl-item ').find('a').show();

  // Design 

  jQuery('#design').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
  jQuery('#design').find('.owl-stage-outer').children().unwrap();
  jQuery('#design').owlCarousel({

    loop: true,
    margin: 10,
    nav: true,
    navText: [
      "<i class='fa fa-angle-left fa-3x'></i>",
      "<i class='fa fa-angle-right fa-3x'></i>"
    ],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 6
      }
    }
  });

  jQuery('#design .owl-stage-outer  .owl-item').find('p:first').parent().hide();
  jQuery('#design .owl-stage-outer  .owl-item ').find('a').show();
}

jQuery(document).ready(function() {

  jQuery("#heritage-1").owlCarousel();

  jQuery("#heritage-2").owlCarousel();

  jQuery("#training").owlCarousel();

  jQuery("#design").owlCarousel();

  jQuery("#projects").owlCarousel();

  jQuery("#craft").owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: [
      "<i class='fa fa-angle-left fa-3x'></i>",
      "<i class='fa fa-angle-right fa-3x'></i>"
    ],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }

  });

  jQuery("#FMA").owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: [
      "<i class='fa fa-angle-left fa-3x'></i>",
      "<i class='fa fa-angle-right fa-3x'></i>"
    ],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }

  });




});
