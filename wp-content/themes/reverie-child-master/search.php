<?php get_header(); ?>

<!-- Row for main content area -->
<div class="searched-content">
	<div class="small-12 large-12 columns" id="content" role="main">
	
		<h2 style="color: #c92351;"><?php _e('Search Results for', 'reverie'); ?> "<?php echo get_search_query(); ?>"</h2>
	
	<?php if ( have_posts() ) : ?>
	
		<?php /* Start the Loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', get_post_format() ); ?>
		<?php endwhile; ?>
		
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		
	<?php endif; // end have_posts() check ?>
	
</div>
	</div>
		
<?php get_footer(); ?>