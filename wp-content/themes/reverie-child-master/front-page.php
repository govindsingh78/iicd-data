<?php get_header();?>


<?php 
$lang_type = get_locale();  
if($lang_type == 'hi_IN'){
get_template_part("front-page-1");
}else{
  get_template_part("front-page-2");
}
?>

<?php get_footer(); ?>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.newsTicker.js"></script>

<script type="text/javascript">
  var multilines = jQuery('ul#ticker01').newsTicker({
    row_height: 70,
    speed: 2000,
    duration: 4000,
    max_rows: 4
  });
</script>