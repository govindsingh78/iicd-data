
<div class="large-12 medium-12 small-12 columns">
    <div class="outService">
        <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-4" data-equalizer-watch>
            <li>

                <!-- For Hindi use 4114-->

                <?php
                  
                  $post_4114 = get_post( 4114 ); 
                  $title = get_field('home_title', $post_4114->ID);
                  $content = get_field('home_content', $post_4114->ID);
                  ?>
                    <a href="<?php echo get_post_permalink( $post_30->ID ); ?>">
                        <div class="home-block-img" style="overflow: hidden;">
                           <?php echo get_the_post_thumbnail( $post_4114->ID, 'full' ); ?>
                        </div>   
                    </a>
                    <a href="<?php echo get_post_permalink( $post_4114->ID ); ?>"><h3 class="home-block-title"><?php echo $title; ?> </h3></a>
                    <p>
                       <?php echo $content; ?>
                    </p>   
                    <div class="clear"></div>
                    <span> <a href="<?php echo get_post_permalink( $post_4114->ID ); ?>" onclick="ga('send', 'event', 'button', 'click', 'learn more')"> और जानो</a></span>
                  
               
               
            </li>
            <li>
                <?php
                // 4248
                  $post_157 = get_post( 4089 ); 
                  $title = get_field('home_title', $post_157->ID);
                  $content = get_field('home_content', $post_157->ID);
                  ?>
                    <a href="<?php echo get_post_permalink( $post_157->ID ); ?>">
                        <div class="home-block-img" style="overflow: hidden;">
                           <?php echo get_the_post_thumbnail( $post_157->ID, 'full' ); ?> 
                        </div>
                    </a>
                    <a href="<?php echo get_post_permalink( $post_157->ID ); ?>"><h3 class="home-block-title"> <?php echo $title; ?> </h3></a>
                    <p>
                        <?php echo $content; ?>
                    </p>
                    <div class="clear"></div>
                    <span> <a href="<?php echo get_post_permalink( $post_157->ID ); ?>" onclick="ga('send', 'event', 'button', 'click', 'learn more')"> और जानो</a></span>
            </li>
            <li>
                <?php
                  $post_35 = get_post( 4212 ); 
                  $title = get_field('home_title', $post_35->ID);
                  $content = get_field('home_content', $post_35->ID);
                  ?>
                    <a href="<?php echo get_post_permalink( $post_35->ID ); ?>">
                        <div class="home-block-img" style="overflow: hidden;">
                            <?php echo get_the_post_thumbnail( $post_35->ID, 'full' ); ?>
                        </div>    
                    </a>
                    <a href="<?php echo get_post_permalink( $post_35->ID ); ?>"><h3 class="home-block-title">Placements</h3></a>
                    <p>
                        <?php echo $content; ?>
                    </p>
                    <span> <a href="<?php echo get_post_permalink( $post_35->ID ); ?>" onclick="ga('send', 'event', 'button', 'click', 'learn more')"> और जानो</a></span>
            </li>
            <li>
                <!-- News slider -->
                <div class="holder">
                  <h3><a class="home-block-title" href="/category/news/">समाचार और अपडेट</a></h3>
                  <ul id="ticker01">
                    <?php 
                      $args = array( 'posts_per_page' => -1, 'category' => 8 );
                      $myposts = get_posts( $args );
                      foreach ( $myposts as $post ) : 
                    ?>
                    <li>
                      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                      <p class="rNewsDate"><?php echo get_the_date('F j, Y'); ?></p>
                    </li>
                   <?php endforeach; 
                   wp_reset_postdata(); ?>
                  </ul>
               </div>
               <!-- News slider ends -->

                <?php
                  // $post_287 = get_post( 287 ); 
                  // $title = get_field('home_title', $post_287->ID);
                  // $content = get_field('home_content', $post_287->ID);
                  ?>
                    <!-- <a href="<?php //echo get_post_permalink( $post_287->ID ); ?>">
                        <div class="home-block-img" style="overflow: hidden;">
                            <?php //echo get_the_post_thumbnail( $post_287->ID, 'full' ); ?>
                        </div>    
                    </a>
                    <a href="<?php //echo get_post_permalink( $post_287->ID ); ?>"><h3 class="home-block-title"><?php echo $title; ?> </h3></a>
                    <p>
                        <?php //echo $content; ?>
                    </p>
                    <span> <a href="<?php //echo get_post_permalink( $post_287->ID ); ?>" onclick="ga('send', 'event', 'button', 'click', 'learn more')"> Learn More</a></span> -->
            </li>
        </ul>
    </div>
</div>
<div class="newletter-bg">
    <div class="large-3 medium-3 small-12 columns">
        <div class="downHeading">
            <a href="<?php the_field('link1'); ?>" target="_black"> <img src="<?php the_field('link1_image'); ?>" onclick="ga('send', 'event', 'button', 'click', 'footer')">  </a>
        </div>
    </div>
    <div class="large-3 medium-3 small-12 columns">
        <div class="downHeading">
            <a href="<?php the_field('link2'); ?>" target="_black"> <img src="<?php the_field('link2_image'); ?>" onclick="ga('send', 'event', 'button', 'click', 'footer')"> </a>
        </div>
    </div>
    <div class="large-3 medium-3 small-12 columns">
        <div class="downHeading">
            <a href="<?php the_field('link3'); ?>"> <img src="<?php the_field('link3_image'); ?>" onclick="ga('send', 'event', 'button', 'click', 'footer')"> </a>
        </div>
    </div>
    <div class="large-3 medium-3 small-12 columns">
        <div class="downHeading">
            <a href="<?php the_field('link4'); ?>"> <img src="<?php the_field('link4_image'); ?>" onclick="ga('send', 'event', 'button', 'click', 'footer')"> </a>
        </div>
    </div>
</div>
 

<div class="large-12 medium-12 small-12 columns">
  <div class="SubinnpageText def-page-text home-about">
      <input type="checkbox" class="read-more-state" id="post-1" /> 
      <h3 class="home-about-title"><strong>IICD के बारे में</strong></h3>
      <p class="read-more-wrap">भारतीय शिल्प संस्थान & डिजाइन, जयपुर, भारत में अग्रणी शिल्प और डिजाइन कॉलेजों में से एक है जो समकालीन सामाजिक-आर्थिक संदर्भ में शिल्प और कारीगरों के विकास की दिशा में काम करते हैं । IICD अद्वितीय रूप से तैनात है और शिल्प & डिजाइन के क्षेत्र में स्नातक और स्नातकोत्तर डिग्री कार्यक्रम प्रदान करता है । इन कार्यक्रमों में स्पष्ट लक्ष्यों के साथ शिल्प के संवेदनशील, रचनात्मक डिजाइनरों और चिकित्सकों के रूप में विकसित करने के लिए एक वैश्विक नागरिक के रूप में भारतीय संस्कृति और समाज के प्रति योगदान के साधन के साथ छात्र प्रदान करते हैं ।
      <span class="read-more-target">
         भारत में शीर्ष पायदान शिल्प और डिजाइन कॉलेजों में से एक, IICD एक अद्वितीय अध्यापन कि synergizes पारंपरिक ज्ञान और समकालीन तरीके के साथ कौशल के साथ सिखाता है । IICD भारत, जयपुर के ' क्राफ्ट सिटी ' में स्थित एक सुंदर परिसर के साथ रचनात्मक प्रयास को आगे बढ़ाने के लिए उत्कृष्ट ढांचागत सुविधाएं प्रदान करता है ।
          भारत में अग्रणी डिजाइन कॉलेजों में से एक के रूप में ब्रांडिंग IICD यह वैचारिक और व्यावहारिक अनुभव पर मजबूत ध्यान केंद्रित के साथ एक अनुभव सीखने वातावरण प्रदान करता है के लिए असत्य नहीं होगा । IICD भारत में एक ऐसी डिजाइन संस्थान है कि एक अकादमिक संस्कृति सक्षम संकाय, विशेषज्ञों, चिकित्सकों और दुनिया के विभिंन भागों से छात्र समुदाय के साथ समृद्ध है । संस्थान में शिल्प क्षेत्र में हितधारकों की एक सीमा के साथ घनिष्ठ सहयोग भी है जो छात्रों को इस क्षेत्र का अधिक से अधिक जोखिम हासिल करने में मदद करता है ।
          भारत में अग्रणी शिल्प और डिजाइन कॉलेजों के रूप में, IICD संस्था ने अग्रणी शिल्प & डिजाइन संस्थानों और संगठनों के साथ घनिष्ठ संपर्क और सहयोग किया है जो अपने छात्रों को मैक्रो और सूक्ष्म स्तर पर बढ़ती चुनौतियों को पूरा करने के लिए तैयार करने में मदद करते हैं । IICD, बीटेक क्वालिटी एजुकेशन के अलावा अपने छात्रों के व्यक्तित्व को संवारने के लिए प्रयास करते हैं । वही उनकी मदद शिल्प और डिजाइन उद्योग में उज्ज्वल कैरियर की संभावनाओं है ।
          </span>
        </p>
        <label for="post-1" class="read-more-trigger"></label>
      </div>
  </div>
  <div class="large-12 medium-12 small-12 columns">
      <div class="innpageText def-page-text inxtittaghead">
        <h1 class="home-about-title">भारत में पाठ्यक्रम डिजाइनिंग</h1>
        <div class="SubinnpageText def-page-text">
      <p>वहां लोग हैं, जो डिजाइन के बारे में भावुक है और एक ही में एक कैरियर को आगे बढ़ाने की इच्छा के बहुमत है । अपने जुनून को पेशे में तब्दील करने के लिए, इंडियन इंस्टिट्यूट ऑफ क्राफ्ट एंड डिजाइन, जयपुर (IICD) भारत में कुछ बेहतरीन डिजाइनिंग कोर्स प्रदान करता है । चाहे एक व्यक्ति में एक डिग्री के लिए करना है <a href="http://www.iicd.ac.in/fashion-design-fd/">फैशन डिजाइनिंग</a>  या शिल्प और डिजाइन पाठ्यक्रमों को आगे बढ़ाने के लिए इच्छाओं, IICD सभी आवश्यक सुविधाओं और सुविधाओं के लिए एक ही है । IICD के पाठ्यक्रमों में मोटे तौर पर वर्गीकृत कर रहे हैं:</p>
      <ul>
      <li><a href="https://www.iicd.ac.in/under-graduate-design/" target="_blank">स्नातक पाठ्यक्रम</a></li>
      <li><a href="https://www.iicd.ac.in/post-graduate-design/" target="_blank">स्नातकोत्तर पाठ्यक्रम</a></li>
      </ul>
      <p>उपर्युक्त श्रेणियां भारत में लोकप्रिय डिजाइनिंग पाठ्यक्रमों में से कुछ प्रदान करती हैं, जिन पर पाठ्यक्रम शामिल हैं:</p>
      <ul>
      <li>लकड़ी, धातु, और पत्थर के रूप में कठिन सामग्री</li>
      <li>नरम सामग्री है जो वस्त्र, चमड़े, कागज और प्राकृतिक फाइबर शामिल</li>
      <li>बरतन, Stoneware, टेराकोटा, चीनी मिट्टी के बरतन जैसे सामग्री निकाली</li>
      <li>फैशन डिजाइन (केवल यूजी)</li>
      </ul>
      <p>सभी aforementioned पाठ्यक्रम विशेषज्ञ तकनीशियनों और एकीकृत कार्यशालाओं और समर्पित स्टूडियो में अग्रणी चिकित्सकों के मार्गदर्शन में की पेशकश कर रहे हैं । इन सभी कार्यशालाओं और स्टूडियो राज्य के अत्याधुनिक मशीनरी और उपकरणों के साथ सुसज्जित हैं ।</p>
      <p>सर्वश्रेष्ठ के रूप में भारत में IICD द्वारा की पेशकश की पाठ्यक्रम ब्रांडिंग वे स्वयं के संयोजन के द्वारा समर्थित है के लिए सही नहीं होगा-निर्देशित अध्ययन के रूप में के रूप में अच्छी तरह से कक्षा में व्यावसायिक परियोजनाओं । इन सबसे ऊपर, इन पाठ्यक्रमों अपने छात्रों को उद्योग में सहयोगात्मक और अनुशासनात्मक काम के लिए अवसर प्रदान करते हैं ।</p>
      <h2>शिल्प और डिजाइन में डिग्री</h2>
      <p>IICD के लिए शिल्प क्षेत्र और बढ़ावा नवाचार और अच्छी तरह से तैयार की गुणवत्ता के उत्पादों के लिए रचनात्मक सोच की क्षमता का एहसास सेटअप था । शिल्प और डिजाइन में एक डिग्री के साथ, IICD उच्च गुणवत्ता पेशेवरों विकसित करने के लिए शिल्प क्षेत्र में परिवर्तन के उत्प्रेरक के रूप में कार्य करना है । IICD प्रमुख शिक्षा कार्यक्रमों में शिल्पकारों के वार्डों को शामिल करने के लिए प्रतिबद्ध है । इसके साथ ही राज्य सरकार संस्थाओं, सामाजिक उद्यमों और गैर सरकारी संगठनों से भी आग्रह करती है कि वे ऐसे उम्मीदवारों को समर्थन और प्रायोजक करें जो शिल्प और डिजाइन में डिग्री बढ़ाने की इच्छा रखते हैं ।</p>
      <p>IICD व्यक्तियों की एक बड़ी संख्या को प्रोत्साहित करने के लिए शिल्प और डिजाइन उद्योग उज्ज्वल कैरियर की संभावनाओं और उद्योग की तेजी से विकास दर के कारण में एक कैरियर का पीछा ।</p>
      <ul>
      <li>भारत में अनुमानित ३६० शिल्प समूहों के साथ विभिन्न शिल्प क्षेत्रों में लगे २३० लाख कारीगरों ।</li>
      <li>हस्तशिल्प और हथकरघा क्षेत्र एक साथ २४,३०० करोड़ रु. के उद्योग भारत के निर्यात के लिए १०,००० करोड़ रुपए की कालाबाजारी कर रहे हैं ।</li>
      <li>12 वीं योजना के काम दस्तावेज के अनुसार शिल्प उद्योग ने १.६२ लाख करोड़ रुपये की कमाई दर्ज की ।</li>
      </ul>
      </div>
      </div>  
    </div>

</div>  
 
