  </div>
</section>




<?php if(get_locale() == 'hi_IN'){?>

<footer>

  <div class="row">
<div class="large-3 medium-3 small-12 columns footer-links">
<span class="footer-title">हमारे बारे में</span>
<ul class="footer-ul">
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/vision-mission/">दूरदर्शिता और मिशन</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/iicd-at-a-glance/">आईआईसीडी एक नज़र में</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/chairpersons-message/">अध्यक्ष का संदेश</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/chairperson-academic-councils-message/">अध्यक्ष अकादमिक परिषद का संदेश</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/directors-message/">निदेशक का संदेश</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/campus/">अवलोकन और कैंपस सुविधाएं</a></li>
</ul>
</div>
<div class="large-3 medium-3 small-12 columns footer-links">
<span class="footer-title">शिक्षाविदों</span>
<ul class="footer-ul">
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/academics-overview/">अवलोकन</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/academic-calendar/">अकादमिक कैलेंडर</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/learning-model/">सीखना मॉडल</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/campus/student-descipline/">छात्र अनुशासन</a></li>
</ul>
</div>
<div class="large-3 medium-3 small-12 columns footer-links">
<span class="footer-title">अधिसूचना</span>
<ul class="footer-ul">
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/fee-notification/">शुल्क अधिसूचना</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/alumni/">पूर्व छात्रों</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/careers/">करियर</a></li>
  <li class="footer-link"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/05/disciplinary-commiitee.pdf" target="_blank" rel="noopener noreferrer">अनुशासनात्मक समिति</a></li>
  <li class="footer-link"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/05/sexual-Harresment-committee.pdf" target="_blank" rel="noopener noreferrer">यौन उत्पीड़न समिति</a></li>
  <li class="footer-link"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/07/Anti-Ragging-Notice.pdf" target="_blank" rel="noopener noreferrer">एंटी रैगिंग कमेटी</a></li>
</ul>
</div>

<div class="large-3 medium-3 small-12 columns footer-links">
<span class="footer-title">संपर्क करें</span>
<ul class="footer-ul">
  <li class="footer-link"><a href="#">जे -8, झलाना इंस्टीट्यूशनल एरिया, जयपुर -302004, राजस्थान। इंडिया</a></li>
  <li class="footer-link"><a href="tel:911412701203" title="">फ़ोन: +91-141-2701203, 2701504, 2700156</a></li>
  <li class="footer-link"><a href="tel:1412700160" title="">फैक्स: +91-141-2700160</a></li>
  <li class="footer-link"><a href="tel:+919460673297" title="">प्रवेश : +91-9460673297</a></li>
  <li class="footer-link"><a href="mailto:info@iicd.ac.in" title="">ईमेल:info@iicd.ac.in</a></li>
</ul>
</div>
    
</div>
<div class="eventsliderfooter">
	<div class="row">
		<div class="large-12 medium-12 small-12 columns">
			<h5>घटना / परियोजना भागीदार</h5>
                    <div id="EventSlider" class="owl-carousel owl-theme">
                        <?php
                    $homesliderArgs = array(
                    'posts_per_page' => -1,
                    'post_type' => 'sliders',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'slider-category',
                            'field' => 'slug',
                            'terms' => 'event-partners-slider',
                            )
                            )
                    );
                    $homesliderPosts = get_posts($homesliderArgs);
                    foreach ($homesliderPosts as $post) : setup_postdata($post);
                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                    $url = $thumb['0']; ?>
                            <div class="item"><img src="<?php echo $url; ?>" class="img-responsive"></div>
                            <?php
                              endforeach;
                              wp_reset_postdata();
                            ?>
                    </div>
                    <script>
                    	var owl = jQuery('#EventSlider');
						owl.owlCarousel({
						    items:8,
						    loop:true,
						    margin:10,
						    autoplay:true,
						    autoplayTimeout:5000
						});
                    </script>
              </div>
      
	</div>
</div>
<div class="footerimageslogo">
	<div class="row">
		<div class="large-6 medium-6 small-12 columns">
      <h5>अकादमिक उत्कृष्टता साथी</h5>
			<ul>
			  <li>
			    <img src="https://www.iicd.ac.in/wp-content/uploads/2018/01/1.png" alt="Adelaide Central School of Art">
			  </li>
			  <li>
			    <img src="https://www.iicd.ac.in/wp-content/uploads/2018/01/2.png" alt="Playmouth">
			  </li>
			  <li>
			    <img src="https://www.iicd.ac.in/wp-content/uploads/2018/01/3.png" alt="École de design">
			  </li>
			  <li>
			    <img src="https://www.iicd.ac.in/wp-content/uploads/2018/01/4.png" alt="startup oasis">
			  </li>
			</ul>
		</div>

    <div class="large-2 medium-2 small-12 columns">
      <h5>के सदस्य </h5>
      <ul>
        <li>
        <a href="http://www.wccapr.org/">        
          <img src="https://www.iicd.ac.in/wp-content/uploads/2018/03/wcc-logo.png" alt="World Crafts Council" style="max-width: 130px;">
        </a>
        </li>
      </ul>
    </div>
    <div class="large-2 medium-2 small-12 columns">
      <h5>के साथ संबद्ध</h5>
      <ul>
        <li>
        <a href="http://rajskills.edu.in/">
          <img src="https://www.iicd.ac.in/wp-content/uploads/2018/03/risu-logo.png" alt="Rajasthan ILD Skill University" style="max-width: 130px;">
        </a>
        </li>
      </ul>
    </div>
    <div class="large-2 medium-3 small-12 columns">
     <h5>द्वारा डिजाइन और विकसित</h5> 
     <ul>
        <li>
          <a href="http://www.sitsl.io/">
          <img src="https://www.iicd.ac.in/wp-content/uploads/2018/02/sitsl-logo.png" alt="SITSL Online Services" style="max-width: 130px;">
          </a>
        </li>
      </ul>
    </div>
    
	</div>
</div>
<div class="row">
    <div class="large-12 meduim-12 small-12 columns">
<?php $copy_text = get_option('footer_copy'); if($copy_text ){ ?>
      <div class="copyright"> <?php echo $copy_text; ?>  </div>
<?php } ?>
    </div>
</div>
</footer>
<?php 
}
if(get_locale() == 'en_US'){
?>
<footer>

  <div class="row">
<div class="large-3 medium-3 small-12 columns footer-links">
<span class="footer-title">About Us</span>
<ul class="footer-ul">
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/vision-mission/">Vision &amp; Mission</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/iicd-at-a-glance/">IICD &ndash; At a Glance</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/chairpersons-message/">Chairperson's Message</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/chairperson-academic-councils-message/">Chairperson Academic Council’s Message</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/directors-message/">Director's Message</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/campus/">Overview &amp; Campus Facilities</a></li>
</ul>
</div>
<div class="large-3 medium-3 small-12 columns footer-links">
<span class="footer-title">Academics</span>
<ul class="footer-ul">
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/academics-overview/">Overview</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/academic-calendar/">Academic Calendar</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/learning-model/">Learning Model</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/campus/student-descipline/">Student Discipline</a></li>
</ul>
</div>
<div class="large-3 medium-3 small-12 columns footer-links">
<span class="footer-title">Notification</span>
<ul class="footer-ul">
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/fee-notification/">Fee Notification</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/alumni/">Alumni</a></li>
  <li class="footer-link"><a href="<?php echo get_site_url() ?>/careers/">Careers</a></li>
  <li class="footer-link"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/05/disciplinary-commiitee.pdf" target="_blank" rel="noopener noreferrer">Disciplinary Committee</a></li>
  <li class="footer-link"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/05/sexual-Harresment-committee.pdf" target="_blank" rel="noopener noreferrer">Sexual Harresment Committee</a></li>
  <li class="footer-link"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/07/Anti-Ragging-Notice.pdf" target="_blank" rel="noopener noreferrer">Anti Ragging Committee</a></li>
</ul>
</div>

<div class="large-3 medium-3 small-12 columns footer-links">
<span class="footer-title">Contact</span>
<ul class="footer-ul">
  <li class="footer-link"><a href="#">J-8, Jhalana Institutional Area, Jaipur-302004, Rajasthan. INDIA</a></li>
  <li class="footer-link"><a href="tel:911412701203" title="">Phone: +91-141-2701203, 2701504, 2700156</a></li>
  <li class="footer-link"><a href="tel:1412700160" title="">Fax: +91-141-2700160</a></li>
  <li class="footer-link"><a href="tel:+919460673297" title="">Admissions : +91-9460673297</a></li>
  <li class="footer-link"><a href="mailto:info@iicd.ac.in" title="">Email:info@iicd.ac.in</a></li>
</ul>
</div>
    
</div>
<div class="eventsliderfooter">
  <div class="row">
    <div class="large-12 medium-12 small-12 columns">
      <h5>Event / Project Partners</h5>
                    <div id="EventSlider" class="owl-carousel owl-theme">
                        <?php
                    $homesliderArgs = array(
                    'posts_per_page' => -1,
                    'post_type' => 'sliders',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'slider-category',
                            'field' => 'slug',
                            'terms' => 'event-partners-slider',
                            )
                            )
                    );
                    $homesliderPosts = get_posts($homesliderArgs);
                    foreach ($homesliderPosts as $post) : setup_postdata($post);
                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                    $url = $thumb['0']; ?>
                            <div class="item"><img src="<?php echo $url; ?>" class="img-responsive"></div>
                            <?php
                              endforeach;
                              wp_reset_postdata();
                            ?>
                    </div>
                    <script>
                      var owl = jQuery('#EventSlider');
            owl.owlCarousel({
                items:8,
                loop:true,
                margin:10,
                autoplay:true,
                autoplayTimeout:5000
            });
                    </script>
              </div>
      
  </div>
</div>
<div class="footerimageslogo">
  <div class="row">
    <div class="large-6 medium-6 small-12 columns">
      <h5>Academic Excellence Partner</h5>
      <ul>
        <li>
          <img src="https://www.iicd.ac.in/wp-content/uploads/2018/01/1.png" alt="Adelaide Central School of Art">
        </li>
        <li>
          <img src="https://www.iicd.ac.in/wp-content/uploads/2018/01/2.png" alt="Playmouth">
        </li>
        <li>
          <img src="https://www.iicd.ac.in/wp-content/uploads/2018/01/3.png" alt="École de design">
        </li>
        <li>
          <img src="https://www.iicd.ac.in/wp-content/uploads/2018/01/4.png" alt="startup oasis">
        </li>
      </ul>
    </div>
   <div class="large-2 medium-2 small-12 columns">
      <h5>Member of</h5>
      <ul>
        <li>
        <a href="http://www.wccapr.org/">        
          <img src="https://www.iicd.ac.in/wp-content/uploads/2018/03/wcc-logo.png" alt="World Crafts Council" style="max-width: 130px;">
        </a>
        </li>
      </ul>
    </div>
    <div class="large-2 medium-2 small-12 columns">
      <h5>Affiliated with</h5>
      <ul>
        <li>
        <a href="http://rajskills.edu.in/">
          <img src="https://www.iicd.ac.in/wp-content/uploads/2018/03/risu-logo.png" alt="Rajasthan ILD Skill University" style="max-width: 130px;">
        </a>
        </li>
      </ul>
    </div>
    <div class="large-2 medium-3 small-12 columns">
     <h5>Designed &amp; Developed by</h5> 
     <ul>
        <li>
          <a href="http://www.sitsl.io/">
          <img src="https://www.iicd.ac.in/wp-content/uploads/2018/02/sitsl-logo.png" alt="SITSL Online Services" style="max-width: 130px;">
          </a>
        </li>
      </ul>
    </div>
  </div>
</div>
<div class="row">
    <div class="large-12 meduim-12 small-12 columns">
<?php $copy_text = get_option('footer_copy'); if($copy_text ){ ?>
      <div class="copyright"> <?php echo $copy_text; ?>  </div>
<?php } ?>
    </div>
</div>
</footer>
<?php
}
?>
<!-- Google Code for Registrations Conversion Page -->
<!--script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 928445375;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "8s_MCKjXnWUQv-fbugM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/928445375/?label=8s_MCKjXnWUQv-fbugM&amp;guid=ON&amp;script=0"/>
</div>
</noscript-->


<!-- Google Code for Remarketing Tag Start -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 928445375;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/928445375/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Google Code for Remarketing Tag End -->
<?php wp_footer(); ?>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-listnav.js"></script>
<script>



jQuery( "#acrdn" ).click(function() {
  jQuery( "#acrdncont" ).slideToggle( "slow" );
});

jQuery( "#acrdn1" ).click(function() {
  jQuery( "#acrdncont1" ).slideToggle( "slow" );
});

jQuery( "#acrdn2" ).click(function() {
  jQuery( "#acrdncont2" ).slideToggle( "slow" );
});

  jQuery(function () {
//jQuery('#demoThree li .title strong').prepend('<div style="display:none;">abcd</div>');

//jQuery('#tabpage_3 #demoThree li p, #tabpage_3 #demoFour li p').prepend('<span style="display:none;">abc</span>');

    jQuery('#demoThree').listnav({
        allText: 'All',
        initLetter: 'A',
        includeNums: false,
        noMatchText: 'Nothing matched your filter, please click another letter.',
        prefixes: ['Ms. ','Mr. ','Dr. ']
    });

    jQuery('#demoTwo').listnav({
        allText: 'All',
        initLetter: 'A',
        includeNums: false,
        noMatchText: 'Nothing matched your filter, please click another letter.',
        prefixes: ['Ms. ','Mr. ','Dr. ']
    });

    jQuery('#demoOne').listnav();
    jQuery('.demoTwo').listnav({
      includeAll: false,
      noMatchText: 'There are no matching entries.'
    });

    
    jQuery('#demoFour').listnav({
      initLetter: 'A',
      includeNums: false,
      isAll: false,
    });
//	$('#demoFour').listnav({
//		cookieName: 'cookie-demo',
//		includeAll: false,
//		onClick: function(letter) {
//			$(".myLastClicked").text(letter.toUpperCase());
//		}
//	});
    jQuery('#demoFive').listnav({
      includeOther: true,
      prefixes: ['a', 'The Complete Works of']
    });
    jQuery('#demoSix').listnav({
      filterSelector: '.last-name',
      includeNums: false,
      removeDisabled: true,
      allText: 'Complete Cast of Star Trek TNG'
    });
    jQuery('.demo a').click(function (e) {
      e.preventDefault();
    });
  });

jQuery(document).ready(function(){
	jQuery('.facultyTab ul.tabs li:first-child, .tabs-content .content:first-child').addClass('active');
  });
    

</script>

<script>
  (function ($) {
    $(document).foundation();
  })(jQuery);
</script>
<script>
  (function ($) {
    $(document).ready(function () {
      $(document).foundation();
      $('#demoThree li .title').on('click', function (e) {
        e.preventDefault();
        $('.divduration').hide();
        $('#demoThree li .title').removeClass('active');
        $(this).addClass('active');
        $(this).parent().children('.divduration').show();
      });
    });
  })(jQuery);
</script>

<script>
jQuery(document).ready(function () {
	jQuery('.readmore').click(function () {
                    if (jQuery(this).text() == 'Read More') {
                      jQuery(this).text('Read Less');
                    } else {
                      jQuery(this).text('Read More')
                    }
                    jQuery(this).prev('.showmore').fadeToggle(800);
                });
});
</script>


<script>
//     jQuery(document).on('ready page:load', function () {
// jQuery(function(){ jQuery(document).foundation(); });
// });
  </script>


<script type="text/javascript">

jQuery(document).ready(function() {
    
     jQuery("#projectongoingopen").hide();
      jQuery("#bestheld").hide();
	  
	  	  
});
  
    jQuery("#projectover").click(function(){
    jQuery("#projectoveropen").toggle('slow');
	 jQuery("#projectongoingopen").hide('slow');
      jQuery("#bestheld").hide('slow');
	
});
     jQuery("#projectongoing").click(function(){
    jQuery("#projectongoingopen").toggle('slow');
	 jQuery("#projectoveropen").hide('slow');
      jQuery("#bestheld").hide('slow');
});


  jQuery("#projectheld").click(function(){
    jQuery("#bestheld").toggle();
	 jQuery("#projectoveropen").hide('slow');
      jQuery("#projectongoingopen").hide('slow');
});

</script>
<script>
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
 console.log("OUT fUNCTION");
function openCity(evt, cityName) {
    console.log("iN fUNCTION");

    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>
</body>
<style type="text/css">
   /* Style the list */
div.tab {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #f1f1f1;
}

/* Float the list items side by side */
div.tab div {float: left;}

/* Style the links inside the list items */
div.tab div a {
    display: inline-block;
    color: black;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of links on hover */
div.tab div a:hover {background-color: #ddd;}

/* Create an active/current tablink class */
div.tab div a:focus, .active {background-color: #ccc;}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border-top: none;
}
.footer-links {
  margin-top: 10px;
}
.footer-title {
  color: #fff;
  font-size: 18px;
  margin-bottom: 2px;
}
.footer-title {
  color: #fff;
  font-size: 20px;
  margin-bottom: 2px;
  border-bottom: 0.5px solid #fff; 
}
ul.footer-ul {
  margin-left: 0px !important;
  list-style: none;
}
ul.footer-ul li.footer-link a{
  color: #fff;
  transform: none;
}
/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border-top: none;
}
.footer-links {
  margin-top: 10px;
}
.footer-title {
  color: #fff;
  font-size: 20px;
  margin-bottom: 2px;
  border-bottom: 0.5px solid #fff; 
}
ul.footer-ul {
  margin-left: 0px !important;
  list-style: none;
}
ul.footer-ul li.footer-link a{
  color: #fff;
  transform: none;
}
</style>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.7/jquery.bxslider.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.7/jquery.bxslider.min.css">

<!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/foundation/5.4.7/js/foundation/foundation.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/foundation/5.4.7/js/foundation/foundation.topbar.min.js'></script>
<script>
  (function ($) {
    $(document).foundation();
  })(jQuery);
</script>
 -->
 <script type="text/javascript">
   $(document).ready(function(){
    jQuery('.bxslider').bxSlider({
  pager:false,
  controls:false,
  auto:true
});


     jQuery('.bxsliderpro').bxSlider({
  pager:false,
  controls:false,
  auto:true,
  minSlides: 3,
  maxSlides: 4,
  slideWidth: 170,
  slideMargin: 10,
  infiniteLoop:true
});

  
});



 </script>
 <style type="text/css"> .bx-wrapper{
    border: 0px solid #fff !important;
    background: transparent !important;
    -webkit-box-shadow: 0 0 0px #ccc !important;
    box-shadow: 0 0 0px #ccc !important;
}</style>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
 jQuery( function() {
    jQuery( ".date_pick" ).datepicker({
      format:'dd/mm/yy',
    	dateFormat: 'dd/mm/yy',
    altField: '#thealtdate',
    altFormat: 'dd/mm/yy'
    });
  } );
  </script>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/obbserv.css">


</html>
