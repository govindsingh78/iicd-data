<?php die(); ?><?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>

<channel>
	<title>IICD</title>
	<atom:link href="http://www.iicd.ac.in/feed/" rel="self" type="application/rss+xml" />
	<link>http://www.iicd.ac.in</link>
	<description>Craft Design Institute</description>
	<lastBuildDate>Thu, 14 Dec 2017 09:53:16 +0000</lastBuildDate>
	<language>en-US</language>
	<sy:updatePeriod>hourly</sy:updatePeriod>
	<sy:updateFrequency>1</sy:updateFrequency>
	
	<item>
		<title>Sixth Convocation Event At IICD</title>
		<link>http://www.iicd.ac.in/2017/11/28/sixth-convocation-event-iicd/</link>
		<comments>http://www.iicd.ac.in/2017/11/28/sixth-convocation-event-iicd/#respond</comments>
		<pubDate>Tue, 28 Nov 2017 10:59:28 +0000</pubDate>
		<dc:creator><![CDATA[iicdadmin]]></dc:creator>
				<category><![CDATA[News]]></category>

		<guid isPermaLink="false">http://www.iicd.ac.in/?p=2829</guid>
		<description><![CDATA[<p>IICD Jaipur conducted its 6th convocation on November 18, 2017. The event was attended by the honorable Chief Minister of Rajasthan, Mrs. Vasundhra Raje Sindhia.</p>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/11/28/sixth-convocation-event-iicd/">Sixth Convocation Event At IICD</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></description>
				<content:encoded><![CDATA[<p>IICD Jaipur conducted its 6th convocation on November 18, 2017. The event was attended by the honorable Chief Minister of Rajasthan, Mrs. Vasundhra Raje Sindhia.</p>
<div class="large-12 medium-12 small-12 columns">
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/CONVOCATION.png"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/CONVOCATION.png" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/CONVOCATION2.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/CONVOCATION2.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/CONVOCATION3.png"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/CONVOCATION3.png" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/CONVOCATION4.png"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/CONVOCATION4.png" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
</div>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/11/28/sixth-convocation-event-iicd/">Sixth Convocation Event At IICD</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></content:encoded>
			<wfw:commentRss>http://www.iicd.ac.in/2017/11/28/sixth-convocation-event-iicd/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>IICD Bags the Award for the Best Craft Institute/Academy of the Year 2017</title>
		<link>http://www.iicd.ac.in/2017/11/23/iicd-bags-award-best-craft-instituteacademy-year-2017/</link>
		<comments>http://www.iicd.ac.in/2017/11/23/iicd-bags-award-best-craft-instituteacademy-year-2017/#respond</comments>
		<pubDate>Thu, 23 Nov 2017 22:50:28 +0000</pubDate>
		<dc:creator><![CDATA[iicdadmin]]></dc:creator>
				<category><![CDATA[News]]></category>

		<guid isPermaLink="false">http://www.iicd.ac.in/?p=2818</guid>
		<description><![CDATA[<p>We, at IICD, are happy to share that the institute has bagged the award for the Craft Institute/Academy of the Year 2017 at the First International Craft Awards 2017 held on 14th Oct 2017. The award was presented to the director of IICD, Dr Toolika Gupta by Smt. Maneka Gandhi, Cabinet Minister for Women &#038; [&#8230;]</p>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/11/23/iicd-bags-award-best-craft-instituteacademy-year-2017/">IICD Bags the Award for the Best Craft Institute/Academy of the Year 2017</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></description>
				<content:encoded><![CDATA[<p>We, at IICD, are happy to share that the institute has bagged the award for the Craft Institute/Academy of the Year 2017 at the First International Craft Awards 2017 held on 14th Oct 2017. </p>
<p>The award was presented to the director of IICD, Dr Toolika Gupta by Smt. Maneka Gandhi, Cabinet Minister for Women &#038; Child Development, Govt. of India.</p>
<div class="large-12 medium-12 small-12 columns">
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/award1.png"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/award1.png" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/award2.png"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/award2.png" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/award3.png"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/award3.png" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/award4.png"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/award4.png" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/award5.png"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/award5.png" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/award6.png"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/award6.png" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/award7.png"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/award7.png" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
</div>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/11/23/iicd-bags-award-best-craft-instituteacademy-year-2017/">IICD Bags the Award for the Best Craft Institute/Academy of the Year 2017</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></content:encoded>
			<wfw:commentRss>http://www.iicd.ac.in/2017/11/23/iicd-bags-award-best-craft-instituteacademy-year-2017/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>MoU between IICD &#038; Australian Government at City Palace</title>
		<link>http://www.iicd.ac.in/2017/11/14/mou-iicd-australian-government-city-palace/</link>
		<comments>http://www.iicd.ac.in/2017/11/14/mou-iicd-australian-government-city-palace/#respond</comments>
		<pubDate>Tue, 14 Nov 2017 07:28:54 +0000</pubDate>
		<dc:creator><![CDATA[iicdadmin]]></dc:creator>
				<category><![CDATA[News]]></category>

		<guid isPermaLink="false">http://www.iicd.ac.in/?p=2798</guid>
		<description><![CDATA[<p>Indian Institute of Crafts and Design, Jaipur(IICD) has signed a MoU with Adelaide Central School of Art, South Australia under the International Linkages programme on 8 November, 2017. The objective of the MOU is to outline the possible ways both parties could develop and carry out collaborative art and craft activities.</p>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/11/14/mou-iicd-australian-government-city-palace/">MoU between IICD &#038; Australian Government at City Palace</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></description>
				<content:encoded><![CDATA[<p>Indian Institute of Crafts and Design, Jaipur(IICD) has signed a MoU with Adelaide Central School of Art, South Australia under the International Linkages programme on 8 November, 2017. The objective of the MOU is to outline the possible ways both parties could develop and carry out collaborative art and craft activities.</p>
<div class="large-12 medium-12 small-12 columns">
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/MoU-0-1.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/MoU-0-1.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a>
</div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/MoU-1.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/MoU-1.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a>
</div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/MoU-2.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/MoU-2.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a>
</div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/MoU-3.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/MoU-3.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a>
</div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/MoU-4.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/MoU-4.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a>
</div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/11/MoU-5.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/11/MoU-5.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
</div>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/11/14/mou-iicd-australian-government-city-palace/">MoU between IICD &#038; Australian Government at City Palace</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></content:encoded>
			<wfw:commentRss>http://www.iicd.ac.in/2017/11/14/mou-iicd-australian-government-city-palace/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>The Rajasthani Pride: Gota Patti</title>
		<link>http://www.iicd.ac.in/2017/11/11/rajasthani-pride-gota-patti/</link>
		<comments>http://www.iicd.ac.in/2017/11/11/rajasthani-pride-gota-patti/#respond</comments>
		<pubDate>Sat, 11 Nov 2017 09:59:47 +0000</pubDate>
		<dc:creator><![CDATA[iicdadmin]]></dc:creator>
				<category><![CDATA[Blogs]]></category>

		<guid isPermaLink="false">http://www.iicd.ac.in/?p=2784</guid>
		<description><![CDATA[<p>Also called “Lappe Ka Kaam”, Gota Patti is one of the most famous arts of the Indian State &#8211; Rajasthan. Lappe Ka Kaam was traditionally made on religious idols, prayer cloths, royal outfits and attire designed for auspicious occasions such as a wedding. Image Courtesy: Pinterest; Pin: Gota Patti Lappe ka Kaam The Gota (or [&#8230;]</p>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/11/11/rajasthani-pride-gota-patti/">The Rajasthani Pride: Gota Patti</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></description>
				<content:encoded><![CDATA[<p><span style="font-weight: 400;">Also called </span><i><span style="font-weight: 400;">“Lappe Ka Kaam”</span></i><span style="font-weight: 400;">, Gota Patti is one of the most famous arts of the Indian State &#8211; Rajasthan. Lappe Ka Kaam was traditionally made on religious idols, prayer cloths, royal outfits and attire designed for auspicious occasions such as a wedding.</span></p>
<p><center><img class="alignnone" src="http://www.iicd.ac.in/wp-content/uploads/2017/11/GottaPatti1-320x427.png" /></center><center><em><span style="font-weight: 400; font-size: 10px;">Image Courtesy: Pinterest; Pin: Gota Patti Lappe ka Kaam</span></em></center></p>
<p><span style="font-weight: 400;">The Gota (or ribbons) were previously made of gold and silver. However, in the recent time, the craftspeople found its alternative in the form of cotton and metal threads. These ribbons are cut and molded into different shapes such as circles, leaves, dots, squares, etc. One the desired shape has been achieved, they are then hemmed on the cloth’s border to enhance its elegance.</span></p>
<p><center><img class="alignnone" src="http://www.iicd.ac.in/wp-content/uploads/2017/11/GottaPatti2-320x218.png" /></center><center><em><span style="font-weight: 400; font-size: 10px;">Image Source: www.indiaemporium.com</span></em></center></p>
<p><span style="font-weight: 400;">To undertake </span><i><span style="font-weight: 400;">“Lappe Ka Kaam”, </span></i><span style="font-weight: 400;">the Gota shapes are glued on the fabric. Further, to strengthen their hold on the cloth, they are then appliqued with stitches. In fact, the modern Gota shapes now come with tapes on their back. These tapes ensure that the shapes stay firm on the fabric once they are pasted.</span></p>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/11/11/rajasthani-pride-gota-patti/">The Rajasthani Pride: Gota Patti</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></content:encoded>
			<wfw:commentRss>http://www.iicd.ac.in/2017/11/11/rajasthani-pride-gota-patti/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>International Conference on Recent Trends and Sustainability in Crafts &#038; Design</title>
		<link>http://www.iicd.ac.in/2017/11/09/international-conference-recent-trends-sustainability-crafts-design-2/</link>
		<comments>http://www.iicd.ac.in/2017/11/09/international-conference-recent-trends-sustainability-crafts-design-2/#respond</comments>
		<pubDate>Thu, 09 Nov 2017 05:05:23 +0000</pubDate>
		<dc:creator><![CDATA[iicdadmin]]></dc:creator>
				<category><![CDATA[News]]></category>

		<guid isPermaLink="false">http://www.iicd.ac.in/?p=2781</guid>
		<description><![CDATA[<p>IICD is the proud host of the International Conference on Recent Trends in Crafts &#38; Design (2017). The conference will take place on 17th-18th November, 2017 to discuss the latest trends in art and design. To know more, visit: http://www.iicd.ac.in/2017/07/28/international-conference-recent-trends-sustainability-crafts-design/ Media Courtesy: Dainik Bhaskar </p>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/11/09/international-conference-recent-trends-sustainability-crafts-design-2/">International Conference on Recent Trends and Sustainability in Crafts &#038; Design</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></description>
				<content:encoded><![CDATA[<p><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif;"><span class="il">IICD</span> is the proud host of the International Conference on Recent Trends in Crafts &amp; Design (2017). The conference will take place on <span class="aBn" tabindex="0" data-term="goog_239364852"><span class="aQJ">17th-18th November, 2017</span></span> to discuss the latest trends in art and design.</span></p>
<p><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif;">To know more, visit: <a href="http://www.iicd.ac.in/2017/07/28/international-conference-recent-trends-sustainability-crafts-design/" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.iicd.ac.in/2017/07/28/international-conference-recent-trends-sustainability-crafts-design/&amp;source=gmail&amp;ust=1510290051564000&amp;usg=AFQjCNGKHcjLb6qBB4DnIyyig7oeHENTyg">http://www.<span class="il">iicd</span>.ac.in/<wbr />2017/07/28/international-<wbr />conference-recent-trends-<wbr />sustainability-crafts-design/</a></span></p>
<figure><img class="alignnone" src="http://www.iicd.ac.in/wp-content/uploads/2017/11/n.png" /></figure>
<p><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif;">Media Courtesy: Dainik Bhaskar </span></p>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/11/09/international-conference-recent-trends-sustainability-crafts-design-2/">International Conference on Recent Trends and Sustainability in Crafts &#038; Design</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></content:encoded>
			<wfw:commentRss>http://www.iicd.ac.in/2017/11/09/international-conference-recent-trends-sustainability-crafts-design-2/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Handloom Haat inaugurated by IICD Director, Dr. Toolika Gupta</title>
		<link>http://www.iicd.ac.in/2017/10/10/handloom-haat-inaugurated-iicd-director-dr-toolika-gupta/</link>
		<comments>http://www.iicd.ac.in/2017/10/10/handloom-haat-inaugurated-iicd-director-dr-toolika-gupta/#respond</comments>
		<pubDate>Tue, 10 Oct 2017 11:07:38 +0000</pubDate>
		<dc:creator><![CDATA[iicdadmin]]></dc:creator>
				<category><![CDATA[News]]></category>

		<guid isPermaLink="false">http://www.iicd.ac.in/?p=2709</guid>
		<description><![CDATA[<p>Organized by the Rajasthan State Handloom Development Corporation, Handloom Haat was inaugurated on October 2, 2017 by the Director of IICD, Dr. Toolika Gupta who was also the chief guest of the event. The event lasted until October 5, 2017. Photo Courtesy: Dainik Bhaskar</p>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/10/10/handloom-haat-inaugurated-iicd-director-dr-toolika-gupta/">Handloom Haat inaugurated by IICD Director, Dr. Toolika Gupta</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></description>
				<content:encoded><![CDATA[<p>Organized by the Rajasthan State Handloom Development Corporation, Handloom Haat was inaugurated on October 2, 2017 by the Director of IICD, Dr. Toolika Gupta who was also the chief guest of the event. The event lasted until October 5, 2017.</p>
<p>Photo Courtesy: Dainik Bhaskar</p>
<div class="trainingforHeri1-img popup-gallery">
<div id="" class="">
<div class=""><a href="http://www.iicd.ac.in/wp-content/uploads/2017/10/news.png" title="Handloom Haat"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/10/news.png"/></p>
<div class="mask" style="position:relative">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
</div>
</div>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/10/10/handloom-haat-inaugurated-iicd-director-dr-toolika-gupta/">Handloom Haat inaugurated by IICD Director, Dr. Toolika Gupta</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></content:encoded>
			<wfw:commentRss>http://www.iicd.ac.in/2017/10/10/handloom-haat-inaugurated-iicd-director-dr-toolika-gupta/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>IICD at VASTRA 2017</title>
		<link>http://www.iicd.ac.in/2017/10/10/iicd-vastra-2017/</link>
		<comments>http://www.iicd.ac.in/2017/10/10/iicd-vastra-2017/#respond</comments>
		<pubDate>Tue, 10 Oct 2017 10:48:00 +0000</pubDate>
		<dc:creator><![CDATA[iicdadmin]]></dc:creator>
				<category><![CDATA[News]]></category>

		<guid isPermaLink="false">http://www.iicd.ac.in/?p=2697</guid>
		<description><![CDATA[<p>The 6th edition of VASTRA i.e. &#8220;VASTRA &#8211; An International Textile and Apparel Fair 2017&#8221; (VASTRA &#8211; 2017) was held during 21st to 24th September, 2017 at Jaipur Exhibition &#38; Convention Centre, Sitapura Industrial Area, Jaipur, India. Rajasthan State Industrial Development and Investment Corporation Ltd. (RIICO) is the organizer and Federation of Indian Chambers of [&#8230;]</p>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/10/10/iicd-vastra-2017/">IICD at VASTRA 2017</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></description>
				<content:encoded><![CDATA[<p>The 6th edition of VASTRA i.e. &#8220;VASTRA &#8211; An International Textile and Apparel Fair 2017&#8221; (VASTRA &#8211; 2017) was held during 21st to 24th September, 2017 at Jaipur Exhibition &amp; Convention Centre, Sitapura Industrial Area, Jaipur, India. Rajasthan State Industrial Development and Investment Corporation Ltd. (RIICO) is the organizer and Federation of Indian Chambers of Commerce and Industry (FICCI) is the co-organizer of the fair.</p>
<p>VASTRA, an all-encompassing trade fair and conference on Textiles and Apparel, presents a fusion of the finest and the latest in textile products – from fiber to fashion, services and technology. Crafted to create business opportunities, it aims at revitalizing existing business ties and forging of new business relations.</p>
<p>Live demos of traditional textile crafts, business fashion shows, concurrent conferences were a few special recurring features. IICD coordinated the live demonstration of the crafts like block printing, aari-tari (aade ka kaam) and patwa thread craft. A stall was also set-up by display of the products designed by our students.</p>
<div class="trainingforHeri1-img popup-gallery">
<div id="" class="owl-carousel toggleOnTabClick">
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra1.jpg" title="Vastra - 2017"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra1.jpg"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra2.jpg" title="Vastra - 2017"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra2.jpg"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra3.jpg" title="Vastra - 2017"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra3.jpg"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra4.jpg" title="Vastra - 2017"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra4.jpg"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra5.jpg" title="Vastra - 2017"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra5.jpg"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra6.jpg" title="Vastra - 2017"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra6.jpg"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra7.jpg" title="Vastra - 2017"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra7.jpg"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra8.jpg" title="Vastra - 2017"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra8.jpg"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra9.jpg" title="Vastra - 2017"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/10/vastra9.jpg"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
</div>
</div>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/10/10/iicd-vastra-2017/">IICD at VASTRA 2017</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></content:encoded>
			<wfw:commentRss>http://www.iicd.ac.in/2017/10/10/iicd-vastra-2017/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Bamboo Workshop of Nine Dot Square at IICD</title>
		<link>http://www.iicd.ac.in/2017/09/21/bamboo-workshop-nine-dot-square-at-iicd/</link>
		<comments>http://www.iicd.ac.in/2017/09/21/bamboo-workshop-nine-dot-square-at-iicd/#respond</comments>
		<pubDate>Thu, 21 Sep 2017 10:44:19 +0000</pubDate>
		<dc:creator><![CDATA[iicdadmin]]></dc:creator>
				<category><![CDATA[News]]></category>

		<guid isPermaLink="false">http://www.iicd.ac.in/?p=2648</guid>
		<description><![CDATA[<p>The sustainable workshop kickstarted at IICD campus with 4 artisans from Agartala and 15 participants. The inaugural session had Ms Meghna Ajit, a NID graduate in conversation with Mr Brijendra, an artisan from Agartala. The workshop team started working headlong to experiment with the material and made a structure with a lot of enthusiasm. The [&#8230;]</p>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/09/21/bamboo-workshop-nine-dot-square-at-iicd/">Bamboo Workshop of Nine Dot Square at IICD</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></description>
				<content:encoded><![CDATA[<div class="SubinnpageText def-page-text">
<ul>
<p>The sustainable workshop kickstarted at IICD campus with 4 artisans from Agartala and 15 participants. The inaugural session had Ms Meghna Ajit, a NID graduate in conversation with Mr Brijendra, an artisan from Agartala. The workshop team started working headlong to experiment with the material and made a structure with a lot of enthusiasm.</p>
<p>The result can be seen at the event at Diggi Palace on 22nd, 23rd, 24th September. Book your dates to view a spectacular event of Crafts &#038; Design.</p>
</div>
<div class="trainingforHeri1-img popup-gallery">
<div id="" class="owl-carousel toggleOnTabClick">
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/09/Bamboo1.png" title="Bamboo Workshop"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/09/Bamboo1.png"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/09/Bamboo2.png" title="Bamboo Workshop"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/09/Bamboo2.png"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/09/Bamboo3.png" title="Bamboo Workshop"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/09/Bamboo3.png"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/09/Bamboo4.png" title="Bamboo Workshop"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/09/Bamboo4.png"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/09/Bamboo5.png" title="Bamboo Workshop"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/09/Bamboo5.png"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/09/Bamboo6.png" title="Bamboo Workshop"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/09/Bamboo6.png"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
<div class="item"><a href="http://www.iicd.ac.in/wp-content/uploads/2017/09/Bamboo7.png" title="Bamboo Workshop"></p>
<div class="view second-effect"><img class="th" src="http://www.iicd.ac.in/wp-content/uploads/2017/09/Bamboo7.png"/></p>
<div class="mask">
<div class="zoom"><i class="fa fa-search"></i> </div>
</div>
</div>
<p></a></div>
</div>
</div>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/09/21/bamboo-workshop-nine-dot-square-at-iicd/">Bamboo Workshop of Nine Dot Square at IICD</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></content:encoded>
			<wfw:commentRss>http://www.iicd.ac.in/2017/09/21/bamboo-workshop-nine-dot-square-at-iicd/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Puppet Making Workshop With Kathputli Artisans at IICD</title>
		<link>http://www.iicd.ac.in/2017/08/31/puppet-making-workshop-with-kathputli-artisans-at-iicd/</link>
		<comments>http://www.iicd.ac.in/2017/08/31/puppet-making-workshop-with-kathputli-artisans-at-iicd/#respond</comments>
		<pubDate>Thu, 31 Aug 2017 07:11:04 +0000</pubDate>
		<dc:creator><![CDATA[iicdadmin]]></dc:creator>
				<category><![CDATA[News]]></category>

		<guid isPermaLink="false">http://www.iicd.ac.in/?p=2575</guid>
		<description><![CDATA[<p>As a part of the PG Foundation module – Research on Art and Crafts Practice in India &#8211; the students were introduced to the process of making puppets or Kathputli. They gained hands-on experience working with the artisans and learned the skill to go on further to create puppets inspired by the tradition.</p>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/08/31/puppet-making-workshop-with-kathputli-artisans-at-iicd/">Puppet Making Workshop With Kathputli Artisans at IICD</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></description>
				<content:encoded><![CDATA[<p>As a part of the PG Foundation module – Research on Art and Crafts Practice in India &#8211; the students were introduced to the process of making puppets or Kathputli. They gained hands-on experience working with the artisans and learned the skill to go on further to create puppets inspired by the tradition.</p>
<div class="large-12 medium-12 small-12 columns">
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/08/Puppet2.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/08/Puppet2.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/08/Puppet3.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/08/Puppet3.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/08/Puppet4.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/08/Puppet4.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/08/Puppet5.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/08/Puppet5.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/08/Puppet6.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/08/Puppet6.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns" style="float: left;">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/08/Puppet7.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/08/Puppet7.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
</div>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/08/31/puppet-making-workshop-with-kathputli-artisans-at-iicd/">Puppet Making Workshop With Kathputli Artisans at IICD</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></content:encoded>
			<wfw:commentRss>http://www.iicd.ac.in/2017/08/31/puppet-making-workshop-with-kathputli-artisans-at-iicd/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Independence Day celebration at IICD</title>
		<link>http://www.iicd.ac.in/2017/08/21/independence-day-celebration-iicd/</link>
		<comments>http://www.iicd.ac.in/2017/08/21/independence-day-celebration-iicd/#respond</comments>
		<pubDate>Mon, 21 Aug 2017 04:55:54 +0000</pubDate>
		<dc:creator><![CDATA[iicdadmin]]></dc:creator>
				<category><![CDATA[News]]></category>

		<guid isPermaLink="false">http://www.iicd.ac.in/?p=2541</guid>
		<description><![CDATA[<p>Independence Day was celebrated at IICD with full zeal and passion. Everyone actively participated to make the day special. Cultural program by the students Cultural program by the students Tree plantation by the Director Dr. Toolika Gupta Flag hoisting by the Director of IICD, Dr. Toolika Gupta</p>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/08/21/independence-day-celebration-iicd/">Independence Day celebration at IICD</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></description>
				<content:encoded><![CDATA[<p>Independence Day was celebrated at IICD with full zeal and passion. Everyone actively participated to make the day special.</p>
<div class="large-12 medium-12 small-12 columns">
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/08/Post-365.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/08/Post-365.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/08/1.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/08/1.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a><br />
         <a href="javascript:void(0);">Cultural program by the students</a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/08/2.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/08/2.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a><br />
        <a href="javascript:void(0);">Cultural program by the students</a>
    </div>
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/08/3.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/08/3.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a><br />
        <a href="javascript:void(0);">Tree plantation by the Director Dr. Toolika Gupta</a>
    </div>
</div>
<div class="large-12 medium-12 small-12 columns">
<div class="large-3 medium-12 small-12 columns">
        <a href="http://www.iicd.ac.in/wp-content/uploads/2017/08/4a.jpg"><br />
            <img src="http://www.iicd.ac.in/wp-content/uploads/2017/08/4a.jpg" style="width: 100%; border: 1px solid black;"><br />
        </a><br />
        <a href="javascript:void(0);">Flag hoisting by the Director of IICD, Dr. Toolika Gupta</a>
    </div>
</div>
<p>The post <a rel="nofollow" href="http://www.iicd.ac.in/2017/08/21/independence-day-celebration-iicd/">Independence Day celebration at IICD</a> appeared first on <a rel="nofollow" href="http://www.iicd.ac.in">IICD</a>.</p>
]]></content:encoded>
			<wfw:commentRss>http://www.iicd.ac.in/2017/08/21/independence-day-celebration-iicd/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
	</channel>
</rss>

<!-- Dynamic page generated in 0.700 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2017-12-15 09:55:12 -->
